#include <iostream>
#include <fstream>
#include <Segment.hpp>
#include <dseg/DirectSegmentsDetector.hpp>
#include <dseg/SegmentsSet.hpp>
#include <dseg/SegmentHypothesis.hpp>
#include <hdseg/HierarchicalDirectSegmentsDetector.hpp>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <lsd/LSDSegmentsDetector.hpp>
#include <chaining/ChainingSegmentsDetector.hpp>
#include <hough/HoughSegmentsDetector.hpp>
#include <utils/Macros_p.hpp>
#include <Segment.hpp>

#include <evaluation/StatisticalComparison.hpp>

void printHelp()
{
  std::cout << "segments_evaluation [options] [source]" << std::endl;
  std::cout << std::endl;
  std::cout << "Evaluate algorithms on a sequence of images." << std::endl;
  std::cout << "Source: " << std::endl;
  std::cout << "  sequence [filepattern] [first] [last] [ref] (example of file pattern is 'input_%04d.png')" << std::endl;
  std::cout << "  noise    [filename] [noise increment] [noise maximum]" << std::endl;
  std::cout << std::endl;
  std::cout << "Options: " << std::endl;
  std::cout << "--help            show this help message" << std::endl;
  std::cout << "--version         show the version" << std::endl;
  std::cout << "--algorithm name  select which algorithm to use (all, dseg, dseg4, hdseg, hough, chaining, lsd)" << std::endl;
  std::cout << "--length-evaluation evaluate a single algorithm with different length 20, 50 and 100" << std::endl;
  std::cout << "--tau-evaluation  evaluate dseg algorithm with different value of tau 0, 10, 20 and 30" << std::endl;
  std::cout << "--scale [factor]  scale the image to the factor" << std::endl;
}

void printVersion()
{
  std::cout << "segments_evaluation v1.0" << std::endl;
  std::cout << "Copyright (c) 2008,2009,2010 LAAS/CNRS" << std::endl;
  std::cout << "Copyright (c) 2015 Cyrille Berger" << std::endl;
}


enum AlgoName {
  AT_Dseg     = 0x1,
  AT_Dseg4    = 0x2,
  AT_HDseg    = 0x4,
  AT_Hough    = 0x8,
  AT_Chaining = 0x10,
  AT_LSD      = 0x20,
  
  AT_ALL      = 0xFFFF
};

struct BaseSequencer
{
  virtual ~BaseSequencer() {}
  virtual cv::Mat nextImage() = 0;
  virtual cv::Mat refernceImage() = 0;
  virtual bool hasNext() const = 0;
};

struct SequenceSequencer : public BaseSequencer
{
  cv::Mat getImage(int idx) const;
  virtual cv::Mat refernceImage();
  virtual cv::Mat nextImage();
  virtual bool hasNext() const { return next < last; }
  std::string m_pattern;
  int next;
  int last;
  int ref;
};

cv::Mat SequenceSequencer::getImage(int idx) const
{
  char buffer[255];
  sprintf(buffer, m_pattern.c_str(), idx);
  return cv::imread(buffer, cv::IMREAD_GRAYSCALE);
}

cv::Mat SequenceSequencer::refernceImage()
{
  return getImage(ref);
}

cv::Mat SequenceSequencer::nextImage()
{
  cv::Mat im = getImage(next);
  ++next;
  return im;
}

struct NoiseSequencer : public BaseSequencer
{
  virtual cv::Mat refernceImage();
  virtual cv::Mat nextImage();
  virtual bool hasNext() const { return add_noise <= max_noise; }
  bool update_mult_noise;
  double mult_noise, add_noise, inc_noise;
  double max_noise;
  std::string m_filename;
};

cv::Mat NoiseSequencer::refernceImage()
{
  return cv::imread(m_filename, cv::IMREAD_GRAYSCALE);
}

cv::Mat NoiseSequencer::nextImage()
{
  if(update_mult_noise)
  {
    mult_noise += inc_noise;
    update_mult_noise = false;
  } else {
    add_noise  += inc_noise;
    update_mult_noise = true;
  }
  cv::Mat imgr = cv::imread(m_filename, cv::IMREAD_GRAYSCALE);
  cv::Mat noise(imgr.rows, imgr.cols, CV_8UC1);
  cv::RNG rng(1242);
  rng.fill(noise, cv::RNG::NORMAL, cv::Scalar(125.0, 125.0, 125.0), cv::Scalar(mult_noise, mult_noise, mult_noise));
  cv::multiply(imgr, noise, imgr, 1.0 / 125.0);
  rng.fill(noise, cv::RNG::NORMAL, cv::Scalar(0.0, 0.0, 0.0), cv::Scalar(add_noise, add_noise, add_noise));
  cv::add(imgr, noise, imgr);

  return imgr;
}

class SegmentsDetector4 : public libsegments::SegmentsDetector
{
public:
  SegmentsDetector4(libsegments::SegmentsDetector* _sd) : m_sd(_sd) {}
  virtual ~SegmentsDetector4() {}
  virtual std::vector< libsegments::Segment > detectSegments(const cv::Mat& _image)
  {
    cv::Mat im;
    cv::resize(_image, im, cv::Size(), 0.25, 0.25);
    return m_sd->detectSegments(im);
  }
private:
  libsegments::SegmentsDetector* m_sd;
};

std::string toLowerCase(const std::string &str)
{
  std::string str2 = str;
  std::transform(str2.begin(), str2.end(), str2.begin(), (int(*)(int))std::tolower);
  return str2;
}

int main(int argc, char **argv)
{
  AlgoName algoName       = AT_ALL;
  BaseSequencer* bs       = 0;
  bool length_evaluation  = false;
  bool tau_evaluation     = false;
  double scale_factor     = 0.5;
  
  for(int i = 1; i < argc; ++i)
  {
    std::string arg = argv[i];
    if( arg == "--help")
    {
      printHelp();
      return 0;
    } else if( arg == "--version") {
      printVersion();
    } else if( arg == "--length-evaluation") {
      length_evaluation = true;
    } else if( arg == "--tau-evaluation") {
      tau_evaluation = true;
    } else if( arg == "--scale") {
      ++i;
      scale_factor = atof(argv[i]);
    } else if( arg == "--algorithm") {
      if( i + 1 >= argc)
      {
        std::cout << "Invalid number of arguments" << std::endl;
        printHelp();
        return -1;
      } else {
        ++i;
        std::string type = argv[i];
        if( type == "all")
        {
          algoName = AT_ALL;
        } else if( type == "dseg")
        {
          algoName = AT_Dseg;
        } else if( type == "dseg4")
        {
          algoName = AT_Dseg4;
        } else if(type == "hdseg")
        {
          algoName = AT_HDseg;
        } else if(type == "hough")
        {
          algoName = AT_Hough;
        } else if(type == "chaining")
        {
          algoName = AT_Chaining;
        } else if(type == "lsd")
        {
          algoName = AT_LSD;
        } else {
          std::cout << "Invalid algorithm name" << std::endl;
          printHelp();
          return -1;
        }
      }
    } else {
      std::string sequencer_type = argv[i];
      if(sequencer_type == "sequence")
      {
        ++i;
        if(i + 4 != argc)
        {
          std::cout << "Invalid number of arguments" << std::endl;
          printHelp();
          return -1;
        }
        SequenceSequencer* ss = new SequenceSequencer;
        ss->m_pattern = argv[i];
        ss->next      = std::atoi(argv[i + 1]);
        ss->last      = std::atoi(argv[i + 2]);
        ss->ref       = std::atoi(argv[i + 3]);
        i += 3;
        bs = ss;
      } else if(sequencer_type == "noise")
      {
        ++i;
        if(i + 3 != argc)
        {
          std::cout << "Invalid number of arguments" << std::endl;
          printHelp();
          return -1;
        }
        NoiseSequencer* ns    = new NoiseSequencer;
        ns->update_mult_noise = true;
        ns->mult_noise        = 0.0;
        ns->add_noise         = 0.0;
        ns->inc_noise         = std::atof(argv[i + 1]);
        ns->max_noise         = std::atof(argv[i + 2]);
        ns->m_filename        = argv[i];
        i += 2;
        bs = ns;
      } else {
        std::cerr << "Unknown experiment: " << sequencer_type << std::endl;
        printHelp();
        return -1;
      }
    }
  }
  
  std::vector< libsegments::SegmentsDetector* > detectors;
  
  if(length_evaluation)
  {
    if(algoName == AT_Dseg)
    {
      libsegments::dseg::DirectSegmentsDetector* dsd = new libsegments::dseg::DirectSegmentsDetector;
      dsd->setMinimumSegmentLength(20);
      detectors.push_back(dsd);
      dsd = new libsegments::dseg::DirectSegmentsDetector;
      dsd->setMinimumSegmentLength(50);
      detectors.push_back(dsd);
      dsd = new libsegments::dseg::DirectSegmentsDetector;
      dsd->setMinimumSegmentLength(100);
      detectors.push_back(dsd);
    } else {
      std::cerr << "Unsupported algorithm for length evaluation" << std::endl;
      return -1;
    }
  } else if(tau_evaluation) {
    libsegments::dseg::DirectSegmentsDetector* dsd = new libsegments::dseg::DirectSegmentsDetector;
    dsd->setMinimumSegmentLength(20);
    dsd->setThresholdMaxGradient(0);
    detectors.push_back(dsd);
    dsd = new libsegments::dseg::DirectSegmentsDetector;
    dsd->setMinimumSegmentLength(20);
    dsd->setThresholdMaxGradient(10);
    detectors.push_back(dsd);
    dsd = new libsegments::dseg::DirectSegmentsDetector;
    dsd->setMinimumSegmentLength(20);
    dsd->setThresholdMaxGradient(20);
    detectors.push_back(dsd);
    dsd = new libsegments::dseg::DirectSegmentsDetector;
    dsd->setMinimumSegmentLength(20);
    dsd->setThresholdMaxGradient(30);
    detectors.push_back(dsd);    
  } else {
    if(algoName & AT_Dseg)
    {
      libsegments::dseg::DirectSegmentsDetector* dsd = new libsegments::dseg::DirectSegmentsDetector;
      dsd->setMinimumSegmentLength(20);
      detectors.push_back(dsd);
    }
    if(algoName & AT_Dseg4)
    {
      libsegments::dseg::DirectSegmentsDetector* dsd = new libsegments::dseg::DirectSegmentsDetector;
      dsd->setMinimumSegmentLength(5);
      detectors.push_back(new SegmentsDetector4(dsd));
    }
    if(algoName & AT_HDseg)
    {
      libsegments::hdseg::HierarchicalDirectSegmentsDetector* hdsd = new libsegments::hdseg::HierarchicalDirectSegmentsDetector;
      hdsd->setMinimumSegmentLength(20);
      detectors.push_back(hdsd);
    }
    if(algoName & AT_Hough)
    {
      libsegments::hough::HoughSegmentsDetector* hsd = new libsegments::hough::HoughSegmentsDetector;
      hsd->setMinimumLineLength(20);
      detectors.push_back(hsd);
    }
    if(algoName & AT_Chaining)
    {
      libsegments::chaining::ChainingSegmentsDetector* csd = new libsegments::chaining::ChainingSegmentsDetector;
      csd->setThreshold(20);
      detectors.push_back(csd);
    }
    if(algoName & AT_LSD)
    {
      libsegments::lsd::LSDSegmentsDetector* lsd = new libsegments::lsd::LSDSegmentsDetector;
      lsd->setMinimalSegmentLength(20);
      detectors.push_back(lsd);
    }
  }

  std::ofstream of_repetability, of_unmatches, of_splited, of_detected;
  of_repetability.open("repetability.csv");
  of_unmatches.open("unmatches.csv");
  of_splited.open("splited.csv");
  of_detected.open("detected.csv");
  
  std::vector< std::vector< libsegments::Segment> > references;
  
  {
    cv::Mat img_ = bs->refernceImage();
    cv::Mat img;
    cv::resize(img_, img, cv::Size(), scale_factor, scale_factor);
    for(int i = 0; i < detectors.size(); ++i)
    {
      std::vector< libsegments::Segment > segments = detectors[i]->detectSegments(img);
      references.push_back(segments);
    }
  }
  
  int idx = 0;
  while(bs->hasNext())
  {
    of_repetability << idx;
    of_unmatches    << idx;
    of_splited      << idx;
    of_detected     << idx;
    cv::Mat img_ = bs->nextImage();
    cv::Mat img;
    cv::resize(img_, img, cv::Size(), scale_factor, scale_factor);
    for(int i = 0; i < detectors.size(); ++i)
    {
      std::vector< libsegments::Segment > segments = detectors[i]->detectSegments(img);
      libsegments::evaluation::StatisticalComparison sc(references[i], segments, libsegments::evaluation::StatisticalComparison::MatchMode::DoubleMatch);
      of_repetability << " " << sc.repetability();
      of_unmatches    << " " << sc.unmatched();
      of_splited      << " " << sc.splited();
      of_detected     << " " << sc.totalCompSegments();
    }
    of_repetability << '\n';
    of_unmatches    << '\n';
    of_splited      << '\n';
    of_detected     << '\n';
    ++idx;
  }
  return 0;
}