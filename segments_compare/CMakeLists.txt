
set(DSEG_SRC main.cpp )

add_executable(segments_compare ${DSEG_SRC})
target_link_libraries(segments_compare rt ${OpenCV_LIBRARIES} ${PNG_LIBRARIES} libsegments)
install(TARGETS segments_compare ${INSTALL_TARGETS_DEFAULT_ARGS} )
