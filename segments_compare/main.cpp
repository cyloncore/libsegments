#include <iostream>
#include <fstream>
#include <dseg/DirectSegmentsDetector.hpp>
#include <dseg/SegmentsSet.hpp>
#include <dseg/SegmentHypothesis.hpp>
#include <hdseg/HierarchicalDirectSegmentsDetector.hpp>

#include <opencv2/highgui.hpp>
#include <lsd/LSDSegmentsDetector.hpp>
#include <chaining/ChainingSegmentsDetector.hpp>
#include <hough/HoughSegmentsDetector.hpp>
#include <utils/Macros_p.hpp>
#include <Segment.hpp>

#include <evaluation/StatisticalComparison.hpp>

void printHelp()
{
  std::cout << "segments_compare [options] input1.png input2.png" << std::endl;
  std::cout << std::endl;
  std::cout << "Extract segment in input1.png and input2.png and run the comparison." << std::endl;
  std::cout << std::endl;
  std::cout << "--help            show this help message" << std::endl;
  std::cout << "--version         show the version" << std::endl;
  std::cout << "--algorithm name  select which algorithm to use (dseg, hdseg, hough, chaining, lsd)" << std::endl;
}

void printVersion()
{
  std::cout << "segments_evaluation v1.0" << std::endl;
  std::cout << "Copyright (c) 2008,2009,2010 LAAS/CNRS" << std::endl;
  std::cout << "Copyright (c) 2015 Cyrille Berger" << std::endl;
}

enum AlgoName {
  AT_Dseg, AT_HDseg, AT_Hough, AT_Chaining, AT_LSD
};

std::string toLowerCase(const std::string &str)
{
  std::string str2 = str;
  std::transform(str2.begin(), str2.end(), str2.begin(), (int(*)(int))std::tolower);
  return str2;
}

int main(int argc, char **argv) {
  AlgoName algoName = AT_Dseg;
  std::string input1Filename;
  std::string input2Filename;
  for(int i = 1; i < argc; ++i)
  {
    std::string arg = argv[i];
    if( arg == "--help")
    {
      printHelp();
      return 0;
    } else if( arg == "--version") {
      printVersion();
    } else if( arg == "--algorithm") {
      if( i + 1 >= argc)
      {
        std::cout << "Invalid number of arguments" << std::endl;
        printHelp();
        return -1;
      } else {
        ++i;
        std::string type = argv[i];
        if( type == "dseg")
        {
          algoName = AT_Dseg;
        } else if(type == "hdseg")
        {
          algoName = AT_HDseg;
        } else if(type == "hough")
        {
          algoName = AT_Hough;
        } else if(type == "chaining")
        {
          algoName = AT_Chaining;
        } else if(type == "lsd")
        {
          algoName = AT_LSD;
        } else {
          std::cout << "Invalid algorithm name" << std::endl;
          printHelp();
          return -1;
        }
      }
    } else {
      if(input1Filename.empty())
      {
        input1Filename = arg;
      } else if(input2Filename.empty())
      {
        input2Filename = arg;
      } else {
        std::cout << "Invalid number of arguments" << std::endl;
        printHelp();
        return -1;
      }
    }
  }
  if(input1Filename.empty() or input2Filename.empty())
  {
    std::cout << "Invalid number of arguments" << std::endl;
    printHelp();
    return -1;
  }
  
  cv::Mat image1 = cv::imread(input1Filename.c_str(), cv::IMREAD_GRAYSCALE);
  if(image1.empty())
  {
    std::cout << "Failed to load image 1" << std::endl;
    return -1;
  }
  cv::Mat image2 = cv::imread(input2Filename.c_str(), cv::IMREAD_GRAYSCALE);
  if(image2.empty())
  {
    std::cout << "Failed to load image 2" << std::endl;
    return -1;
  }
  libsegments::SegmentsDetector* detector = 0;
  switch(algoName)
  {
    case AT_Dseg:
    {
      detector = new libsegments::dseg::DirectSegmentsDetector;
      break;
    }
    case AT_HDseg:
    {
      detector = new libsegments::hdseg::HierarchicalDirectSegmentsDetector;
      break;
    }
    case AT_Hough:
    {
      detector = new libsegments::hough::HoughSegmentsDetector;
      break;
    }
    case AT_Chaining:
    {
      detector = new libsegments::chaining::ChainingSegmentsDetector;
      break;
    }
    case AT_LSD:
    {
      detector = new libsegments::lsd::LSDSegmentsDetector;
      break;
    }
  }
  std::vector<libsegments::Segment> segments1 = detector->detectSegments(image1);
  std::vector<libsegments::Segment> segments2 = detector->detectSegments(image2);
  delete detector;
  
  libsegments::evaluation::StatisticalComparison sc(segments1, segments2, libsegments::evaluation::StatisticalComparison::MatchMode::DoubleMatch);

  std::cout << "# segments in Image1: " << sc.totalRefSegments()  << std::endl
            << "# segments in Image2: " << sc.totalCompSegments() << std::endl
            << "repetability:         " << sc.repetability()      << std::endl
            << "unmatched:            " << sc.unmatched()         << std::endl
            << "splitted:             " << sc.splited()           << std::endl
            << "distanceMean:         " << sc.distanceMean()      << std::endl
            << "distanceStdDev:       " << sc.distanceStdDev()    << std::endl;
  
  return 0;
}

