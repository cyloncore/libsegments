#include <iostream>
#include <fstream>
#include <dseg/DirectSegmentsDetector.hpp>
#include <dseg/SegmentsSet.hpp>
#include <dseg/SegmentHypothesis.hpp>
#include <hdseg/HierarchicalDirectSegmentsDetector.hpp>

#include <opencv2/highgui.hpp>
#include <lsd/LSDSegmentsDetector.hpp>
#include <elsed/ELSEDSegmentsDetector.hpp>
#include <chaining/ChainingSegmentsDetector.hpp>
#include <hough/HoughSegmentsDetector.hpp>
#include <utils/Macros_p.hpp>
#include <Segment.hpp>

void printHelp()
{
  std::cout << "segments [options] input.png [output]" << std::endl;
  std::cout << std::endl;
  std::cout << "Extract segment in input.png and write the result in the output, if none is specified, the list will be dumped in standard output." << std::endl;
  std::cout << std::endl;
  std::cout << "--help            show this help message" << std::endl;
  std::cout << "--version         show the version" << std::endl;
  std::cout << "--output type     the type of output which can be txt or png" << std::endl;
  std::cout << "--algorithm name  select which algorithm to use (dseg, hdseg, hough, chaining, lsd, elsed)" << std::endl;
}

void printVersion()
{
  std::cout << "segments v1.0" << std::endl;
  std::cout << "Copyright (c) 2008,2009,2010 LAAS/CNRS" << std::endl;
  std::cout << "Copyright (c) 2010 Cyrille Berger" << std::endl;
}

enum OutputType {
  OT_Auto, OT_PNG, OT_TXT
};

enum AlgoName {
  AT_Dseg, AT_HDseg, AT_Hough, AT_Chaining, AT_LSD, AT_ELSED
};

std::string toLowerCase(const std::string &str)
{
  std::string str2 = str;
  std::transform(str2.begin(), str2.end(), str2.begin(), (int(*)(int))std::tolower);
  return str2;
}

int main(int argc, char **argv) {
  OutputType outputType = OT_Auto;
  AlgoName algoName = AT_Dseg;
  std::string inputFilename;
  std::string outputFilename;
  for(int i = 1; i < argc; ++i)
  {
    std::string arg = argv[i];
    if( arg == "--help")
    {
      printHelp();
      return 0;
    } else if( arg == "--version") {
      printVersion();
    } else if( arg == "--output") {
      if( i + 1 >= argc)
      {
        std::cout << "Invalid number of arguments" << std::endl;
        printHelp();
        return -1;
      } else {
        ++i;
        std::string type = argv[i];
        if( type == "png")
        {
          outputType = OT_PNG;
        } else if(type == "txt")
        {
          outputType = OT_TXT;
        } else {
          std::cout << "Invalid output type" << std::endl;
          printHelp();
          return -1;
        }
      }
    } else if( arg == "--algorithm") {
      if( i + 1 >= argc)
      {
        std::cout << "Invalid number of arguments" << std::endl;
        printHelp();
        return -1;
      } else {
        ++i;
        std::string type = argv[i];
        if( type == "dseg")
        {
          algoName = AT_Dseg;
        } else if(type == "hdseg")
        {
          algoName = AT_HDseg;
        } else if(type == "hough")
        {
          algoName = AT_Hough;
        } else if(type == "chaining")
        {
          algoName = AT_Chaining;
        } else if(type == "lsd")
        {
          algoName = AT_LSD;
        } else if(type == "elsed")
        {
          algoName = AT_ELSED;
        } else {
          std::cout << "Invalid algorithm name" << std::endl;
          printHelp();
          return -1;
        }
      }
    } else {
      if(inputFilename.empty())
      {
        inputFilename = arg;
      } else if(outputFilename.empty())
      {
        outputFilename = arg;
      } else {
        std::cout << "Invalid number of arguments" << std::endl;
        printHelp();
        return -1;
      }
    }
  }
  if(inputFilename.empty())
  {
    std::cout << "Invalid number of arguments" << std::endl;
    printHelp();
    return -1;
  }
  if(outputFilename.empty())
  {
    if(outputType != OT_Auto and outputType != OT_TXT)
    {
      std::cout << "No output filename, can only use the text output." << std::endl;
      printHelp();
      return -1;
    }
    outputType = OT_TXT;
  }
  if(outputType == OT_Auto)
  {
    if(outputFilename.size() > 3 && toLowerCase(outputFilename.substr( outputFilename.length() - 3).c_str()) == "png" )
    {
      outputType = OT_PNG;
    } else {
      outputType = OT_TXT;
    }
  }
  
  cv::Mat image = cv::imread(inputFilename.c_str(), cv::IMREAD_GRAYSCALE);
  if(image.empty())
  {
    std::cout << "Failed to load image" << std::endl;
    return -1;
  }
  libsegments::SegmentsDetector* detector = 0;
  switch(algoName)
  {
    case AT_Dseg:
    {
      detector = new libsegments::dseg::DirectSegmentsDetector;
      break;
    }
    case AT_HDseg:
    {
      detector = new libsegments::hdseg::HierarchicalDirectSegmentsDetector;
      break;
    }
    case AT_Hough:
    {
      detector = new libsegments::hough::HoughSegmentsDetector;
      break;
    }
    case AT_Chaining:
    {
      detector = new libsegments::chaining::ChainingSegmentsDetector;
      break;
    }
    case AT_LSD:
    {
      detector = new libsegments::lsd::LSDSegmentsDetector;
      break;
    }
    case AT_ELSED:
    {
      detector = new libsegments::elsed::ELSEDSegmentsDetector;
      break;
    }
  }
  std::vector<libsegments::Segment> segments = detector->detectSegments(image);
  delete detector;
  switch(outputType)
  {
    case OT_TXT:
    {
      std::ostream* o = &std::cout;
      if(not outputFilename.empty())
      {
        o = new std::ofstream(outputFilename.c_str());
      }
      LS_FOREACH(const libsegments::Segment& seg, segments)
      {
        *o << seg.x1 << " " << seg.y1 << " " << seg.x2 << " " << seg.y2 << std::endl;
      }
      if(not outputFilename.empty())
      {
        delete o;
      }
    }
      break;
    case OT_PNG:
    {
      cv::Mat img(image.size(), CV_MAKETYPE(CV_8U, 1));
      memset(img.data, 0, img.rows * img.step);
      cv::Scalar c{255, 255, 255, 255};
      LS_FOREACH(const libsegments::Segment& seg, segments)
      {
        cv::Point p1;
        p1.x = seg.x1;
        p1.y = seg.y1;
        cv::Point p2;
        p2.x = seg.x2;
        p2.y = seg.y2;
        cv::line(img, p1, p2, c, 1);
      }
      cv::imwrite(outputFilename, img);
    }
      break;
    default:
      std::cout << "Internal error, invalid output type." << std::endl;
      return -10;
  }
  return 0;
}
