#include "SegmentsDetector.hpp"

using namespace libsegments;

SegmentsDetector::SegmentsDetector()
{
}

SegmentsDetector::~SegmentsDetector()
{
}

#include "chaining/ChainingSegmentsDetector.hpp"
#include "dseg/DirectSegmentsDetector.hpp"
#include "hdseg/HierarchicalDirectSegmentsDetector.hpp"
#include "hough/HoughSegmentsDetector.hpp"
#include "lsd/LSDSegmentsDetector.hpp"
#include "elsed/ELSEDSegmentsDetector.hpp"

SegmentsDetector* SegmentsDetector::create(Algorithm _algorithm)
{
  switch(_algorithm)
  {
  case Algorithm::Chaining:
    return new chaining::ChainingSegmentsDetector();
  case Algorithm::DSeg:
    return new dseg::DirectSegmentsDetector();
  case Algorithm::HDSeg:
    return new hdseg::HierarchicalDirectSegmentsDetector();
  case Algorithm::Hough:
    return new hough::HoughSegmentsDetector();
  case Algorithm::LSD:
    return new lsd::LSDSegmentsDetector();
  case Algorithm::ELSED:
    return new elsed::ELSEDSegmentsDetector();
  }
  return nullptr;
}
