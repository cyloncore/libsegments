#ifndef _SEGMENTS_PLUS_BASE_H_
#define _SEGMENTS_PLUS_BASE_H_


#include <opencv2/opencv.hpp>

#include <vector>
#include <map>

#include <Eigen/Core>

#include "../utils/Macros.hpp"

namespace libsegments {
  namespace dseg {
    class SegmentsSet;
    class SegmentProbabilityEstimator;
    class SegmentHypothesis;
    class GrowStopCondition;
    struct SegmentDebug {
      cv::Mat inasegment;
      cv::Mat proba;
      cv::Mat dx;
      cv::Mat dy;
    };
    struct GrowSegmentContext {
      SegmentHypothesis* segHyp;
      cv::Mat originalImage; //< only usefull when ENABLE_SEGMENT_HYPOTHESIS_DEBUG
      cv::Mat proba;
      cv::Mat dx;
      cv::Mat dy;
      cv::Mat* inasegment;
      std::map<int, int> segmentToCount;
      double x_1;
      double x_2;
      double y_1;
      double y_2;
      int k_p;
      int k_n;
      GrowStopCondition* growStopCondition;
    };
    /**
     * @ingroup dseg
     * Base class for algorithms that wants to use the segment growing algorithm, for detection
     * or tracking.
     */
    class DirectSegmentsBase {
      public:
        DirectSegmentsBase();
        virtual ~DirectSegmentsBase();
      protected:
        bool searchPoint( GrowSegmentContext& _context, double _searchStep, const Eigen::Vector2d& _pi, double& _xr, double& _yr, double& _angler, int& _bestI, int _searchNbSteps);
        bool smartSearchPoint( GrowSegmentContext& _context, double _searchStep, const Eigen::Vector2d& _pi, double& _xr, double& _yr, double& _angler, int& _bestI, int _searchNbSteps);
        void coloriage( double x_1, double y_1, double x_2, double y_2, cv::Mat& inasegment, int _value);
        /**
         */
        void computeGradientValuesOnLine( double& a, double& b, double angle, double cosA, double sinA, float* _mag1, float* _mag2, float* _mag3, int j ) const;
        bool hasMaximumGradient( double _angle, double _cosA, double _sinA, float* _mag1 , float* _mag2, float* _mag3, int j ) const;
        bool growSegment( GrowSegmentContext& _context, int _step = 1 );
        SegmentHypothesis* attemptToMerge( SegmentHypothesis* segHyp, const cv::Mat& inasegment, SegmentsSet&  segmentHypothesises , const std::map<int, int>& segmentToCount);
      private:
        /**
         * The search size is the number of point use to adjust the segment. This defines
         * the number of points used for discretization with a line.
         */
        LS_DEFINE_PARAM_RW( int, searchNbSteps, setSearchNbSteps);
        /**
         * Define the maximal search step. If the current step is higher, the number of steps
         * will be adjusted so that the area covered by the search remains the same. And the value
         * of maximalSearchStep is used as a step.
         */
        LS_DEFINE_PARAM_RW( double, maximalSearchStep, setMaximalSearchStep);
        /**
         * Set the threshold on how much the gradients must be colinear to accept a point
         * in the line.
         */
        LS_DEFINE_PARAM_RW( double, thresholdGradColinearity, setThresholdGradColinearity );
        /**
         * Set the threshold for the maximum of gradient. Which is used to know if a pixel
         * is the start of a gradient, it's the minimum difference between the gradient on
         * the pixel and the gradients of the neighbourghood.
         */
        LS_DEFINE_PARAM_RW( double, thresholdMaxGradient, setThresholdMaxGradient );
        /**
         * Set the minimum length of segments.
         */
        LS_DEFINE_PARAM_RO( double, minimumSegmentLength);
        /**
         * Set the method used for computing the probability of presence of a segment.
         */
        LS_DEFINE_PARAM_RW( SegmentProbabilityEstimator*, segmentProbabilityEstimator, setSegmentProbabilityEstimator );
        /**
         * Define the maximum length of the search area for point measurement
         */
        LS_DEFINE_PARAM_RW( double, maximalSearchLength, setMaximalSearchLength);
      public:
        void setMinimumSegmentLength(double _minimumSegmentLength);
      protected:
        double minimumSegmentLengthPow2() const;
      protected:
        int segCount;
        void adjustStep( double& _step, int &_searchNbSteps ) const;
      private:
        double m_minimumSegmentLengthPow2;
    };
  }
}


#endif
