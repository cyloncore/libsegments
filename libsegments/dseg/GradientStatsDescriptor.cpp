/* $Id: GradientStatsDescriptor.cpp 3384 2008-12-01 22:35:20Z cberger $ */

#include "GradientStatsDescriptor.hpp"

using namespace libsegments::dseg;

GradientStatsDescriptor::GradientStatsDescriptor() :
    m_sumGradients(0.0),
    m_meanGradients(0.0),
    m_meanGradientsUpToDate( true ),
    m_count(0)
{
}

GradientStatsDescriptor::~GradientStatsDescriptor()
{
}

void GradientStatsDescriptor::appendGradient( double gradient )
{
  m_sumGradients += gradient;
  ++m_count;
  m_meanGradientsUpToDate = false;
}

double GradientStatsDescriptor::meanGradients() const
{
  if( not m_meanGradientsUpToDate )
  {
    m_meanGradients = m_sumGradients / m_count;
    m_meanGradientsUpToDate = true;
  }
  return m_meanGradients;
}
