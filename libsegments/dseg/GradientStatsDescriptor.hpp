/* $Id: GradientStatsDescriptor.hpp 3384 2008-12-01 22:35:20Z cberger $ */

#ifndef _DSEG_GRADIENT_DESCRIPTOR_HPP_
#define _DSEG_GRADIENT_DESCRIPTOR_HPP_

namespace libsegments {
  namespace dseg {
    class GradientStatsDescriptor {
      public:
        GradientStatsDescriptor();
        ~GradientStatsDescriptor();
      public:
        void appendGradient( double gradient );
        double meanGradients() const;
      private:
        double m_sumGradients;
        mutable double m_meanGradients;
        mutable bool m_meanGradientsUpToDate;
        int m_count;
    };
  }
}

#endif
