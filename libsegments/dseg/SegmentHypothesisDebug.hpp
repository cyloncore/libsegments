/* $Id: SegmentHypothesisDebug.hpp 3657 2009-02-26 16:20:24Z cberger $ */

#ifndef _SEGMENTS_HYPOTHESIS_DEBUG_H_
#define _SEGMENTS_HYPOTHESIS_DEBUG_H_

#include <vector>
#include <Eigen/Core>

namespace libsegments {
  namespace dseg {
    class LineFitter;
    class SearchPointLineDebug {
      struct PointInfo {
        Eigen::Vector2d point;
        double angle;
        double grad;
        double gray;
      };
      public:
        SearchPointLineDebug();
        void addPoint( double _x, double _y, double _angle, double _grad, double _gray);
        int count() const;
        Eigen::Vector2d pointAt( int _index );
        double angleAt( int _index );
        double gradAt( int _index );
        double grayAt( int _index );
        void lastIsSelected(int _offset);
        int selectedIndex() const;
      private:
        std::vector< PointInfo > m_points;
        int m_selectedIndex;
    };
    class SegmentHypothesisDebug {
      public:
      public:
        int searchPointLinesCount() const;
        void addSearchPointLine( const SearchPointLineDebug& _spl);
        const SearchPointLineDebug& searchPointLineAt( unsigned int index );
        void merge( const SegmentHypothesisDebug& );
      private:
        std::vector< SearchPointLineDebug > m_searchPointLines;
    };
  }
}

#endif
