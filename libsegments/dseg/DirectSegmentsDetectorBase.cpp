#include "DirectSegmentsDetectorBase.hpp"

#include "LineFitterKalman2.hpp"
#include "../utils/Image.hpp"
#include "../utils/Utils.hpp"
#include "../utils/Macros_p.hpp"
#include "SegmentHypothesis.hpp"
#include "SegmentsSet.hpp"
#include "SegmentProbabilityEstimator.hpp"
#include "SegmentHypothesisDebug.hpp"
#include "GradientsDescriptor.hpp"

#include "Debug.hpp"

#include "LoopDetectionContext.hpp"
#include "DetectorGrowStopCondition.hpp"
#include "../utils/IdFactory.hpp"

#include "../Segment.hpp"

using namespace libsegments::dseg;

DirectSegmentsDetectorBase::DirectSegmentsDetectorBase(libsegments::utils::IdFactory* _idMaker) : m_adaptativeStep(true), m_mergeSegment(true), m_computeDescriptor( false), m_idMaker(_idMaker), m_ownIdMaker(false)
{
  if(m_idMaker == 0)
  {
    m_idMaker = new libsegments::utils::IdFactory;
    m_ownIdMaker = true;
  }
}

DirectSegmentsDetectorBase::~DirectSegmentsDetectorBase()
{
  if(m_ownIdMaker)
  {
    delete m_idMaker;
  }
}

unsigned int DirectSegmentsDetectorBase::nextId()
{
  return m_idMaker->getId();
}

void DirectSegmentsDetectorBase::loopDetect( LoopDetectionContext* _context, double _minimumSegmentLengthScale)
{
  int width = _context->proba().cols;
  int height = _context->proba().rows;
  // Search for segments
  int step = minimumSegmentLength() * 0.5 * _minimumSegmentLengthScale;
  if( step == 0 ) step = 1;
  for( int i = 1; i < height - 2; i++ )
  {
    int stepj = 1;
    if( adaptativeStep() )
    {
      stepj = ((i % step) == 0) ? 1 : step;
    }
    const short* _dx = (short*)(_context->dx().ptr(i));
    const short* _dy = (short*)(_context->dy().ptr(i));
    float* _mag1 = (float*)(_context->proba().ptr(i-1));
    float* _mag2 = (float*)(_context->proba().ptr(i));
    float* _mag3 = (float*)(_context->proba().ptr(i+1));
//     int* inasegment_it = (int*)_context->inasegment().data(i);
    for( int j = 1; j < width - 2; j+= stepj )
    {
//       if( inasegment_it[j] == 0 /*and (j > 331 and j < 338 ) and (i > 49 and i < 86)*/ )
      if( !isInASegment(_context, j, i) )
      {
        float center = _mag2[ j ];
        double angle = atan2( _dy[j], _dx[j] );
        double cosA = cos( angle );
        double sinA = sin( angle );
        double a = 1e6, b = 1e6;
        computeGradientValuesOnLine( a, b, angle, cosA, sinA, _mag1, _mag2, _mag3, j );
        double c = 1e6, d = 1e6;
        computeGradientValuesOnLine( c, d, angle + M_PI_2, sinA, -cosA, _mag1, _mag2, _mag3, j );
#if 1
        if( (center - a) > thresholdMaxGradient() and (center - b) > thresholdMaxGradient() and center > c and center > d )
        {
          LS_DEBUG_COND(DEBUG_COND, "=========================" << segCount);
          double x0 = j + 0.5;
          double y0 = i + 0.5;
          LS_DEBUG_COND(DEBUG_COND, LS_PP_VAR(angle) );
          SegmentHypothesis* segHyp = new SegmentHypothesis(x0, y0, angle);
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
          SearchPointLineDebug searchPointLine;
          searchPointLine.addPoint( x0, y0, angle, _context->proba().getPixelValue<float>(x0, y0), _context->image().getPixelValue<uint8_t>(x0, y0)  );
          segHyp->segmentHypothesisDebug()->addSearchPointLine( searchPointLine );
#endif

          GrowSegmentContext gsc;
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
          gsc.originalImage = _context->image();
#endif
          gsc.segHyp = segHyp;
          gsc.proba = _context->proba();
          gsc.dx = _context->dx();
          gsc.dy = _context->dy();
          gsc.inasegment = &_context->inasegment();
          gsc.x_1 = x0;
          gsc.x_2 = x0;
          gsc.y_1 = y0;
          gsc.y_2 = y0;
          gsc.k_p = 1;
          gsc.k_n = -1;
          DetectorGrowStopCondition dgsc;
          gsc.growStopCondition = &dgsc;
          
          bool success = growSegment( gsc );
          
          
          if( success and segHyp->countPoints() > 1 ) // needs at least an other point for the least square optimization
          {
            LS_DEBUG_COND(DEBUG_COND, segHyp->countPoints() << " " << gsc.x_1 << " " << gsc.y_1 << " " << gsc.x_2 << " " << gsc.y_2 );
            if(DEBUG_COND)
            {
              segHyp->dump(true);
            }
            // Project the extremity points on the segment
            segHyp->setExtremity1( gsc.x_1, gsc.y_1 );
            segHyp->setExtremity2( gsc.x_2, gsc.y_2 );
            detectedSegment( segHyp, _context, gsc );

          } else {
            delete segHyp;
          }
        }
#endif
      }
    }
  }
  return ;
}

bool DirectSegmentsDetectorBase::isInASegment( LoopDetectionContext* _context, int _x, int _y )
{
  return utils::image::getPixelValue<int>(_context->inasegment(), _x, _y) != 0;
}

std::vector< libsegments::Segment > DirectSegmentsDetectorBase::detectSegments( const cv::Mat& _image )
{
  libsegments::dseg::SegmentsSet segments;
  detectSegments(_image, segments);
  std::vector< libsegments::Segment > result;
  for(int i = 0; i < segments.count(); ++i)
  {
    libsegments::dseg::SegmentHypothesis* sh = segments.segmentAt(i);
    libsegments::Segment seg;
    seg.x1 = sh->x1();
    seg.y1 = sh->y1();
    seg.x2 = sh->x2();
    seg.y2 = sh->y2();
    result.push_back(seg);
  }
  return result;
}
