/* $Id: DirectSegmentsDetector.hpp 4054 2009-08-24 19:02:41Z cberger $ */

#ifndef _SEGMENTS_PLUS_H_
#define _SEGMENTS_PLUS_H_

#include "DirectSegmentsDetectorBase.hpp"

namespace libsegments {
  namespace dseg {
    /**
     * @ingroup dseg
     * This is the main class for detection segments in an image.
     */
    class DirectSegmentsDetector : public DirectSegmentsDetectorBase {
      public:
        DirectSegmentsDetector(utils::IdFactory* _idMaker = 0);
        /**
         * Start detection of segments in an image.
         * \param _image image on which to makes the detection
         * \param segmentStorage storage that will be filled with newly extracted segment,
         *                       if there are segments in the storage before calling this
         *                       function, then the dector will avoid segment detection
         *                       on the already existing segments (the detection can be
         *                       called after tracking, and tracked segments won't be
         *                       detected again).
         * \param SegmentDebug collect some debug information
         */
        void detectSegments( const cv::Mat& _image, SegmentsSet& segmentStorage, SegmentDebug* _sdebug = 0);
      protected:
        virtual void detectedSegment( SegmentHypothesis* , DirectSegmentsDetectorBase::LoopDetectionContext* _context, GrowSegmentContext&);
      private:
        double m_qualityThresholdPow2;
    };
  }
}


#endif
