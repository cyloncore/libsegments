/* $Id: SegmentProbabilityEstimator.cpp 3220 2008-10-09 08:16:58Z cberger $ */

#include "SegmentProbabilityEstimator.hpp"
#include "../utils/Utils.hpp"
#include "../utils/Image.hpp"

using namespace libsegments::dseg;

SegmentProbabilityEstimator::~SegmentProbabilityEstimator()
{
}

SobelSegmentProbabilityEstimator::~SobelSegmentProbabilityEstimator()
{
}

void SobelSegmentProbabilityEstimator::compute( const cv::Mat& _dx, const cv::Mat& _dy, cv::Mat& _proba )
{
  int width = _dx.cols;
  int height = _dx.rows;
  for( int i = 0; i < height; i++ )
  {
    const short* dx_it = (short*)(_dx.ptr(i));
    const short* dy_it = (short*)(_dy.ptr(i));
    float* mag_it = (float*)(_proba.ptr(i));
    for( int j = 0; j < width; j++ )
    {
      int x = dx_it[j];
      int y = dy_it[j];
      mag_it[j] = sqrt((double)x*x + (double)y*y);
    }
  }
}


RotationProbabilitySegmentProbabilityEstimator::~RotationProbabilitySegmentProbabilityEstimator()
{
}

void RotationProbabilitySegmentProbabilityEstimator::compute( const cv::Mat& _dx, const cv::Mat& _dy, cv::Mat& _proba )
{
  int width = _dx.cols;
  int height = _dy.rows;
  cv::Mat angle( height+2, width+2, CV_32FC1 );
  
  memset( angle.data , 0, angle.step);
  memset( angle.data + angle.step*(height+1) , 0, angle.step);
  for( int i = 0; i < height; i++ )
  {
    const short* dx_it = (short*)(_dx.ptr(i));
    const short* dy_it = (short*)(_dy.ptr(i));
    float* angle_it = (float*)(angle.data + angle.step*(i+1));
    angle_it[0] = 0;
    for( int j = 0; j < width; j++ )
    {
      int x = dx_it[j];
      int y = dy_it[j];
      angle_it[j+1] = atan2(y, x);
    }
    angle_it[width+1] = 0;
  }
  for(int i = 0; i < height; i++)
  {
    float* dst_it = (float*)(_proba.ptr( i ));
    float* _angle1 = (float*)(angle.data + angle.step*(i));
    float* _angle2 = (float*)(angle.data + angle.step*(i+1));
    float* _angle3 = (float*)(angle.data + angle.step*(i+2));
    for( int j = 0; j < width; j++ )
    {
      float angle_c = _angle2[j+1];
      float corr_angle = 0.0;
      corr_angle += fabs(corrAngle(_angle1[j], angle_c ) );
      corr_angle += fabs(corrAngle(_angle1[j+1], angle_c ) );
      corr_angle += fabs(corrAngle(_angle1[j+2], angle_c ) );
      corr_angle += fabs(corrAngle(_angle2[j], angle_c ) );
      corr_angle += fabs(corrAngle(_angle2[j+2], angle_c ) );
      corr_angle += fabs(corrAngle(_angle3[j], angle_c ) );
      corr_angle += fabs(corrAngle(_angle3[j+1], angle_c ) );
      corr_angle += fabs(corrAngle(_angle3[j+2], angle_c ) );
      dst_it[j] = corr_angle;
    }
  }
}

DirRotationProbabilitySegmentProbabilityEstimator::~DirRotationProbabilitySegmentProbabilityEstimator()
{
}

void DirRotationProbabilitySegmentProbabilityEstimator::compute( const cv::Mat& _dx, const cv::Mat& _dy, cv::Mat& _proba )
{
  int width = _dx.cols;
  int height = _dy.rows;
  cv::Mat angle( height+2, width+2, CV_32FC1 );
  
  memset( angle.data , 0, angle.step);
  memset( angle.data + angle.step*(height+1) , 0, angle.step);
  for( int i = 0; i < height; i++ )
  {
    const short* dx_it = (short*)(_dx.ptr(i));
    const short* dy_it = (short*)(_dy.ptr(i));
    float* _angle = (float*)(angle.data + angle.step*(i+1));
    _angle[0] = 0;
    for( int j = 0; j < width; j++ )
    {
      int x = dx_it[j];
      int y = dy_it[j];
      _angle[j+1] = atan2(y, x);
    }
    _angle[width+1] = 0;
  }
  for(int i = 0; i < height; i++)
  {
    float* dst_it = (float*)(_proba.ptr( i ));
    float* _angle1 = (float*)(angle.data + angle.step*(i));
    float* _angle2 = (float*)(angle.data + angle.step*(i+1));
    float* _angle3 = (float*)(angle.data + angle.step*(i+2));
    const short* dx_it = (short*)(_dx.ptr(i));
    const short* dy_it = (short*)(_dy.ptr(i));
    for( int j = 0; j < width; j++ )
    {
      float angle_c = _angle2[j+1];
      
      float angle_deg = angle_c * 180 / M_PI;
      if( angle_deg < 0.0) angle_deg = 360 + angle_deg;
      int at = (int)((angle_deg / 45));
      if( dx_it[j] == 0 and dy_it[j] == 0)
      {
        dst_it[j] = 0;
      } else {
        double hyp = sqrt( dx_it[j] * dx_it[j] + dy_it[j] * dy_it[j] ) * M_SQRT1_2;
        double cosA = fabs(dx_it[j] / hyp) ;
        double sinA = fabs(dy_it[j] / hyp);
        double angle_1, angle_2;
        switch( at )
        {
          case 0:
          case 4:
            angle_1 = (_angle1[j+1] * (1 - sinA) + _angle1[j+2] * sinA );
            angle_2 = (_angle3[j+1] * (1 - sinA) + _angle3[j  ] * sinA );
            break;
          case 1:
          case 5:
            angle_1 = (_angle1[j+2] * cosA + _angle2[j+2] * (1 - cosA ) );
            angle_2 = (_angle3[j+1] * cosA + _angle2[j  ] * (1 - cosA ) );
            break;
          case 2:
          case 6:
            angle_1 = (_angle2[j+2] * (1 - cosA) + _angle3[j+2] * cosA );
            angle_2 = (_angle2[j  ] * (1 - cosA) + _angle1[j  ] * cosA );
            break;
          case 3:
          case 7:
            angle_1 = (_angle3[j+2] * sinA + _angle3[j+1] * (1 - sinA) );
            angle_2 = (_angle1[j  ] * sinA + _angle1[j+1] * (1 - sinA) );
            break;
        }
        double corr_angle = 0.0;
        corr_angle += fabs(corrAngle(angle_1, angle_c ) );
        corr_angle += fabs(corrAngle(angle_2, angle_c ) );
        dst_it[j] = corr_angle;
      }
    }
  }
}

DirRotationProbability2SegmentProbabilityEstimator::DirRotationProbability2SegmentProbabilityEstimator(int _aperture) : m_aperture(_aperture)
{
}

DirRotationProbability2SegmentProbabilityEstimator::~DirRotationProbability2SegmentProbabilityEstimator()
{
}

void DirRotationProbability2SegmentProbabilityEstimator::compute( const cv::Mat& _dx, const cv::Mat& _dy, cv::Mat& _proba )
{
  int width = _dx.cols;
  int height = _dx.rows;
  cv::Mat angle( height, width, CV_MAKETYPE(CV_32F, 1) );
  
  for( int i = 0; i < height; i++ )
  {
    const short* dx_it = (short*)(_dx.ptr(i));
    const short* dy_it = (short*)(_dy.ptr(i));
    float* _angle = (float*)(angle.ptr(i+1));
    for( int j = 0; j < width; j++ )
    {
      int x = dx_it[j];
      int y = dy_it[j];
      _angle[j] = atan2(y, x);
    }
  }
   for(int i = 0; i < height; i++)
  {
    float* dst_it = (float*)(_proba.ptr( i ));
    const short* dx_it = (short*)(_dx.ptr(i));
    const short* dy_it = (short*)(_dy.ptr(i));
    const float* _angle = (float*)(angle.ptr(i));
    for( int j = 0; j < width; j++ )
    {
      double angle_c = _angle[j];
      int x = dx_it[j];
      int y = dy_it[j];
      if( x == 0 and y == 0 )
      {
        dst_it[j] = 0;
      } else {
        double hyp = sqrt( x * x + y * y ) * M_SQRT1_2;
        double cosA = fabs(x / hyp);
        double sinA = fabs(y / hyp);
        int count = 0;
        double corr_angle = 0.0;
        for( int k = -m_aperture; k <= m_aperture; ++k)
        {
          if( k != 0 )
          {
            double x_1 = j + sinA * k;
            double y_1 = i + cosA * k;
            if( x_1 >= 0 and y_1 >= 0 and x_1 <= width - 1 and y_1 <= height - 1 )
            {
              corr_angle += fabs(corrAngle( utils::image::getSubPixelValue<float>( angle, x_1, y_1, 0), angle_c ) );
              ++count;
            }
          }
        }
        dst_it[j] = corr_angle;
      }
    }
  }
}

SobelDirRotationProbability2SegmentProbabilityEstimator::SobelDirRotationProbability2SegmentProbabilityEstimator(int _aperture ) : m_aperture(_aperture)
{
}

SobelDirRotationProbability2SegmentProbabilityEstimator::~SobelDirRotationProbability2SegmentProbabilityEstimator()
{
}

void SobelDirRotationProbability2SegmentProbabilityEstimator::compute( const cv::Mat& _dx, const cv::Mat& _dy, cv::Mat& _proba )
{
  int width = _dx.cols;
  int height = _dx.rows;
  cv::Mat angle( height, width, CV_MAKETYPE(CV_32F, 1) );
  
  for( int i = 0; i < height; i++ )
  {
    const short* dx_it = (short*)(_dx.ptr(i));
    const short* dy_it = (short*)(_dy.ptr(i));
    float* _angle = (float*)(angle.ptr(i+1));
    for( int j = 0; j < width; j++ )
    {
      int x = dx_it[j];
      int y = dy_it[j];
      _angle[j] = atan2(y, x);
    }
  }
   for(int i = 0; i < height; i++)
  {
    float* dst_it = (float*)(_proba.ptr( i ));
    const short* dx_it = (short*)(_dx.ptr(i));
    const short* dy_it = (short*)(_dy.ptr(i));
    const float* _angle = (float*)(angle.ptr(i));
    for( int j = 0; j < width; j++ )
    {
      double angle_c = _angle[j];
      int x = dx_it[j];
      int y = dy_it[j];
      if( x == 0 and y == 0 )
      {
        dst_it[j] = 0;
      } else {
        double hyp = sqrt( x * x + y * y ) * M_SQRT1_2;
        double cosA = fabs(x / hyp);
        double sinA = fabs(y / hyp);
        int count = 0;
        double corr_angle = 0.0;
        for( int k = -m_aperture; k <= m_aperture; ++k)
        {
          if( k != 0 )
          {
            double x_1 = j + sinA * k;
            double y_1 = i + cosA * k;
            if( x_1 >= 0 and y_1 >= 0 and x_1 <= width - 1 and y_1 <= height - 1 )
            {
              corr_angle += fabs(corrAngle( utils::image::getSubPixelValue<float>( angle, x_1, y_1, 0), angle_c ) );
              ++count;
            }
          }
        }
        dst_it[j] = corr_angle * (x*x + y*y);
      }
    }
  }
}
