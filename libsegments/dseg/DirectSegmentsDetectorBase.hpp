/* $Id: DirectSegmentsDetectorBase.hpp 4054 2009-08-24 19:02:41Z cberger $ */

#ifndef _DIRECT_SEGMENTS_DETECTOR_BASE_H_
#define _DIRECT_SEGMENTS_DETECTOR_BASE_H_

#include "DirectSegmentsBase.hpp"
#include "../utils/Macros.hpp"
#include "../SegmentsDetector.hpp"

namespace libsegments {
  namespace utils {
     class IdFactory;
  }
  namespace dseg {
    /**
     * @ingroup dseg
     * This is the base class for write a segment detector.
     */
    class DirectSegmentsDetectorBase : public DirectSegmentsBase, public SegmentsDetector {
      public:
        DirectSegmentsDetectorBase(utils::IdFactory* _idMaker);
        virtual ~DirectSegmentsDetectorBase();
        /**
         * Set wether to use an adaptive step or not. Adaptive step is used to reduced the number of points used as an
         * initial search points for segments.
         */
        LS_DEFINE_PARAM_RW( bool, adaptativeStep, setAdaptativeStep );
        LS_DEFINE_PARAM_RW( bool, mergeSegment, setMergeSegment );
        LS_DEFINE_PARAM_RW( bool, computeDescriptor, setComputeDescriptor );
        virtual void detectSegments( const cv::Mat& _image, SegmentsSet& segmentStorage, SegmentDebug* _sdebug = 0) = 0;
        virtual std::vector< Segment > detectSegments( const cv::Mat& _image );
      protected:
        class LoopDetectionContext;
        void loopDetect( LoopDetectionContext* _context, double _minimumSegmentLengthScale);
        virtual void detectedSegment( SegmentHypothesis*, LoopDetectionContext* _context, GrowSegmentContext&) = 0;
      protected:
        unsigned int nextId();
        virtual bool isInASegment( LoopDetectionContext* _context, int _x, int _y );
      private:
        double m_qualityThresholdPow2;
        utils::IdFactory* m_idMaker;
        bool m_ownIdMaker;
    };
  }
}


#endif
