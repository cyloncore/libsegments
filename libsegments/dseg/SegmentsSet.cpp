/* $Id: SegmentsSet.cpp 4090 2009-09-03 19:32:19Z cberger $ */

#include "SegmentsSet.hpp"
#include "SegmentHypothesis.hpp"
#include "../utils/Macros_p.hpp"

using namespace libsegments::dseg;

SegmentsSet::SegmentsSet() {}

SegmentsSet::SegmentsSet(const SegmentsSet& _rhs)
{
  *this = _rhs;
}

SegmentsSet& SegmentsSet::operator=(const SegmentsSet& _rhs)
{
  LS_FOREACH(SegmentHypothesis* hyp, m_segments)
  {
    hyp->decRef();
    if(not hyp->isRef())
    {
      delete hyp;
    }
  }
  m_segments = _rhs.m_segments;
  LS_FOREACH(SegmentHypothesis* hyp, m_segments)
  {
    hyp->incRef();
  }
  return *this;
}

SegmentsSet::~SegmentsSet()
{
  LS_FOREACH(SegmentHypothesis* hyp, m_segments)
  {
    hyp->decRef();
    if(not hyp->isRef())
    {
      delete hyp;
    }
  }
}

void SegmentsSet::addSegment( SegmentHypothesis* _sh )
{
  m_segments.push_back( _sh );
  _sh->incRef();
}

const SegmentHypothesis* SegmentsSet::segmentAt( unsigned int _index ) const
{
  return m_segments[ _index ];
}

SegmentHypothesis* SegmentsSet::segmentAt( unsigned int _index )
{
  return m_segments[ _index ];
}

SegmentHypothesis* SegmentsSet::segmentId( unsigned int _id )
{
  LS_FOREACH( SegmentHypothesis* otherHyp, m_segments )
  {
    if( otherHyp->id() == _id )
    {
      return otherHyp;
    }
  }
  return 0;
}

unsigned int SegmentsSet::count() const
{
  return m_segments.size();
}
