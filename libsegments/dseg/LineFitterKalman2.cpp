/* $Id: LineFitterKalman2.cpp 4162 2009-10-07 13:34:37Z cberger $ */

#include "LineFitterKalman2.hpp"

#include <Eigen/Dense>

#include "../utils/Macros_p.hpp"
#include "../utils/Utils.hpp"
#include "../utils/Math.hpp"

// Uncomment the following line to active the optimized version of the kalman filter
#define OPTIMIZED_KALMAN

using namespace libsegments::dseg;

struct LineFitterKalman2::Private {
  Private() {}
  Eigen::Vector2d x;
  double angle;
  double direction;
  double obsError;
  Eigen::Vector2d vecNormal;
  Eigen::Vector4d state;
  Eigen::Matrix4d P; /* sym */ 
#ifndef OPTIMIZED_KALMAN
  Eigen::Matrix<double,2,4> H;
#endif
  inline void computeHPH( double t, Eigen::Matrix2d& _result /* sym */ ) const;
  // Fix instantiation of Private in 32bits, see http://eigen.tuxfamily.org/dox-devel/TopicStructHavingEigenMembers.html
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

void LineFitterKalman2::Private::computeHPH( double t, Eigen::Matrix2d& _result /* sym */ ) const
{
#ifndef OPTIMIZED_KALMAN
  H(0,0) = t;
  H(1,2) = t;
  _result.assign( H * P.selfadjointView<Eigen::Upper>() * H.transpose() );
#else
  double tt = t * t;
  double t2 = 2 * t;
  _result(0,0) = tt * P(0,0) + t2 * P(0,1) + P(1,1);
  _result(0,1) = tt * P(0,2) + t * ( P(0,3) + P(1,2) ) + P(1,3);
  _result(1,0) = _result(0,1);
  _result(1,1) = tt * P(2,2) + t2 * P(2,3) + P(3,3);
#endif
}


LineFitterKalman2::LineFitterKalman2(double x_o, double y_o, double angle_o) : d(new Private)
{
  // Init angles
  d->angle = angle_o;
  double cosA = cos(d->angle);
  double sinA = sin(d->angle);
  d->direction = computeDirection( d->angle );
  
  // Init models
//   d->predictModel.Q = jblas::zero_mat(4,4);
//   d->predictModel.Q = 0.1 * jblas::identity_mat(4,4);
  
  d->obsError = 1.0;
  // Initialize the state
  d->state(0) = -sinA;
  d->state(1) = x_o;
  d->state(2) = cosA;
  d->state(3) = y_o;
  d->P = Eigen::Matrix4d::Zero();
  d->P(0,0) = 0.05 * 0.05;
  d->P(1,1) = d->obsError * d->obsError;
  d->P(2,2) = 0.05 * 0.05;
  d->P(3,3) = d->obsError * d->obsError;

  d->vecNormal(0) = cosA;
  d->vecNormal(1) = sinA;
  
#ifndef OPTIMIZED_KALMAN
  d->H = jblas::zero_mat(2,4);
  d->H(0,1) = 1.0;
  d->H(1,3) = 1.0;
#endif
  update();
  
}

LineFitterKalman2::~LineFitterKalman2()
{
  delete d;
}

void LineFitterKalman2::addPoint(double t, const Eigen::Vector2d& v, double angle, double sigmaPx)
{
  d->obsError = sigmaPx * sigmaPx;
#ifndef OPTIMIZED_KALMAN
  // prepare observation matrix
  d->H(0,0) = t;
  d->H(1,2) = t;
#endif
  // Compute inovation
#ifndef OPTIMIZED_KALMAN
  Eigen::Vector2d y = v - ublas::prod(d->H, d->state);
#else
  Eigen::Vector2d y;
  y(0) = v(0) - ( t * d->state(0) + d->state(1));
  y(1) = v(1) - ( t * d->state(2) + d->state(3));
#endif
  // Compute innovation covariance
  Eigen::Matrix2d Sk; /* sym */ 
  d->computeHPH( t, Sk );
  
#if 1
  double cosN = d->state(0);
  double sinN = d->state(2);
  double invNorm2 = 1.0 / ( cosN * cosN + sinN * sinN );
  double cosN2 = cosN * cosN * invNorm2;
  double sinN2 = sinN * sinN * invNorm2;
  
  Sk(0,0) += 1.0 * cosN2 + d->obsError * sinN2; // observation noise, 1.0 is the step*step
  Sk(0,1) += (1.0 - d->obsError ) * cosN * sinN * invNorm2;
  Sk(1,0) = Sk(0,1);
  Sk(1,1) += 1.0 * sinN2 + d->obsError * cosN2;
#else
  Sk(0,0) += d->obsError;
  Sk(1,1) += d->obsError;
#endif
  // Kalman gain
  Eigen::Matrix2d Sk_inv = Sk.inverse();
#ifndef OPTIMIZED_KALMAN
  Eigen::Matrix<double,4,2> tmp = d->H.transpose() * Sk_inv;
#else
  Eigen::Matrix<double,4,2> tmp;
  tmp(0,0) = t * Sk_inv( 0,0 );
  tmp(1,0) = Sk_inv( 0,0 );
  tmp(2,0) = t * Sk_inv( 0,1 );
  tmp(3,0) = Sk_inv( 0,1 );
  tmp(0,1) = t * Sk_inv( 0,1 );
  tmp(1,1) = Sk_inv( 0,1 );
  tmp(2,1) = t * Sk_inv( 1,1 );
  tmp(3,1) = Sk_inv( 1,1 );
#endif
  Eigen::Matrix<double,4,2> Kk = d->P.selfadjointView<Eigen::Upper>() * tmp;
  // Update the state
  d->state += Kk * y;
  // Update covariance
  d->P -= Kk * Sk.selfadjointView<Eigen::Upper>() * Kk.transpose();
 
  update();
}

double LineFitterKalman2::direction() const
{
  return d->direction;
}

double LineFitterKalman2::angle() const
{
  return d->angle;
}

const Eigen::Vector2d& LineFitterKalman2::vecNormal() const
{
  return d->vecNormal;
}

Eigen::Vector2d LineFitterKalman2::pointAt( double t ) const
{
  Eigen::Vector2d point;
  point(0) = t * d->state(0) + d->state(1);
  point(1) = t * d->state(2) + d->state(3);
  return point;
}

Eigen::Matrix2d LineFitterKalman2::covAt( double t ) const
{
  Eigen::Matrix2d ht; /* sym */ 
  d->computeHPH(t, ht );
  return ht;
}

double LineFitterKalman2::errorAt( double t ) const
{
  Eigen::Matrix2d ht; /* sym */ 
  d->computeHPH(t, ht );
  Eigen::Vector2d ev = libsegments::utils::math::eigenValues(ht);
  return 3.0 * sqrt(ev(0) > ev(1) ? ev(0) : ev(1));
}

void LineFitterKalman2::dump() const
{
  LS_DEBUG( "X = " << d->state );
  LS_DEBUG( "P = " << d->P );
}

void LineFitterKalman2::merge( const LineFitterKalman2* _lfk2 )
{
//   LS_DEBUG("==================================================================");
//   LS_DEBUG( d->state );
//   LS_DEBUG( d->P );
  double u1 = d->state(0);
  double v1 = d->state(2);
  double u2 = _lfk2->d->state(0);
  double v2 = _lfk2->d->state(2);
  Eigen::Vector2d obs;
  if( (fabs(u1) > fabs(v1) and fabs( u1 - u2 ) < fabs( u1 + u2 ) )
       or fabs( v1 - v2 ) < fabs( v1 + v2 ) )
  { // the line can be in either direction and still be the same line
    obs(0) = u2;
    obs(1) = v2;
  } else {
    obs(0) = -u2;
    obs(1) = -v2;
  }  
  
  Eigen::Matrix<double,2,4> H = Eigen::Matrix<double,2,4>::Zero();
  H(0,0) = 1;
  H(1,2) = 1;
  // Compute inovation
  Eigen::Vector2d y = obs - H * d->state;
  
  // Compute innovation covariance
  Eigen::Matrix2d Sk = H * d->P * H.transpose(); /* sym */ 
  
//   LS_DEBUG( _lfk2->d->P );
  
  Sk(0,0) += _lfk2->d->P(0,0);
  Sk(0,1) += _lfk2->d->P(0,2);
  Sk(1,0) = Sk(0,1);
  Sk(1,1) += _lfk2->d->P(2,2);
  // Kalman gain
  Eigen::Matrix2d Sk_inv = Sk.inverse();
  Eigen::Matrix<double,4,2> Kk = d->P * H.transpose() * Sk_inv.selfadjointView<Eigen::Upper>();
  // Update the state
  d->state += Kk * y;
  // Update covariance
  d->P -= Kk * Sk.selfadjointView<Eigen::Upper>() * Kk.transpose();
//   LS_DEBUG( d->state );
//   LS_DEBUG( d->P );
 
  d->state(1) = 0.5 * ( d->state(1) + _lfk2->d->state(1) );
  d->state(3) = 0.5 * ( d->state(3) + _lfk2->d->state(3) );
  
  update();
  LS_DEBUG("==================================================================");
}

bool LineFitterKalman2::compatible( const LineFitterKalman2* _lfk2 ) const
{
  double u1 = d->state(0);
  double v1 = d->state(2);
  double u2 = _lfk2->d->state(0);
  double v2 = _lfk2->d->state(2);
  
  double x1 = d->state(1);
  double y1 = d->state(3);
  double x2 = _lfk2->d->state(1);
  double y2 = _lfk2->d->state(3);
    
  {
    Eigen::Vector4d df;
    df(1) = x1 - x2;
    df(3) = y1 - y2;
    if( fabs( u1 - u2 ) < fabs( u1 + u2 ) )
    { // the line can be in either direction and still be the same line
      df(0) = u1 - u2;
      df(2) = v1 - v2;
    } else {
      df(0) = u1 + u2;
      df(2) = v1 + v2;
    }
    Eigen::Matrix4d Pf = d->P; /* sym */ 
    float cosN = d->state(0);
    float sinN = d->state(2);
    Pf(1,1) += cosN * cosN * 100000;
    Pf(1,3) += cosN * sinN * 100000;
    Pf(3,1) = Pf(1,3);
    Pf(3,3) += sinN * sinN * 100000;
    LS_DEBUG( df );
    LS_DEBUG( Pf );
    Eigen::Matrix4d PfInv = Pf.inverse();
    return ( df.adjoint() * ( PfInv * df ) ) < 9.0;
  }
  
}

void LineFitterKalman2::update()
{
  // Update line parameters
  const Eigen::Vector4d& state = d->state;
  double denom = state(2) * state(1) - state(0) * state(3);
  d->x(0) = -state(2) / denom;
  d->x(1) = state(0) / denom;
  // Compute angle
  double angle = atan2( d->x(1), d->x(0) );
  
  d->direction = computeDirection( angle );
  if( corrAngle(angle, d->angle) < 0 )
  {
    if( angle < 0 ) d->angle = angle + M_PI;
    else d->angle = angle - M_PI;
  } else {
    d->angle = angle;
  }
  
  // normal
  double cosA = cos(d->angle);
  double sinA = sin(d->angle);
  d->vecNormal(0) = cosA;
  d->vecNormal(1) = sinA;
//   d->vecDirection(0) = -sinA;
//   d->vecDirection(1) = cosA;

}

const Eigen::Vector2d& LineFitterKalman2::x() const
{
  return d->x;
}

double LineFitterKalman2::quality() const
{
  return d->P.determinant();
}

Eigen::Matrix2d LineFitterKalman2::vecDirectionCov() const
{
  Eigen::Matrix2d dCov; /* sym */ 
  dCov(0,0) = d->P(0,0);
  dCov(0,1) = d->P(0,2);
  dCov(1,0) = dCov(0,1);
  dCov(1,1) = d->P(2,2);
  return dCov;
}

Eigen::Matrix2d LineFitterKalman2::originCov() const
{
  Eigen::Matrix2d oCov; /* sym */ 
  oCov(0,0) = d->P(1,1);
  oCov(0,1) = d->P(1,3);
  oCov(1,0) = oCov(0,1);
  oCov(1,1) = d->P(3,3);
  return oCov;
}

Eigen::Vector2d LineFitterKalman2::origin() const
{
  Eigen::Vector2d ori;
  ori(0) = d->state(1);
  ori(1) = d->state(3);
  return ori;
}

Eigen::Matrix2d LineFitterKalman2::originMergeCov() const
{
  Eigen::Matrix2d omCov = originCov(); /* sym */ 
  float cosN = d->state(0);
  float sinN = d->state(2);
  omCov(0,0) += cosN * cosN * 100000;
  omCov(0,1) += cosN * sinN * 100000;
  omCov(0,1) = omCov(1, 0);
  omCov(1,1) += sinN * sinN * 100000;
  return omCov;
}

bool LineFitterKalman2::isCompatibleObservation( double _x, double _y ) const
{
#ifndef LS_NDEBUG
  {
    double u = d->state(0);
    double x0 = d->state(1);
    double v = d->state(2);
    double y0 = d->state(3);
    double t = ( u * ( _x - x0 ) + v * ( _y - y0 ) ) / ( u * u + v * v );
    Eigen::Vector2d diff;
    diff(0) = _x - ( t * u + x0 );
    diff(1) = _y - ( t * v + y0 );
  
    Eigen::Matrix2d m, invm;
    d->computeHPH( t, m );
    invm = m.inverse();
    LS_DEBUG( "(_x, _y) = " << _x << " " << _y << " (x(t), y(t)) = " << ( t * u + x0 ) << " " << ( t * v + y0 ) );
    LS_DEBUG( diff << " " << m << " " << invm << " " << diff.dot(Eigen::Vector2d(invm * diff )) );
//     return ublas::inner_prod(diff, Eigen::Vector2d(ublas::prod( invm, diff ) )) < 9;
  }
#endif
  {
    double u = d->state(0);
    double x0 = d->state(1);
    double v = d->state(2);
    double y0 = d->state(3);
    double t = ( u * ( _x - x0 ) + v * ( _y - y0 ) ) / ( u * u + v * v );
    double dx = _x - ( t * u + x0 );
    double dy = _y - ( t * v + y0 );
    double err = errorAt( t );
    LS_DEBUG( err << " " << sqrt(dx * dx + dy * dy) << " t = " << t << " _x = " << _x << " " << ( t * u + x0 ) << " _y = " << _y << " " << ( t * v + y0 ) );
    return (dx * dx + dy * dy) < (err * err );
  }
}

bool LineFitterKalman2::isCompatibleDirectionObservation( double _u, double _v ) const
{
  Eigen::Vector2d diff;
  diff(0) = _u - d->state(0); 
  diff(1) = _v - d->state(2); 

  Eigen::Matrix2d m;
  
  m(0,0) = d->P(0,0);
  m(0,1) = d->P(0,2);
  m(1,0) = m(0,1);
  m(1,1) = d->P(2,2);
  Eigen::Matrix2d invm = m.inverse();
  LS_DEBUG( _u << " " << d->state(0) << " " << _v << " " << d->state(2) );
  LS_DEBUG( diff.norm() << " " << diff << " " << m << " " << invm << diff.adjoint() * Eigen::Vector2d(invm * diff ) );
  
  return (diff.adjoint() * (invm * diff )) < 9;
}

double LineFitterKalman2::distance( double _x, double _y ) const
{
  double u = d->state(0);
  double x0 = d->state(1);
  double v = d->state(2);
  double y0 = d->state(3);
  double t = ( u * ( _x - x0 ) + v * ( _y - y0 ) ) / ( u * u + v * v );
  double dx = _x - ( t * u + x0 );
  double dy = _y - ( t * v + y0 );
  return sqrt(dx * dx + dy * dy);
}

void LineFitterKalman2::setUncertainty( double sigma_u, double sigma_x0, double sigma_v, double sigma_y0 )
{
  d->P = Eigen::Matrix4d::Zero();
  d->P(0,0) = sigma_u * sigma_u;
  d->P(1,1) = sigma_x0 * sigma_x0;
  d->P(2,2) = sigma_v * sigma_v;
  d->P(3,3) = sigma_y0 * sigma_y0;  
}

const Eigen::Vector4d& LineFitterKalman2::state() const
{
  return d->state;
}
