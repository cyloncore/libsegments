/* $Id:$ */

#ifndef DEBUG_HPP
#define DEBUG_HPP

// #define INTEREST_ID 35
// #define DEBUG_COND (segCount == (INTEREST_ID))
// #define ADD_COND ( segHyp->id() == INTEREST_ID )
// #define TRACK_COND ( sh->id() == INTEREST_ID )
// #define ENABLE_SEGMENT_HYPOTHESIS_DEBUG (true)

// #define DEBUG_COND ( segCount >= 31 and segCount <= 32 )
// #define DEBUG_COND ( segCount < 4 )
// #define ADD_COND ( true )

// No debug
#define DEBUG_COND ( false )
#define ADD_COND ( true )
#define TRACK_COND ( true )
#define ENABLE_SEGMENT_HYPOTHESIS_DEBUG (false)

#endif
