/* $Id: */

#include "GradientsDescriptor.hpp"

#include <cstring>

#include "LineFitterKalman2.hpp"
#include "SegmentHypothesis.hpp"
#include "../utils/Image.hpp"
#include "../utils/Utils.hpp"

using namespace libsegments::dseg;

// A = 1.0 -  exp( - ( j - (count-1) * 0.5 ) .** 2 / (0.1*(count-1)))
static double coeffs[8] = { 0.30033, 0.95982, 0.99987, 1.00000, 1.00000, 0.99987, 0.95982, 0.30033 };

GradientsDescriptor::GradientsDescriptor( const SegmentHypothesis& _segHyp, const cv::Mat& _dx, const cv::Mat& _dy ) : m_count(8), m_subdescriptorsCount(8), m_total( m_count * m_subdescriptorsCount), m_topHisto( new double[ m_total]), m_bottomHisto( new double[ m_total])
{
  memset( m_topHisto, 0, m_total * sizeof(double));
  memset( m_bottomHisto, 0, m_total * sizeof(double));
  double tMin, tMax;
  _segHyp.computeTMinMax( tMin, tMax );
  const Eigen::Vector2d& _normal = _segHyp.lineFitter().vecNormal();
  double lineAngle = _segHyp.lineFitter().angle();
  Eigen::Vector2d _minusNormal = -_normal;
  double tStep = (tMax - tMin) / m_subdescriptorsCount;
  double ttStep = 2 * tStep;
  for( int i = 0; i < m_subdescriptorsCount; ++i)
  {
    double tStart = tMin + ( i - 0.5 ) * tStep;
    double* topHisto = m_topHisto + i * m_count;
    double* bottomHisto = m_bottomHisto + i * m_count;
    for( double t = tStart; t < (tStart + ttStep); t += 1.0 )
    {
      double coef = exp( -pow2( ( t - tStart + tStep )  / ttStep ) );
      Eigen::Vector2d startPoint = _segHyp.lineFitter().pointAt( t );
      // Fill top histo
      fillHisto( _dx, _dy, topHisto, startPoint, _normal, lineAngle, ttStep, coef  );
      // Fill bottom histo
      fillHisto( _dx, _dy, bottomHisto, startPoint, _minusNormal, lineAngle, ttStep, coef   );
    }
  }
  double topLength = 0.0;
  double bottomLength = 0.0;
  for( int i = 0;  i < m_total; ++i)
  {
    topLength += m_topHisto[i] * m_topHisto[i];
    bottomLength += m_bottomHisto[i] * m_bottomHisto[i];
  }
  if( topLength == 0.0 )
  {
    for( int i = 0;  i < m_total; ++i)
    {
      m_topHisto[i] = 1.0;
    }
    topLength = 1.0 / m_total;
  } else {
    topLength = 1.0 / sqrt( topLength );
  }
  if( bottomLength == 0.0 )
  {
    for( int i = 0;  i < m_total; ++i)
    {
      m_bottomHisto[i] = 1.0;
    }
    bottomLength = 1.0 / m_total;
  } else {
    bottomLength = 1.0 / sqrt( bottomLength );
  }
  double topLength2 = 0.0;
  double bottomLength2 = 0.0;
  for( int i = 0;  i < m_total; ++i)
  {
    
    m_topHisto[i] *= topLength;
    if( m_topHisto[i] > 0.2 ) m_topHisto[i] = 0.2;
    if( m_topHisto[i] < 0.0 ) m_topHisto[i] = 0.0;
    m_topHisto[i] *= coeffs[i % 8];
    topLength2 += m_topHisto[i] * m_topHisto[i];
    
    m_bottomHisto[i] *= bottomLength;
    if( m_bottomHisto[i] > 0.2 ) m_bottomHisto[i] = 0.2;
    if( m_bottomHisto[i] < 0.0 ) m_bottomHisto[i] = 0.0;
    m_bottomHisto[i] *= coeffs[i % 8];
    bottomLength2 += m_bottomHisto[i] * m_bottomHisto[i];
  }
  topLength2 = 1.0 / sqrt( topLength2 );
  bottomLength2 = 1.0 / sqrt( bottomLength2 );
  for( int i = 0;  i < m_total; ++i)
  {
    m_topHisto[i] *= topLength2;
    m_bottomHisto[i] *= bottomLength2;
  }
}

GradientsDescriptor::~GradientsDescriptor()
{
  delete m_topHisto;
  delete m_bottomHisto;
}

void GradientsDescriptor::fillHisto( const cv::Mat& _dx, const cv::Mat& _dy, double* _histo, const Eigen::Vector2d& _startPoint, const Eigen::Vector2d& _direction, double _lineAngle, int _length, double _coef  )
{
  Eigen::Vector2d currentPoint = _startPoint + 3.0 * _direction;
  double lastNorm = DBL_MAX;
  for(int i = 0; i < _length; ++i)
//   while(true)
  {
    int ix = int(currentPoint(0));
    int iy = int(currentPoint(1));
    if( not check(ix, _dx.cols - 1) or not check(iy, _dx.rows - 1) ) return;
    int dx = utils::image::getPixelValue<short>( _dx, ix, iy, 0 );
    int dy = utils::image::getPixelValue<short>( _dy, ix, iy, 0 );
//     double dx = _dx.getSubPixelValue<short>( ix, iy, 0, JfrImage_INTERP_CUBIC );
//     double dy = _dy.getSubPixelValue<short>( ix, iy, 0, JfrImage_INTERP_CUBIC );
    double norm = sqrt( dx * dx + dy * dy );
//     if( norm > lastNorm) return ;
    double angle = atan2(dy, dx);
//     double diff = cos( angle - _lineAngle);
    double correctedAngle = ( angle - _lineAngle);
//     int idx = int( (m_count) * ( 1.0 + diff) * 0.5);
//     if( idx >= m_count ) idx = m_count - 1;
//     if( idx != 0 and idx != (m_count - 1 ) )
    for(int j = 0; j < m_count; ++j )
    {
//       double lnorm = norm * ( 1.0 - fabs( fpow( -1.0 + 2.0 * j / (m_count-1) - diff ), 2.0) );
//       double lnorm = norm * exp( -pow( -1.0 + 2.0 * j / (m_count-1) - diff, 2.0) );
//       JFR_DEBUG( j << " " << exp( -pow2( cos( M_PI * j / (m_count-1) - 0.5 * correctedAngle ) ) ) );
      double lnorm = norm * _coef * exp( -pow2( cos( M_PI * j / (m_count-1) - 0.5 * correctedAngle ) ) );
      _histo[ j ] += lnorm;
    }
    lastNorm = norm;
    currentPoint += _direction;
  }
}

double histo2histo( const double* _h1, const double* _h2, int _count, double _normalizeCoef, bool verbose)
{
  if( verbose)
  {
    std::cout << "============ " << _h1 << " === " << _h2 << " ============" << std::endl;
  }
  double v = 0.0;
  for(int i = 0; i < _count; i++)
  {
    double vb = _h1[i] - _h2[i];
    v += vb * vb;
    if( verbose )
    {
      std::cout << i << " " << v << " " << vb << " " << " " << _h1[i] << " " << _h2[i] << std::endl;
    }
  }
  return 1.0 - sqrt(v) * _normalizeCoef;
}

double GradientsDescriptor::comparison( const GradientsDescriptor* _rhs, bool verbose ) const
{
//   return 0.5 * (histo2histo( m_topHisto, _rhs->m_topHisto, m_total, 1.0, verbose ) + histo2histo( m_bottomHisto, _rhs->m_bottomHisto, m_total, 1.0, verbose ) );
  return fmax(histo2histo( m_topHisto, _rhs->m_topHisto, m_total, 1.0, verbose ), histo2histo( m_bottomHisto, _rhs->m_bottomHisto, m_total, 1.0 , verbose ) );
}

void GradientsDescriptor::dump()
{
  for( int i = 0; i < m_total; ++i)
  {
    std::cout << i << " " << m_topHisto[ i] << " " << m_bottomHisto[i] << std::endl;
  }
}
