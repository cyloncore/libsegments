/* $Id: */

#ifndef _DSEG_GRADIENTS_DESCRIPTOR_HPP_
#define _DSEG_GRADIENTS_DESCRIPTOR_HPP_

#include <Eigen/Core>

namespace cv {
  class Mat;
}
namespace libsegments {
  namespace dseg {
    class SegmentHypothesis;
    class GradientsDescriptor {
      public:
        GradientsDescriptor( const SegmentHypothesis& _segHyp, const cv::Mat& _dx, const cv::Mat& _dy );
        ~GradientsDescriptor();
        double comparison( const GradientsDescriptor* _rhs, bool verbose = false ) const;
        void dump();
      private:
        void fillHisto( const cv::Mat& _dx, const cv::Mat& _dy, double* _histo, const Eigen::Vector2d& _startPoint, const Eigen::Vector2d& _direction, double _lineAngle, int _length, double _coef );
      private:
        int m_count;
        int m_subdescriptorsCount;
        int m_total;
        double* m_topHisto;
        double* m_bottomHisto;
    };
  }
}

#endif
