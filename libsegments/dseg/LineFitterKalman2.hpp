/* $Id: LineFitterKalman2.hpp 4162 2009-10-07 13:34:37Z cberger $ */

#ifndef _LINE_FITTER_KALMAN2_H_
#define _LINE_FITTER_KALMAN2_H_

#include "Eigen/Core"

namespace libsegments {
  namespace dseg {
    /**
     * @internal
     * This class is used to estimate the parameters of segments using a Kalman filter.
     */
    class LineFitterKalman2 {
      public:
        /**
         * Create a LineFitter
         * @param x_o x coordinate of the first point
         * @param y_o y coordinate of the second point
         */
        LineFitterKalman2(double x_o, double y_o, double angle_o);
        ~LineFitterKalman2();
        /**
         * This function is called when a new point is detected
         * @param t distance from the first point
         * @param _point position of the point
         * @param _angle angle of the gradient around the point
         * @param step step used to find the maximum of gradient
         */
        void addPoint(double t, const Eigen::Vector2d& _point, double angle, double sigmaPx);
        /**
         * @return the current direction of the line between [ -PI/2, PI/2]
         */
        double direction() const;
        /**
         * @return the angle of the line between [-PI, PI]
         */
        double angle() const;
        /**
         * @return the vector normal
         */
        const Eigen::Vector2d& vecNormal() const;
        /**
         * @return the point at positon @p t
         */
        Eigen::Vector2d pointAt( double t ) const;
        /**
         * @return the error at positon @p t
         */
        double errorAt( double t ) const;
      private:
        Eigen::Matrix2d /* sym */ covAt( double t ) const;
      public:
        void dump() const;
        /**
         * Merge two segments.
         */
        void merge( const LineFitterKalman2* );
        /**
         * @return true if @p _lf is compatible for merging with this segment
         */
        bool compatible( const LineFitterKalman2* _lf ) const;
        const Eigen::Vector2d& x() const;
        /**
         * @return the determinant of the covariance matrix
         */
        double quality() const;
      private:
        /**
         * @return the covariance matrix of the direction vector
         */
        Eigen::Matrix2d /* sym */ vecDirectionCov() const;
        /**
         * @return the covariance matrix of the origin
         */
        Eigen::Matrix2d /* sym */ originCov() const;
        Eigen::Matrix2d /* sym */ originMergeCov() const;
      public:
        /**
         * @return the origin
         */
        Eigen::Vector2d origin() const;
        /**
         * Check if an observation is compatible with this parameters
         */
        bool isCompatibleObservation( double _x, double _y ) const;
        /**
         * Check if a direction is compatible with this parameters
         */
        bool isCompatibleDirectionObservation( double _u, double _v ) const;
        /**
         * @return the distance between the line and (\ref x , \ref y )
         */
        double distance( double _x, double _y ) const;
        /**
         * Set the uncertainty level of this segment
         */
        void setUncertainty( double sigma_u, double sigma_x0, double sigma_v, double sigma_y0 );
        /**
         * @return the kalman state
         */
        const Eigen::Vector4d& state() const;
      private:
        void update();
        struct Private;
        Private* const d;
    };
  }
}

#endif
