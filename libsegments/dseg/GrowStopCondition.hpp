/* $Id: GrowStopCondition.hpp 3955 2009-08-03 09:56:01Z cberger $ */
#ifndef _DSEG__GROW_STOP_CONDITION_H_
#define _DSEG_GROW_STOP_CONDITION_H_

namespace libsegments {
  namespace dseg {
    /**
     * @ingroup dseg
     * This class control when the segment detection should stop growing
     * a segment.
     */
    class GrowStopCondition {
      public:
        virtual ~GrowStopCondition();
        virtual bool shouldStopPositiveSearch() const = 0;
        virtual bool shouldStopNegativeSearch() const = 0;
        virtual void reset() = 0;
        virtual void reached( double t ) = 0;
    };
  }
}

#endif
