/* $Id: LoopDetectionContext.cpp 3889 2009-07-17 16:25:23Z cberger $ */

#include "LoopDetectionContext.hpp"

using namespace libsegments::dseg;

DirectSegmentsDetectorBase::LoopDetectionContext::LoopDetectionContext( SegmentsSet* segmentStorage, const cv::Mat& proba, const cv::Mat& dx, const cv::Mat& dy, cv::Mat& inasegment
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
        , const cv::Mat& _image
#endif
) : m_proba(proba), m_dx(dx), m_dy(dy), m_inasegment(inasegment), m_segmentStorage(segmentStorage)
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
      , m_image(_image)
#endif
{
}

