/* $Id: SegmentHypothesisDebug.cpp 3657 2009-02-26 16:20:24Z cberger $ */

#include "SegmentHypothesisDebug.hpp"

using namespace libsegments::dseg;

SearchPointLineDebug::SearchPointLineDebug() : m_selectedIndex(-1)
{
}

void SearchPointLineDebug::addPoint( double _x, double _y, double _angle, double _grad, double _gray )
{
  PointInfo pi;
  pi.point(0) = _x;
  pi.point(1) = _y;
  pi.angle = _angle;
  pi.grad = _grad;
  pi.gray = _gray;
  m_points.push_back( pi );
}

int SearchPointLineDebug::count() const
{
  return m_points.size();
}

Eigen::Vector2d SearchPointLineDebug::pointAt( int _index )
{
  return m_points[ _index ].point;
}

double SearchPointLineDebug::angleAt( int _index )
{
  return m_points[ _index ].angle;
}

double SearchPointLineDebug::gradAt( int _index )
{
  return m_points[ _index ].grad;
}

double SearchPointLineDebug::grayAt( int _index )
{
  return m_points[ _index ].gray;
}

void SearchPointLineDebug::lastIsSelected(int _offset)
{
  m_selectedIndex = m_points.size() + _offset - 1;
}

int SearchPointLineDebug::selectedIndex() const
{
  return m_selectedIndex;
}

int SegmentHypothesisDebug::searchPointLinesCount() const
{
  return m_searchPointLines.size();
}

void SegmentHypothesisDebug::addSearchPointLine( const SearchPointLineDebug& _spl)
{
  m_searchPointLines.push_back( _spl );
}

const SearchPointLineDebug& SegmentHypothesisDebug::searchPointLineAt( unsigned int index )
{
  return m_searchPointLines[ index ];
}

void SegmentHypothesisDebug::merge( const SegmentHypothesisDebug& shd)
{
  m_searchPointLines.insert( m_searchPointLines.end(), shd.m_searchPointLines.begin(), shd.m_searchPointLines.end() );
}
