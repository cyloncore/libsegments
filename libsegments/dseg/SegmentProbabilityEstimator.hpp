/* $Id: SegmentProbabilityEstimator.hpp 3220 2008-10-09 08:16:58Z cberger $ */

#ifndef _SEGMENT_PROBABILITY_ESTIMATOR_H_
#define _SEGMENT_PROBABILITY_ESTIMATOR_H_

#include <opencv2/opencv.hpp>

namespace libsegments {
  namespace dseg {
    class SegmentProbabilityEstimator {
      public:
        virtual ~SegmentProbabilityEstimator();
        virtual void compute( const cv::Mat& _dx, const cv::Mat& _dy, cv::Mat& _proba ) = 0;
    };
    class SobelSegmentProbabilityEstimator : public SegmentProbabilityEstimator {
      public:
        virtual ~SobelSegmentProbabilityEstimator();
        virtual void compute( const cv::Mat& _dx, const cv::Mat& _dy, cv::Mat& _proba );
    };
    class RotationProbabilitySegmentProbabilityEstimator : public SegmentProbabilityEstimator {
      public:
        virtual ~RotationProbabilitySegmentProbabilityEstimator();
        virtual void compute( const cv::Mat& _dx, const cv::Mat& _dy, cv::Mat& _proba );
    };
    class DirRotationProbabilitySegmentProbabilityEstimator : public SegmentProbabilityEstimator {
      public:
        virtual ~DirRotationProbabilitySegmentProbabilityEstimator();
        virtual void compute( const cv::Mat& _dx, const cv::Mat& _dy, cv::Mat& _proba );
    };
    class DirRotationProbability2SegmentProbabilityEstimator : public SegmentProbabilityEstimator {
      public:
        DirRotationProbability2SegmentProbabilityEstimator(int _aperture = 2);
        virtual ~DirRotationProbability2SegmentProbabilityEstimator();
        virtual void compute( const cv::Mat& _dx, const cv::Mat& _dy, cv::Mat& _proba );
      private:
        int m_aperture;
    };
    class SobelDirRotationProbability2SegmentProbabilityEstimator : public SegmentProbabilityEstimator {
      public:
        SobelDirRotationProbability2SegmentProbabilityEstimator(int _aperture = 2);
        virtual ~SobelDirRotationProbability2SegmentProbabilityEstimator();
        virtual void compute( const cv::Mat& _dx, const cv::Mat& _dy, cv::Mat& _proba );
      private:
        int m_aperture;
    };
  }
}


#endif
