/* $Id: DetectorGrowStopCondition.hpp 3664 2009-03-03 13:04:49Z cberger $ */

#ifndef _DSEG_DETECTOR_GROW_STOP_CONDITION_HPP_
#define _DSEG_DETECTOR_GROW_STOP_CONDITION_HPP_

#include "GrowStopCondition.hpp"

namespace libsegments {
  namespace dseg {
    class DetectorGrowStopCondition : public GrowStopCondition {
      public:
        DetectorGrowStopCondition();
        virtual ~DetectorGrowStopCondition();
        virtual bool shouldStopPositiveSearch() const;
        virtual bool shouldStopNegativeSearch() const;
        virtual void reset();
        virtual void reached( double t );
      private:
        bool m_shouldStopNegativeSearch, m_shouldStopPositiveSearch;
    };
  }
}

#endif
