#include "DirectSegmentsDetector.hpp"

#include "LineFitterKalman2.hpp"
#include "../utils/Macros_p.hpp"
#include "../utils/Utils.hpp"
#include "SegmentHypothesis.hpp"
#include "SegmentsSet.hpp"
#include "SegmentProbabilityEstimator.hpp"
#include "SegmentHypothesisDebug.hpp"
#include "GradientsDescriptor.hpp"

#include "Debug.hpp"

#include "DetectorGrowStopCondition.hpp"
#include "LoopDetectionContext.hpp"

using namespace libsegments::dseg;

DirectSegmentsDetector::DirectSegmentsDetector(libsegments::utils::IdFactory* _idMaker) : DirectSegmentsDetectorBase(_idMaker)
{
}

void DirectSegmentsDetector::detectSegments( const cv::Mat& _image, SegmentsSet& _segmentStorage, SegmentDebug* _sdebug)
{
  segCount = 0;
  int width = _image.cols;
  int height = _image.rows;
  cv::Mat dx( height, width, CV_MAKETYPE(CV_16S, 1) );
  cv::Mat dy( height, width, CV_MAKETYPE(CV_16S, 1) );
  cv::Mat proba( height, width, CV_MAKETYPE(CV_32F, 1) );
  cv::Mat inasegment( height, width, CV_MAKETYPE(CV_32S, 1) );
  
  if( _sdebug )
  {
    _sdebug->inasegment = inasegment;
    _sdebug->proba = proba;
    _sdebug->dx = dx;
    _sdebug->dy = dy;
  }
  
  memset( inasegment.data, 0, inasegment.rows * inasegment.step);
  
  for(std::size_t i = 0; i < _segmentStorage.count(); ++i )
  {
    const SegmentHypothesis* sh = _segmentStorage.segmentAt( i );
    
    coloriage( sh->x1(), sh->y1(), sh->x2(), sh->y2(), inasegment, sh->id() );
  }
  
  cv::Sobel( _image, dx, CV_16S, 1, 0, 3 );
  cv::Sobel( _image, dy, CV_16S, 0, 1, 3 );
  // Compute maximum probability
  segmentProbabilityEstimator()->compute( dx, dy, proba  );
  
  DirectSegmentsDetectorBase::LoopDetectionContext context(&_segmentStorage, proba, dx, dy, inasegment
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
      , _image
#endif
                                                            );
  loopDetect( &context, 1.0 );
  
  if( computeDescriptor() )
  {
    for( unsigned int i = 0; i < _segmentStorage.count(); ++i )
    {
      SegmentHypothesis* segHyp = _segmentStorage.segmentAt( i );
      segHyp->setDescriptor( new GradientsDescriptor( *segHyp, dx, dy ) );
    }
  }

  
}

void DirectSegmentsDetector::detectedSegment( SegmentHypothesis* segHyp , DirectSegmentsDetectorBase::LoopDetectionContext* _context, GrowSegmentContext& gsc)
{
  //---- Add the segment to set of points ----//
  // Read the extremities
  double x_1 = segHyp->x1(); double y_1 = segHyp->y1();
  double x_2 = segHyp->x2(); double y_2 = segHyp->y2();
  LS_DEBUG_COND(DEBUG_COND, x_1 << " " << y_1 << " " << x_2 << " " << y_2 );
  LS_DEBUG_COND(DEBUG_COND, "Line parameters: " << segHyp->lineFitter().x() );
  LS_DEBUG_COND(DEBUG_COND, "Line parameters: " << segHyp->lineParameters() );
  LS_DEBUG_COND(DEBUG_COND, "Line parameters: " << (segHyp->lineFitter().x()(0) * x_1 + segHyp->lineFitter().x()(1) * y_1 + 1.0));
  LS_DEBUG_COND(DEBUG_COND, "Line parameters: " << (segHyp->lineFitter().x()(0) * gsc.x_1 + segHyp->lineFitter().x()(1) * gsc.y_1 + 1.0));
  LS_DEBUG_COND(DEBUG_COND, "Line parameters: " << (segHyp->lineFitter().x()(0) * x_2 + segHyp->lineFitter().x()(1) * y_2 + 1.0));
  LS_DEBUG_COND(DEBUG_COND, "Line parameters: " << (segHyp->lineFitter().x()(0) * gsc.x_2 + segHyp->lineFitter().x()(1) * gsc.y_2 + 1.0));
  //---- Coloriage ----//
  // Create line segment
  SegmentHypothesis* mergedWith = 0;
  if( mergeSegment() )
  {
    mergedWith = attemptToMerge( segHyp, _context->inasegment(), *_context->segmentStorage(), gsc.segmentToCount );
  }
  if( mergedWith )
  {
    coloriage( x_1, y_1, x_2, y_2, _context->inasegment(), mergedWith->id() );
    delete segHyp;
  } else if( segHyp->squareLength() > minimumSegmentLengthPow2() ) {
  //                 segHyp->setId( segCount++ );
      segHyp->setId( nextId() );
  //               JFR_DEBUG(segHyp->lineFitter().quality());
      coloriage( x_1, y_1, x_2, y_2, _context->inasegment(), segHyp->id() );
      if( ADD_COND )
      {
        _context->segmentStorage()->addSegment( segHyp );
      }
      ++segCount;
  } else {
    coloriage( x_1, y_1, x_2, y_2, _context->inasegment(), -1);
    delete segHyp;
  }
}