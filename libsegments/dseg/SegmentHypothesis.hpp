/* $Id: SegmentHypothesis.hpp 4162 2009-10-07 13:34:37Z cberger $ */

#ifndef _SEGMENTS_HYPOTHESIS_H_
#define _SEGMENTS_HYPOTHESIS_H_

#include <Eigen/Core>

namespace libsegments {
  namespace dseg {
    class LineFitterKalman2;
    class SegmentHypothesisDebug;
    class GradientStatsDescriptor;
    class GradientsDescriptor;
    /**
     * @ingroup dseg
     * This class contains information relative to a segment.
     */
    class SegmentHypothesis {
      friend class SegmentsSet;
      public:
        SegmentHypothesis(double x_o, double y_o, double angle_o);
        ~SegmentHypothesis();
        /**
         * Add an observation of the segment
         * @param _t distance to the origin (usefull for parametric representation of a segment, like in \ref LineFitterKalman2 )
         * @param _x x-coordinate of the observation
         * @param _y y-coordinate of the observation
         * @param _angle angle arround the point
         */
        void addPoint(double _t, double _x, double _y, double _angle, double sigmaPx);
        const Eigen::Vector2d& lineParameters() const;
        void dump(bool _verbose) const;
        /**
         * @return the number of points used to generate this segment
         * hypothesis.
         */
        int countPoints() const;
        /**
         * Set the first extremity, the coordinates given as parameters
         * will be projected on the line.
         */
        void setExtremity1(double _x1, double _y1);
        double x1() const;
        double y1() const;
        double t1() const;
        /**
         * Set the second extremity, the coordinates given as parameters
         * will be projected on the line.
         */
        void setExtremity2(double _x2, double _y2);
        double x2() const;
        double y2() const;
        double t2() const;
        /**
         * @return the square length of the segment
         */
        double squareLength() const;
        void merge(const SegmentHypothesis& rhs);
        const LineFitterKalman2& lineFitter() const;
        unsigned int id() const;
        void setId( unsigned int _id );
        const SegmentHypothesisDebug* segmentHypothesisDebug() const;
        SegmentHypothesisDebug* segmentHypothesisDebug();
        void setSegmentHypothesisDebug( SegmentHypothesisDebug* );
        bool isCompatible( const SegmentHypothesis& ) const;
        void setUncertainty( double sigma_u, double sigma_x0, double sigma_v, double sigma_y0 );
        GradientStatsDescriptor& gradientDescriptor();
        const GradientStatsDescriptor& gradientDescriptor() const;
        void setDescriptor( GradientsDescriptor* _descriptor );
        const GradientsDescriptor* descriptor( ) const;
        /**
         * Apply a scale on a segment.
         */
        void applyScale(double v);
        /**
         * Compute the minimal and maximal value of t.
         */
        void computeTMinMax( double& _tMin, double& _tMax ) const;
      private: // Used for memory management purpose in SegmentSet
        void incRef();
        void decRef();
        bool isRef();
      private:
        void solve() const;
      private:
        struct Private;
        Private* const d;
    };
  }
}

#endif
