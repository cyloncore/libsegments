/* $Id: SegmentsSet.hpp 4085 2009-09-03 13:14:49Z cberger $ */

#ifndef _SEGMENTS_SET_HPP_
#define _SEGMENTS_SET_HPP_

#include <vector>

namespace libsegments {
  namespace dseg {
    class SegmentHypothesis;
    /**
     * This class contains a list of \ref SegmentHypothesis .
     * 
     * @ingroup dseg
     */
    class SegmentsSet {
      public:
        SegmentsSet();
        SegmentsSet(const SegmentsSet&);
        SegmentsSet& operator=(const SegmentsSet& _rhs);
        ~SegmentsSet();
        /**
         * Add a segment to the set.
         */
        void addSegment( SegmentHypothesis* );
        /**
         * @return the \ref SegmentHypothesis at a given index.
         */
        SegmentHypothesis* segmentAt( unsigned int _index );
        const SegmentHypothesis* segmentAt( unsigned int _index ) const;
        SegmentHypothesis* segmentId( unsigned int _id );
        /**
         * @return the number of segments in the set.
         */
        unsigned int count() const;
      private:
        std::vector< SegmentHypothesis* > m_segments;
    };
  }
}

#endif
