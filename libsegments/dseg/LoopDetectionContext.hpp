/* $Id: LoopDetectionContext.hpp 3685 2009-03-09 16:44:32Z cberger $ */

#ifndef _DSEG_LoopDetectionContext_H_
#define _DSEG_LoopDetectionContext_H_

#include "DirectSegmentsDetectorBase.hpp"
#include "Debug.hpp"

namespace libsegments {
  namespace dseg {
    class DirectSegmentsDetectorBase::LoopDetectionContext
    {
      public:
        LoopDetectionContext( SegmentsSet* segmentStorage, const cv::Mat& proba, const cv::Mat& dx, const cv::Mat& dy, cv::Mat& inasegment
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
                              , const cv::Mat& image
#endif
        );
        inline const cv::Mat& proba() const { return m_proba; }
        inline const cv::Mat& dx() const { return m_dx; }
        inline const cv::Mat& dy() const { return m_dy; }
        inline const cv::Mat& inasegment() const { return m_inasegment; }
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
        inline const cv::Mat& image() const { return m_image; }
#endif
        inline cv::Mat& inasegment() { return m_inasegment; }
        SegmentsSet* segmentStorage() { return m_segmentStorage; }
      private:
        cv::Mat m_proba;
        cv::Mat m_dx;
        cv::Mat m_dy;
        cv::Mat m_inasegment;
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
        cv::Mat m_image;
#endif
        SegmentsSet* m_segmentStorage;
    };
  }
}

#endif
