/* $Id: DetectorGrowStopCondition.cpp 3664 2009-03-03 13:04:49Z cberger $ */

#include "DetectorGrowStopCondition.hpp"

using namespace libsegments::dseg;

DetectorGrowStopCondition::DetectorGrowStopCondition()
{
  reset();
}

DetectorGrowStopCondition::~DetectorGrowStopCondition()
{
}

bool DetectorGrowStopCondition::shouldStopPositiveSearch() const
{
  return m_shouldStopPositiveSearch;
}
bool DetectorGrowStopCondition::shouldStopNegativeSearch() const
{
  return m_shouldStopNegativeSearch;
}
void DetectorGrowStopCondition::reset()
{
  m_shouldStopPositiveSearch = true;
  m_shouldStopNegativeSearch = true;
}
void DetectorGrowStopCondition::reached( double t )
{
  if( t < 0.0 )
  {
    m_shouldStopNegativeSearch = false;
  } else {
    m_shouldStopPositiveSearch = false;
  }
}
