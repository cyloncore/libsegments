#include "DirectSegmentsBase.hpp"

#include "SegmentProbabilityEstimator.hpp"
#include "LineFitterKalman2.hpp"
#include "SegmentHypothesis.hpp"
#include "../utils/Utils.hpp"
#include "../utils/Image.hpp"
#include "SegmentHypothesisDebug.hpp"
#include "SegmentsSet.hpp"
#include "GrowStopCondition.hpp"
#include "GradientStatsDescriptor.hpp"

#include "Debug.hpp"

using namespace libsegments::dseg;

DirectSegmentsBase::DirectSegmentsBase() : m_searchNbSteps(2), m_maximalSearchStep(1.0), m_thresholdGradColinearity(0.95), m_thresholdMaxGradient(10), m_segmentProbabilityEstimator( new SobelSegmentProbabilityEstimator ), m_maximalSearchLength(2.0), m_minimumSegmentLengthPow2(0.0)
{
  setMinimumSegmentLength( 10 );
}

DirectSegmentsBase::~DirectSegmentsBase()
{
}

#define CHECK( v, max) if(not check((v), (max))) { return false; }

bool DirectSegmentsBase::smartSearchPoint( GrowSegmentContext& _context, double _searchStep, const Eigen::Vector2d& _pi, double& _xr, double& _yr, double& _angler, int& _bestI, int _searchNbSteps)
{
  double _angle = _context.segHyp->lineFitter().angle();
  const Eigen::Vector2d& _normal = _context.segHyp->lineFitter().vecNormal();
  int maxX = _context.dx.cols - 1;
  int maxY = _context.dy.rows - 1;
  double bestAngleCorr = thresholdGradColinearity();
  double searchStepX = _searchStep * _normal(0);
  double searchStepY = _searchStep * _normal(1);
  
  // Init variables used for searching on the positive i
  double xi_p = _pi(0);
  double yi_p = _pi(1);
  double xim1_p = xi_p - searchStepX;
  double yim1_p = yi_p - searchStepY;
  double xip1_p = xi_p + searchStepX;
  double yip1_p = yi_p + searchStepY;
  
  if( not ( check( xim1_p - 0.5, maxX ) and check( yim1_p - 0.5, maxY ) and check( xi_p - 0.5, maxX ) and check( yi_p - 0.5, maxY )  and check( xip1_p - 0.5, maxX ) and check( yip1_p - 0.5, maxY ) ) )
  {
    return false;
  }
  
  
  // Init probability values for the positive i
  double probaip1_p = utils::image::getSubPixelValue<float>( _context.proba, xip1_p - 0.5, yip1_p - 0.5, 0, utils::image::INTERP_CUBIC );
  double probai_p = utils::image::getSubPixelValue<float>( _context.proba, xi_p - 0.5, yi_p - 0.5, 0, utils::image::INTERP_CUBIC );
  double probaim1_n = utils::image::getSubPixelValue<float>( _context.proba, xim1_p - 0.5, yim1_p - 0.5, 0, utils::image::INTERP_CUBIC );
  
  // Check if we have a maximum around 0 that fullfill the constraints
  if( probai_p > probaip1_p and probai_p > probaim1_n)
  {
    double anglei = utils::image::sampleAngle( _context.dx, _context.dy, xi_p, yi_p );
    double cAngle = corrAngle( anglei, _angle );
    if( cAngle > bestAngleCorr )
    {
      _bestI = 0;
      _xr = xi_p;
      _yr = yi_p;
      _angler = anglei;
      _context.segHyp->gradientDescriptor().appendGradient( probai_p );
      return true;
    }
  }
  // Move of one step the position on positive i
  xi_p = xip1_p;
  yi_p = yip1_p;
  // Init variables used for searching on the negative i
  double xi_n = xim1_p;
  double yi_n = yim1_p;
  
  // Init probability values for the negative i
  double probai_n = probai_p;
  
  for( int i = 1; i <= _searchNbSteps; ++i)
  {
    double xip1_p = xi_p + searchStepX;
    double yip1_p = yi_p + searchStepY;
    double xim1_n = xi_n - searchStepX;
    double yim1_n = yi_n - searchStepY;
    
    double probaip2_p = FLT_MAX;
    if( check( xip1_p - 0.5, maxX ) and check( yip1_p - 0.5, maxY ) )
    {
      probaip2_p = utils::image::getSubPixelValue<float>( _context.proba, xip1_p - 0.5, yip1_p - 0.5, 0, utils::image::INTERP_CUBIC );
    }
    double probaim2_n = FLT_MAX;
    if( check( xim1_n - 0.5, maxX ) and check( yim1_n - 0.5, maxY ) )
    {
      probaim2_n = utils::image::getSubPixelValue<float>( _context.proba, xim1_n - 0.5, yim1_n - 0.5, 0, utils::image::INTERP_CUBIC );
    }
    
    bool consistant_p = false;
    bool consistant_n = false;
    
    double angle_p = NAN , angle_n = NAN , corrAngle_p = NAN , corrAngle_n = NAN ;
          
    if( probaip2_p < probaip1_p and probai_p < probaip1_p )
    {
      angle_p = utils::image::sampleAngle( _context.dx, _context.dy, xi_p, yi_p );
      corrAngle_p = corrAngle( angle_p, _angle );
      consistant_p = corrAngle_p > bestAngleCorr;
    }
    
    
    if( probaim2_n < probaim1_n and probai_n < probaim1_n )
    {
      angle_n = utils::image::sampleAngle( _context.dx, _context.dy, xi_n, yi_n );
      corrAngle_n = corrAngle( angle_n, _angle );
      consistant_n = corrAngle_n > bestAngleCorr;
    }
    
    // Both negative and positive search are likely candidate
    if( consistant_p and consistant_n )
    {
      if( corrAngle_p > corrAngle_n )
      {
        consistant_n = false;
      } else {
        consistant_p = false;
      }
    }
    
    if( consistant_p )
    {
      _bestI = i;
      _xr = xi_p;
      _yr = yi_p;
      _angler = angle_p;
      _context.segHyp->gradientDescriptor().appendGradient( probaip1_p );
      return true;
    } else if( consistant_n )
    {
      _bestI = i;
      _xr = xi_n;
      _yr = yi_n;
      _angler = angle_n;
      _context.segHyp->gradientDescriptor().appendGradient( probaim1_n );
      return true;
    }
    
    // Update positive coordinates
    xi_p = xip1_p;
    yi_p = yip1_p;
    
    // Update positive probas
    probai_p = probaip1_p;
    probaip1_p = probaip2_p;
    
    // Update negative
    xi_n = xim1_n;
    yi_n = yim1_n;
    
    // Update negative probas
    probai_n = probaim1_n;
    probaim1_n = probaim2_n;
  }
  return false;
}

bool DirectSegmentsBase::searchPoint( GrowSegmentContext& _context, double _searchStep, const Eigen::Vector2d& _pi, double& _xr, double& _yr, double& _angler, int& _bestI, int _searchNbSteps)
{
  LS_DEBUG_COND(DEBUG_COND, segCount << " " << _pi << " " << _context.segHyp->lineFitter().vecNormal() );
  double _angle = _context.segHyp->lineFitter().angle();
  const Eigen::Vector2d& _normal = _context.segHyp->lineFitter().vecNormal();
  int maxX = _context.dx.cols - 1;
  int maxY = _context.dy.rows - 1;
  bool success = false;
  double bestAngleCorr = thresholdGradColinearity();
  int bestI = _searchNbSteps + 1;
  int i = -_searchNbSteps;
  double searchStepX = _searchStep * _normal(0);
  double searchStepY = _searchStep * _normal(1);
  double xim1 = _pi(0) + _normal(0) * (i-1) * _searchStep;
  double yim1 = _pi(1) + _normal(1) * (i-1) * _searchStep;
  double xi = xim1 + searchStepX;
  double yi = yim1 + searchStepY;
  double bestProaba = 0.0;
  for( ; i <= _searchNbSteps; ++i )
  {
    if( check( xim1 - 0.5, maxX ) and check( yim1 - 0.5, maxY ) and check( xi - 0.5, maxX ) and check( yi - 0.5, maxY ) )
    {
      break;
    }
    xim1 = xi;
    yim1 = yi;
    xi += searchStepX;
    yi += searchStepY;
  }
  if( i > _searchNbSteps) return false;
  
  double probaim1 = utils::image::getSubPixelValue<float>( _context.proba, xim1 - 0.5, yim1 - 0.5, 0, utils::image::INTERP_CUBIC );
  double probai = utils::image::getSubPixelValue<float>( _context.proba, xi - 0.5, yi - 0.5, 0, utils::image::INTERP_CUBIC );
  
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
  SearchPointLineDebug searchPointLine;
  searchPointLine.addPoint( xim1, yim1, sampleAngle( _context.dx, _context.dy, xim1, yim1 ), probaim1, _context.originalImage.getSubPixelValue<uint8_t>( xim1 - 0.5, yim1 - 0.5, 0, JfrImage_INTERP_CUBIC ) );
  searchPointLine.addPoint( xi, yi, sampleAngle( _context.dx, _context.dy, xi, yi ), probai, _context.originalImage.getSubPixelValue<uint8_t>( xi - 0.5, yi - 0.5, 0, JfrImage_INTERP_CUBIC ) );
#endif
  
  for( ; i <= _searchNbSteps; ++i )
  {
    double xip1 = xi + searchStepX;
    double yip1 = yi + searchStepY;
    if( check( xip1 - 0.5, maxX ) and check( yip1 - 0.5, maxY ) )
    {
      double probaip1 = utils::image::getSubPixelValue<float>( _context.proba, xip1 - 0.5, yip1 - 0.5, 0, utils::image::INTERP_CUBIC );
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
      searchPointLine.addPoint( xip1, yip1, sampleAngle( _context.dx, _context.dy, xip1, yip1 ), probaip1, _context.originalImage.utils::image::getSubPixelValue<uint8_t>( xip1 - 0.5, yip1 - 0.5, 0, utils::image::INTERP_CUBIC )  );
#endif
      LS_DEBUG_COND(DEBUG_COND, i << " " <<  xi << " " << yi << " " << probaim1 << " " << probai << " " << probaip1 );
      if( probai > probaim1 and probai > probaip1)
      {
        double anglei = utils::image::sampleAngle( _context.dx, _context.dy, xi, yi );
        double cAngle = /*fabs*/(corrAngle( anglei, _angle ));
        int ai = abs(i);
//         double anglei = _angles.getSubPixelValue<float>( xi - 1, yi - 1);
        LS_DEBUG_COND(DEBUG_COND, cAngle << " " << anglei << " " << _angle );
        if( (ai < bestI and cAngle > thresholdGradColinearity()) or ( ai == bestI and cAngle > bestAngleCorr ) )
        {
          bestAngleCorr = cAngle;
          bestI = ai;
          _bestI = i;
          _xr = xi;
          _yr = yi;
          _angler = anglei;
          success = true;
          bestProaba = probai;
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
          searchPointLine.lastIsSelected(-1);
#endif
//           LS_DEBUG_COND(DEBUG_COND, segCount << " " << _i << " " << _xr << " " << _yr << " " <<  probaim1 << " " << probai << " " << probaip1 );
        }
      }
      probaim1 = probai;
      probai = probaip1;
      xim1 = xi;
      yim1 = yi;
      xi = xip1;
      yi = yip1;
    } else {
      break;
    }
  }
  if( success )
  {
    _context.segHyp->gradientDescriptor().appendGradient( bestProaba );
  }
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
  _context.segHyp->segmentHypothesisDebug()->addSearchPointLine( searchPointLine );
#endif
  
  return success;
}

SegmentHypothesis* DirectSegmentsBase::attemptToMerge( SegmentHypothesis* segHyp, const cv::Mat& inasegment, SegmentsSet& segmentHypothesises, const std::map<int, int>& _segmentToCount )
{
  LS_DEBUG_COND( DEBUG_COND, "==================== Merge: " << segCount << " ====================");
  int maxCount = 0;
  int maxSegmentId = -1;
  for(std::map<int, int>::const_iterator it = _segmentToCount.begin(); it != _segmentToCount.end(); ++it)
  {
    if( it->second > maxCount )
    {
      maxCount = it->second;
      maxSegmentId = it->first;
    }
  }
  if( maxSegmentId != -1 )
  {
    SegmentHypothesis* otherHyp = segmentHypothesises.segmentId( maxSegmentId );
    LS_ASSERT(otherHyp, "Hypothesis " << maxSegmentId << " was not found");
    LS_DEBUG_COND( DEBUG_COND, "Found overlap ?" << maxCount << " Id: " << otherHyp->id());
    if( otherHyp->isCompatible( *segHyp ) )
    {
      LS_DEBUG_COND( DEBUG_COND, "Found compatible " << otherHyp->id() );
      otherHyp->merge( *segHyp );
      return otherHyp;
    } else {
      return 0;
    }
  }
  return 0;
#if 0
  std::map<int, int> segmentToCount;
  int maxCount = 0;
  int maxSegmentId = -1;
  
  double xStep = ( segHyp->x2() - segHyp->x1());
  double yStep = ( segHyp->y2() - segHyp->y1());
  double norm_steps = sqrt( xStep * xStep + yStep * yStep );
  xStep /= norm_steps;
  yStep /= norm_steps;
  int max_coloriage = (int)norm_steps +1;
  for( int k = 0; k < max_coloriage; ++k )
  {
    int x = (int)( segHyp->x1() + k * xStep );
    int y = (int)( segHyp->y1() + k * yStep );
    for( int j = -1; j <= 1; ++j)
    {
      for( int i = -1; i <= 1; ++i)
      {
        if( check( x + i, inasegment.width() - 1 ) and check( y + j, inasegment.height() - 1 ) )
        {
          int v = inasegment.getPixelValue<int>( x+i, y+j, 0 );
          if( v > 0 )
          {
            int segId = v - 1;
            int nc = ++segmentToCount[ segId ];
            if( nc > maxCount )
            {
              maxCount = nc;
              maxSegmentId = segId;
            }
          }
        }
      }
    }
  }
  if( maxSegmentId != -1 )
  {
    SegmentHypothesis* otherHyp = segmentHypothesises.segmentId( maxSegmentId );
    LS_ASSERT(otherHyp, "Hypothesis " << maxSegmentId << " was not found");
        LS_DEBUG("Found overlap ?" << maxCount);
    if( otherHyp->lineFitter().compatible( &segHyp->lineFitter() ) )
    {
      LS_DEBUG("Found overlap " << maxCount);
      otherHyp->merge( *segHyp );
      return otherHyp;
    } else {
      return 0;
    }
  }
  return 0;
#endif
}

void DirectSegmentsBase::coloriage( double x_1, double y_1, double x_2, double y_2, cv::Mat& inasegment, int _value)
{
  double xStep = (x_2 - x_1);
  double yStep = (y_2 - y_1);
  double norm_steps = sqrt( xStep * xStep + yStep * yStep );
  xStep /= norm_steps;
  yStep /= norm_steps;
  int max_coloriage = (int)norm_steps +1;
  for( int k = 0; k < max_coloriage; ++k )
  {
    int x = (int)( x_1 + k * xStep );
    int y = (int)( y_1 + k * yStep );
    for( int j = -1; j <= 1; ++j)
    {
      for( int i = -1; i <= 1; ++i)
      {
        if( check( x + i, inasegment.cols - 1 ) and check( y + j, inasegment.rows - 1 ) )
        {
          utils::image::setPixelValue<int>( inasegment, _value, x+i, y+j, 0 );
        }
      }
    }
  }
#if 0
  double xStep = (x_2 - x_1);
  double yStep = (y_2 - y_1);
  double norm_steps = sqrt( xStep * xStep + yStep * yStep );
  xStep /= norm_steps;
  yStep /= norm_steps;
  int max_coloriage = (int)norm_steps +1;
  int p_x = x_1;
  int p_y = y_1;
  if( check( p_x,  inasegment.width() - 1 ) and check( p_y, inasegment.height() - 1 ) )
  {
    inasegment.setPixelValue<int>( _value, p_x, p_y, 0 );
  }
  for( int k = -1; k <= max_coloriage; ++k )
  {
    int x = (int)( x_1 + k * xStep );
    int y = (int)( y_1 + k * yStep );
    if( check( p_x, inasegment.width() - 1 ) and check( y, inasegment.height() - 1 ) )
    {
      inasegment.setPixelValue<int>( _value, p_x, y, 0 );
    }
    if( check( x, inasegment.width() - 1 ) )
    {
      if( check( p_y, inasegment.height() - 1 ) )
      {
        inasegment.setPixelValue<int>( _value, x, p_y, 0 );
      }
      if( check( y, inasegment.height() - 1 ) )
      {
        inasegment.setPixelValue<int>( _value, x, y, 0 );
      }
    }
    p_x = x;
    p_y = y;
  }
#endif
}

void DirectSegmentsBase::setMinimumSegmentLength(double _minimumSegmentLength)
{
  m_minimumSegmentLength = _minimumSegmentLength;
  m_minimumSegmentLengthPow2 = _minimumSegmentLength * _minimumSegmentLength;
}

void DirectSegmentsBase::computeGradientValuesOnLine( double& a, double& b, double angle, double cosA, double sinA, float* _mag1, float* _mag2, float* _mag3, int j ) const
{
  double absCosA = fabs( cosA ) / M_SQRT1_2;
  double absSinA = fabs( sinA ) / M_SQRT1_2;
  double angleDeg = angle * 180 / M_PI;
  if( angleDeg < 0.0) angleDeg = 360 + angleDeg;
  int at = (int)((angleDeg / 45));
  switch( at )
  {
    case 0:
    case 4:
      a = (_mag2[j+1] * (1 - absSinA) + _mag3[j+1] * absSinA );
      b = (_mag2[j-1] * (1 - absSinA) + _mag1[j-1] * absSinA );
      break;
    case 1:
    case 5:
      a = (_mag3[j+1] * absCosA + _mag3[j  ] * (1 - absCosA ) );
      b = (_mag1[j-1] * absCosA + _mag1[j  ] * (1 - absCosA ) );
      break;
    case 2:
    case 6:
      a = (_mag3[j  ] * (1 - absCosA) + _mag3[j-1] * absCosA );
      b = (_mag1[j  ] * (1 - absCosA) + _mag1[j+1] * absCosA );
      break;
    case 3:
    case 7:
      a = (_mag3[j-1] * absSinA + _mag2[j-1] * (1 - absSinA) );
      b = (_mag1[j+1] * absSinA + _mag2[j+1] * (1 - absSinA) );
      break;
  }
}

bool DirectSegmentsBase::hasMaximumGradient( double _angle, double _cosA, double _sinA, float* _mag1 , float* _mag2, float* _mag3, int _j ) const
{
  double a = 1e-6;
  double b = 1e-6;
  computeGradientValuesOnLine(a, b, _angle, _cosA, _sinA, _mag1, _mag2, _mag3, _j );
  double center = _mag2[_j];
  return ( (center - a) > thresholdMaxGradient() and (center - b) > thresholdMaxGradient() );
}

#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
# define searchPointProc searchPoint
#else
# define searchPointProc smartSearchPoint
#endif

void DirectSegmentsBase::adjustStep( double& _step, int &_searchNbSteps ) const
{
  double mss = maximalSearchStep();
  if( _step > mss )
  {
    _searchNbSteps = int(_step * _searchNbSteps / mss) + 1;
    _step = mss;
  }
}

bool DirectSegmentsBase::growSegment( GrowSegmentContext& _context, int _step )
{
  LS_ASSERT(_step > 0, "Step need to be a positive value");
  int count = 0;
  double x1,y1, angle1;
  int step_p = _step;
  int step_n = _step;
  while(true)
  {
    bool shouldSearchPositive = true;
    bool shouldSearchNegative = true;
    bool hadASuccess = false;
    int best_i;
    double step;
    int searchNbSteps_;
    double error;
    while( shouldSearchPositive or shouldSearchNegative )
    {
      searchNbSteps_ = searchNbSteps();
      error = _context.segHyp->lineFitter().errorAt(_context.k_p);
      if( error > m_maximalSearchLength)
      {
        error = m_maximalSearchLength;
      }
      step = (error / searchNbSteps_);
      adjustStep( step, searchNbSteps_);
      if( shouldSearchPositive  and ( searchPointProc( _context,
                          step,
                          _context.segHyp->lineFitter().pointAt(_context.k_p),
                          x1,
                          y1,
                          angle1,
                          best_i, 
                          searchNbSteps_ ) ) )
      {
        _context.x_2 = x1;
        _context.y_2 = y1;
        _context.segHyp->addPoint( _context.k_p, x1, y1, angle1, 0.5 );
        LS_DEBUG_COND(DEBUG_COND, _context.segHyp->lineFitter().x() << " " << (_context.segHyp->lineFitter().x()(0) * x1 + _context.segHyp->lineFitter().x()(1) * y1 + 1.0) << " " << x1 << " " << y1);
        _context.growStopCondition->reached( _context.k_p );
        // Iteration
        _context.k_p += step_p;
        ++count;
        hadASuccess = true;
        if( best_i != 0) break;
        
        if( _context.inasegment )
        {
          int segId = utils::image::getPixelValue<int>( *_context.inasegment, x1, y1, 0 );
          if( segId > 0 )
          {
            ++_context.segmentToCount[ segId ];
          }
        }
      } else {
        shouldSearchPositive = false;
      }
      searchNbSteps_ = searchNbSteps();
      error = _context.segHyp->lineFitter().errorAt(_context.k_n);
      if( error > m_maximalSearchLength)
      {
        error = m_maximalSearchLength;
      }
      step = (error / searchNbSteps_);
      adjustStep( step, searchNbSteps_);
      if( shouldSearchNegative  and ( searchPointProc( _context,
                          step,
                          _context.segHyp->lineFitter().pointAt(_context.k_n),
                          x1,
                          y1,
                          angle1,
                          best_i,
                          searchNbSteps_ ) ) )
      {
        _context.x_1 = x1;
        _context.y_1 = y1;
        _context.segHyp->addPoint( _context.k_n, x1, y1, angle1, 0.5 );
        LS_DEBUG_COND(DEBUG_COND, _context.segHyp->lineFitter().x() << " " << (_context.segHyp->lineFitter().x()(0) * x1 + _context.segHyp->lineFitter().x()(1) * y1 + 1.0) << " " << x1 << " " << y1);
        _context.growStopCondition->reached( _context.k_n );
        // Iteration
        _context.k_n -= step_n;
        ++count;
        hadASuccess = true;
        if( best_i != 0) break;
        
        if( _context.inasegment )
        {
          int segId = utils::image::getPixelValue<int>( *_context.inasegment, x1, y1, 0 );
          if( segId > 0 )
          {
            ++_context.segmentToCount[ segId ];
          }
        }
      } else {
        shouldSearchNegative = false;
      }
    }
    if( not hadASuccess )
    {
      LS_DEBUG_COND(DEBUG_COND, segCount << " No more point found");
      if( step_p > 1 or step_n > 1 ) {
        if( step_p > 1 ) {
          --step_p;
          --_context.k_p;
        }
        if( step_n > 1 ) {
          --step_n;
          ++_context.k_n;
        }
      } else {
        if( (_context.growStopCondition->shouldStopPositiveSearch()
            and _context.growStopCondition->shouldStopNegativeSearch() )
            or _context.segHyp->countPoints() <= 1 )
        {
          return true;
        } else {
          if( not _context.growStopCondition->shouldStopPositiveSearch() )
          {
            ++_context.k_p;
            _context.growStopCondition->reached( _context.k_p );
          }
          if( not _context.growStopCondition->shouldStopNegativeSearch() )
          {
            --_context.k_n;
            _context.growStopCondition->reached( _context.k_n );
          }
          _context.growStopCondition->reset();
        }
      }
    }
  }
}

double DirectSegmentsBase::minimumSegmentLengthPow2() const
{
  return m_minimumSegmentLengthPow2;
}
