/* $Id: SegmentHypothesis.cpp 4162 2009-10-07 13:34:37Z cberger $ */

#include "SegmentHypothesis.hpp"

#include <list>

// Fix instantiation of Private in 32bits, see http://eigen.tuxfamily.org/dox-devel/TopicStlContainers.html
#include<Eigen/StdVector>

#include "Debug.hpp"
#include "LineFitterKalman2.hpp"
#include "SegmentHypothesisDebug.hpp"
#include "GradientStatsDescriptor.hpp"
#include "../utils/Macros_p.hpp"

using namespace libsegments::dseg;

struct SegmentHypothesis::Private {
  Private(double x_o, double y_o, double angle_o) : lf( new LineFitterKalman2(x_o, y_o, angle_o)), 
#if !ENABLE_SEGMENT_HYPOTHESIS_DEBUG
          countPoints(0), 
#endif
          ext1(2), ext2(2), debug(0), descriptor(0), ref(0) {}
  mutable LineFitterKalman2* lf;
  mutable bool lineParametersUpToDate;
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
  std::list< Eigen::Vector2d > vecPoints;
  std::list< Eigen::Vector2d > vecAngles;
#else
  int countPoints;
#endif
  Eigen::Vector2d ext1, ext2;
  unsigned int id;
  SegmentHypothesisDebug* debug;
  GradientStatsDescriptor gradientDescriptor;
  GradientsDescriptor* descriptor;
  int ref;
  // Fix instantiation of Private in 32bits, see http://eigen.tuxfamily.org/dox-devel/TopicStructHavingEigenMembers.html
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

inline double pow2( double a )
{
  return a * a;
}

inline void projection( double &x_r, double &y_r, double x_A, double y_A, double a, double b )
{
  double aa = a * a;
  double bb = b * b;
  double ab = a * b;
  double num = 1.0 / (aa + bb);
  x_r = (x_A * bb - y_A * ab - a) * num;
  y_r = -(x_A * ab - y_A * aa + b) * num;
}

SegmentHypothesis::SegmentHypothesis(double x_o, double y_o, double angle_o) : d(new Private(x_o, y_o, angle_o))
{
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
  d->debug = new SegmentHypothesisDebug;
#endif
  d->lineParametersUpToDate = false;
}

SegmentHypothesis::~SegmentHypothesis()
{
  delete d->lf;
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
  delete d->debug;
#endif
  delete d;
}

void SegmentHypothesis::addPoint(double t, double _x, double _y, double _angle, double sigmaPx)
{
  Eigen::Vector2d in;
  in(0) = _x;
  in(1) = _y;
  d->lf->addPoint( t, in, _angle, sigmaPx );
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
  d->vecPoints.push_back( in );
#else
  ++d->countPoints;
#endif
  d->lineParametersUpToDate = false;
}

const Eigen::Vector2d& SegmentHypothesis::lineParameters() const
{
  return d->lf->x();
}

void SegmentHypothesis::dump(bool _verbose) const
{
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
  if( _verbose )
  {
    LS_DEBUG("Points:");
    unsigned int i = 0;
    for( std::list< Eigen::Vector2d>::const_iterator it = d->vecPoints.begin();
        it != d->vecPoints.end(); ++it, ++i)
    {
      LS_DEBUG( i << " <-> " << *it );
    }
  }
  LS_DEBUG_BEGIN();
  LS_DEBUG_SEND("Ruby array : ");
  for(std::list< Eigen::Vector2d>::const_iterator it = d->vecPoints.begin();
       it != d->vecPoints.end(); ++it)
  {
    LS_DEBUG_SEND( "[" << (*it)(0) << "," << (*it)(1) << "]," );
  }
  LS_DEBUG_END();
  if(_verbose)
  {
    LS_DEBUG("Angles:");
    int i = 0;
    for( std::list< Eigen::Vector2d>::const_iterator it = d->vecAngles.begin();
        it != d->vecAngles.end(); ++it, ++i)
    {
      LS_DEBUG( i << " <-> " << *it );
    }
  }
#endif
  d->lf->dump();

#if 0
  {
    double a = (d->lfx1()(0) / d->lf->x1()(1));
    double b = 1.0 / d->lf.x1()(1);
    LS_DEBUG("Solver 1 : " << d->lf->x1() << " (" << a << ", " << b << " ) " );
    double errPoints = 0.0;
    double errAngles = 0.0;
    for(std::list< Eigen::Vector2d>::const_iterator it = d->vecPoints.begin();
         it != d->vecPoints.end(); ++it)
    {
      errPoints += pow2( (*it)(0) * a + (*it)(1) * b + 1.0 );
    }
    for( std::list< Eigen::Vector2d>::const_iterator it = d->vecAngles.begin();
         it != d->vecAngles.end(); ++it)
    {
      errAngles += pow2( (*it)(0) * a + (*it)(1) * b );
    }
    LS_DEBUG( "errPoints = " << errPoints << " errAngles = " << errAngles );
  }
  {
    double a = 1.0 / d->lf->x2()(1);
    double b = d->lf.x2()(0) / d->lf->x2()(1);
    LS_DEBUG("Solver 2 : " << d->lf->x2() << " (" << a << ", " << b << " ) " );
    double errPoints = 0.0;
    double errAngles = 0.0;
    for(std::list< Eigen::Vector2d>::const_iterator it = d->vecPoints.begin();
         it != d->vecPoints.end(); ++it)
    {
      errPoints += pow2( (*it)(0) * a + (*it)(1) * b + 1.0 );
    }
    for( std::list< Eigen::Vector2d>::const_iterator it = d->vecAngles.begin();
         it != d->vecAngles.end(); ++it )
    {
      errAngles += pow2( (*it)(0) * a + (*it)(1) * b );
    }
    LS_DEBUG( "errPoints = " << errPoints << " errAngles = " << errAngles );
  }
#endif

}

int SegmentHypothesis::countPoints() const
{
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
  return d->vecPoints.size();
#else
  return d->countPoints;
#endif
}

void SegmentHypothesis::setExtremity1(double _x1, double _y1)
{
  const Eigen::Vector2d& p = lineParameters();
  projection(d->ext1(0), d->ext1(1), _x1, _y1, p(0), p(1));
  // Translate from "pixel" coordinate to physical coordinates (centered on the center of pixel, instead of the left corner)
//   d->ext1(0) += 0.5; d->ext1(1) += 0.5;
}

double SegmentHypothesis::x1() const
{
  return d->ext1(0);
}

double SegmentHypothesis::y1() const
{
  return d->ext1(1);
}

double SegmentHypothesis::t1() const
{
  const Eigen::Vector4d& state = lineFitter().state();
  double u = state(0);
  double x0 = state(1);
  double v = state(2);
  double y0 = state(3);
  return ( u * ( x1() - x0 ) + v * ( y1() - y0 ) ) / ( u * u + v * v );
}

void SegmentHypothesis::setExtremity2(double _x2, double _y2)
{
  const Eigen::Vector2d& p = lineParameters();
  projection(d->ext2(0), d->ext2(1), _x2, _y2, p(0), p(1));
  // Translate from "pixel" coordinate to physical coordinates (centered on the center of pixel, instead of the left corner)
//   d->ext2(0) += 0.5; d->ext2(1) += 0.5;
}

double SegmentHypothesis::x2() const
{
  return d->ext2(0);
}

double SegmentHypothesis::y2() const
{
  return d->ext2(1);
}

double SegmentHypothesis::t2() const
{
  const Eigen::Vector4d& state = lineFitter().state();
  double u = state(0);
  double x0 = state(1);
  double v = state(2);
  double y0 = state(3);
  return ( u * ( x2() - x0 ) + v * ( y2() - y0 ) ) / ( u * u + v * v );
}

double SegmentHypothesis::squareLength() const
{
  return pow2( d->ext1(0) - d->ext2(0) ) + pow2( d->ext1(1) - d->ext2(1) );
}

void SegmentHypothesis::merge(const SegmentHypothesis& rhs)
{
  LS_DEBUG( "=== merge ===" );
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
  d->debug->merge( *rhs.d->debug );
#endif
  LS_DEBUG( d->lf->state() << " " << rhs.d->lf->state() );
  d->lf->merge( rhs.d->lf );
  LS_DEBUG( d->lf->state() );
  d->lineParametersUpToDate = false;
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
  d->vecPoints.insert( --d->vecPoints.end(), rhs.d->vecPoints.begin(), rhs.d->vecPoints.end() );
  d->vecAngles.insert( --d->vecAngles.end(), rhs.d->vecAngles.begin(), rhs.d->vecAngles.end() );
#else
  d->countPoints += rhs.d->countPoints;
#endif
  // Lets find the new extremities: it's the two most distant points
  int bestI = -1;
  int bestJ = -1;
  double bestDistance = -1.0;
  std::vector< Eigen::Vector2d, Eigen::aligned_allocator<Eigen::Vector2d> > points;
  points.push_back( d->ext1 );
  points.push_back( d->ext2 );
  points.push_back( rhs.d->ext1 );
  points.push_back( rhs.d->ext2 );
  for( int i = 0; i < 4; ++i)
  {
    for( int j = i + 1; j < 4; ++j)
    {
      double d = ( points[i] - points[j] ).norm();
      if( d > bestDistance )
      {
        bestI = i;
        bestJ = j;
        bestDistance = d;
      }
    }
  }
  LS_DEBUG( bestI << " " << bestJ );
  LS_DEBUG( d->ext1 << " " << d->ext2 << " " << rhs.d->ext1 << " " << rhs.d->ext2 );
  setExtremity1( points[bestI](0), points[bestI](1) );
  setExtremity2( points[bestJ](0), points[bestJ](1) );
  LS_DEBUG( d->ext1 << " " << d->ext2  );
  LS_DEBUG( "Finmerge");
}

const LineFitterKalman2& SegmentHypothesis::lineFitter() const
{
  return *d->lf;
}

unsigned int SegmentHypothesis::id() const
{
  return d->id;
}

void SegmentHypothesis::setId( unsigned int _id )
{
  d->id = _id;
}

const SegmentHypothesisDebug* SegmentHypothesis::segmentHypothesisDebug() const
{
  LS_ASSERT( d->debug, "Not compiled with ENABLE_SEGMENT_HYPOTHESIS_DEBUG" );
  return d->debug;
}

SegmentHypothesisDebug* SegmentHypothesis::segmentHypothesisDebug()
{
  LS_ASSERT( d->debug, "Not compiled with ENABLE_SEGMENT_HYPOTHESIS_DEBUG" );
  return d->debug;
}

void SegmentHypothesis::setSegmentHypothesisDebug( SegmentHypothesisDebug* _debug)
{
  d->debug = _debug;
}

bool SegmentHypothesis::isCompatible( const SegmentHypothesis& _sh ) const
{
  if( not d->lf->isCompatibleDirectionObservation( _sh.d->lf->state()(0), _sh.d->lf->state()(2) ) )
  {
//     return false;
  }
  int bestI = -1;
  int bestJ = -1;
  double bestDistance = -1;
  std::vector< Eigen::Vector2d, Eigen::aligned_allocator<Eigen::Vector2d>  > points;
  points.push_back( d->ext1 );
  points.push_back( d->ext2 );
  points.push_back( _sh.d->ext1 );
  points.push_back( _sh.d->ext2 );
  for( int i = 0; i < 4; ++i)
  {
    for( int j = i + 1; j < 4; ++j)
    {
      double d = ( points[i] - points[j] ).norm();
      if( d > bestDistance )
      {
        bestI = i;
        bestJ = j;
        bestDistance = d;
      }
    }
  }
  LS_DEBUG( bestI << " " << bestJ );
  LS_DEBUG( d->ext1 << " " << d->ext2 << " " << _sh.d->ext1 << " " << _sh.d->ext2 );
  bool compatible = false;
  if( bestI < 2 )
  {
    compatible = _sh.d->lf->isCompatibleObservation( points[bestI](0), points[bestI](1) );
  } else {
    compatible = d->lf->isCompatibleObservation( points[bestI](0), points[bestI](1) );
  }
  if( not compatible ) return false;
  
  if( bestJ < 2 )
  {
    compatible = _sh.d->lf->isCompatibleObservation( points[bestJ](0), points[bestJ](1) );
  } else {
    compatible = d->lf->isCompatibleObservation( points[bestJ](0), points[bestJ](1) );
  }
  return compatible;
}

void SegmentHypothesis::setUncertainty( double sigma_u, double sigma_x0, double sigma_v, double sigma_y0 )
{
  d->lf->setUncertainty( sigma_u, sigma_x0, sigma_v, sigma_y0 );
}

GradientStatsDescriptor& SegmentHypothesis::gradientDescriptor()
{
  return d->gradientDescriptor;
}

const GradientStatsDescriptor& SegmentHypothesis::gradientDescriptor() const
{
  return d->gradientDescriptor;
}

void SegmentHypothesis::computeTMinMax( double& _tMin, double& _tMax ) const
{
  const Eigen::Vector4d& state = lineFitter().state();
  double u = state(0);
  double x0 = state(1);
  double v = state(2);
  double y0 = state(3);
  _tMax = ( u * ( x1() - x0 ) + v * ( y1() - y0 ) ) / ( u * u + v * v );
  _tMin = ( u * ( x2() - x0 ) + v * ( y2() - y0 ) ) / ( u * u + v * v );
  if( _tMax < _tMin )
  { // Switch
    double tmp = _tMin;
    _tMin = _tMax;
    _tMax = tmp;
  }
}

void SegmentHypothesis::setDescriptor( GradientsDescriptor* _descriptor )
{
  d->descriptor = _descriptor;
}

const GradientsDescriptor* SegmentHypothesis::descriptor( ) const
{
  return d->descriptor;
}

void SegmentHypothesis::applyScale(double v)
{
  d->ext1 *= v;
  d->ext2 *= v;
  LS_DEBUG("Should also scale the filter.");
}

void SegmentHypothesis::incRef()
{
  ++d->ref;
}

void SegmentHypothesis::decRef()
{
  --d->ref;
  LS_ASSERT(d->ref >= 0, "Segment was unref once too much");
}

bool SegmentHypothesis::isRef()
{
  return d->ref > 0;
}
