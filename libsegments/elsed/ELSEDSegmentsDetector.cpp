#include "elsed/ELSEDSegmentsDetector.hpp"

#include "ELSED.h"

#include "../Segment.hpp"

using namespace libsegments;
using namespace libsegments::elsed;

ELSEDSegmentsDetector::ELSEDSegmentsDetector() : m_minimalSegmentLength(15)
{
  
}

std::vector< Segment > ELSEDSegmentsDetector::detectSegments( const cv::Mat& _image )
{
  upm::ELSEDParams p;
  p.minLineLen = m_minimalSegmentLength;
  upm::ELSED elsed(p);
  upm::Segments segs = elsed.detect(_image);
  
  std::vector< Segment > results;
  
  double ml = minimalSegmentLength() * minimalSegmentLength();
  for (const upm::Segment& seg: segs)
  {
    Segment s;
    s.x1 = seg[0];
    s.y1 = seg[1];
    s.x2 = seg[2];
    s.y2 = seg[3];
    double dx = s.x1 - s.x2;
    double dy = s.y1 - s.y2;
    if( (dx * dx + dy * dy) > ml )
    {
      results.push_back( s );
    }
  }
    
  return results;
}
