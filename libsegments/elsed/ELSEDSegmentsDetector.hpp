#ifndef _ELSD_ELSEDSEGMENTSDETECTOR_H_
#define _ELSD_ELSEDSEGMENTSDETECTOR_H_

#include <vector>

#include "../SegmentsDetector.hpp"
#include "../utils/Macros.hpp"

namespace libsegments {
  struct Segment;
  namespace elsed {
    /**
     * Efficient Line Segment Drawing, using the Suarez's implementation from
     * https://github.com/iago-suarez/ELSED
     */
    class ELSEDSegmentsDetector : public SegmentsDetector {
      public:
        ELSEDSegmentsDetector();
        std::vector< Segment > detectSegments( const cv::Mat& _image );
        LS_DEFINE_PARAM_RW( double, minimalSegmentLength, setMinimalSegmentLength );
    };
  }
}

#endif
