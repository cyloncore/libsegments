## libsegments ##

set(LIBSEGMENTS_SRC
    SegmentsDetector.cpp
    # Evaluation
    evaluation/hungarian.cpp
    evaluation/StatisticalComparison.cpp
    # Utils
    utils/IdFactory.cpp
    utils/Image.cpp
    utils/Pyramid.cpp
    # DSeg
    dseg/DetectorGrowStopCondition.cpp
    dseg/DirectSegmentsBase.cpp
    dseg/DirectSegmentsDetectorBase.cpp
    dseg/DirectSegmentsDetector.cpp
    dseg/GradientsDescriptor.cpp
    dseg/GradientStatsDescriptor.cpp
    dseg/GrowStopCondition.cpp
    dseg/LineFitterKalman2.cpp
    dseg/LoopDetectionContext.cpp
    dseg/SegmentHypothesis.cpp
    dseg/SegmentHypothesisDebug.cpp
    dseg/SegmentProbabilityEstimator.cpp
    dseg/SegmentsSet.cpp
    # DSegTracker
    dsegtracker/DirectSegmentsTracker.cpp
    dsegtracker/Predictor.cpp
    dsegtracker/SegmentsSelector.cpp
    dsegtracker/StaticPredictor.cpp
    # HDSeg
    hdseg/HierarchicalDirectSegmentsDetector.cpp
    hdseg/PyramidSP.cpp
    # Hough
    hough/HoughSegmentsDetector.cpp
    # LSD
    lsd/lsd.cpp
    lsd/LSDSegmentsDetector.cpp
    # Chaining
    chaining/ChainingSegmentsDetector.cpp
    chaining/contour1.cpp
    # ELSED
    elsed/EdgeDrawer.cpp
    elsed/ELSED.cpp
    elsed/FullSegmentInfo.cpp
    elsed/ELSEDSegmentsDetector.cpp
  )

add_library(libsegments SHARED ${LIBSEGMENTS_SRC})
target_link_libraries(libsegments rt ${OpenCV_LIBS} ${PNG_LIBRARIES} )

# installation
install(TARGETS libsegments EXPORT libsegmentsTargets ${INSTALL_TARGETS_DEFAULT_ARGS} )
install( FILES
  utils/Macros.hpp
  DESTINATION ${INCLUDE_INSTALL_DIR}/libsegments/utils )
install( FILES
  Segment.hpp
  SegmentsDetector.hpp
  DESTINATION ${INCLUDE_INSTALL_DIR}/libsegments/ )

# dseg
install( FILES
  dseg/DirectSegmentsDetector.hpp
  DESTINATION ${INCLUDE_INSTALL_DIR}/libsegments/dseg )

# hdseg
install( FILES
  hdseg/HierarchicalDirectSegmentsDetector.hpp
  DESTINATION ${INCLUDE_INSTALL_DIR}/libsegments/hdseg )

# elesd
install( FILES
  elsed/ELSEDSegmentsDetector.hpp
  DESTINATION ${INCLUDE_INSTALL_DIR}/libsegments/elsed )

# evaluation
install( FILES
evaluation/StatisticalComparison.hpp
  DESTINATION ${INCLUDE_INSTALL_DIR}/libsegments/evaluation )

# lsd
install( FILES
  lsd/LSDSegmentsDetector.hpp
  DESTINATION ${INCLUDE_INSTALL_DIR}/libsegments/lsd )

# hough
install( FILES
  hough/HoughSegmentsDetector.hpp
  DESTINATION ${INCLUDE_INSTALL_DIR}/libsegments/hough )

# Chaining
install( FILES
  chaining/ChainingSegmentsDetector.hpp
  DESTINATION ${INCLUDE_INSTALL_DIR}/libsegments/chaining )
