#ifndef _LSD_LSDSEGMENTSDETECTOR_H_
#define _LSD_LSDSEGMENTSDETECTOR_H_

#include <vector>

#include "../SegmentsDetector.hpp"
#include "../utils/Macros.hpp"

namespace libsegments {
  struct Segment;
  namespace lsd {
    /**
     * @ingroup lsd
     * Line Segment Dector, using the Grompone's implementation from
     * http://mw.cmla.ens-cachan.fr/megawave/algo/lsd/
     */
    class LSDSegmentsDetector : public SegmentsDetector {
      public:
        LSDSegmentsDetector();
        std::vector< Segment > detectSegments( const cv::Mat& _image );
        LS_DEFINE_PARAM_RW( double, minimalSegmentLength, setMinimalSegmentLength );
    };
  }
}

#endif
