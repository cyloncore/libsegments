/*---------------------------------------------------------------------------

  LSD - Line Segment Detector on digital images

  Copyright 2007,2008 rafael grompone von gioi (grompone@gmail.com)

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  MA 02110-1301, USA.

  ---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------

   This code can be compiled as a pure C language program
   or as a module for the MegaWave2 framework
   (http://megawave.cmla.ens-cachan.fr).

   To compile as a pure C language program use the following
   command:
     cc -DNO_MEGAWAVE -O6 -lm -o lsd lsd.c

   To compile with the MegaWave2 framework, use the following
   command:
     cmw2 -O6 lsd.c

   When executed as an independet program the input is given
   in a PGM grayscale image format file. The name of the file
   must be given as the first parameter to the command. An
   optional second parameter can specify the output file
   (otherwise the output will be printed to stantdard output).
   The output of the program will be a list of line segments,
   one per line in the following format:
     x1 y1 x2 y2 width
   for a line segment from point (x1,y1) to point (x2,y2) and
   a width of 'width'.

   An offset of 0.5,0.5 is added to the output value so that
   (in a visualization with software like fkview, where the lower
    left corner of 0,0 pixel have coordinates 0,0)
   a line segment starting at coordinates 0,0 will be see as
   starting at the center of the pixel 0,0.

   Example of use:
     ./lsd myimage.pgm
     ./lsd myimage.pgm output.txt

   Example of output:
     28.0347 38.244 22.5263 32.4425 3
     3.74755 22.3012 6.92328 12.8189 1
     43.2815 26.4786 34.6753 23.8453 2

  ---------------------------------------------------------------------------*/

/*--------------------------- Commande MegaWave -----------------------------*/
/* mwcommand
  name = {lsd};
  version = {"1.0"};
  author = {"rafael grompone von gioi"};
  function = {"Line Segment Detector"};
  usage = {
    's':[s=1.0]->scale
        "scale s% factor by gaussian filter (only if != 1.0) (default 1.0)",
    'c':[c=0.7]->sigma_scale
        "sigma = c/scale, for -s option (default 0.7)",
    'q':[q=2.0]->q
        "Bound to the quantization error on the gradient norm (default 2.0).",
    'd':[d=8]->d
        "Number of allowed gradient directions (default 8).",
    'e':[eps=0.]->eps
        "Detection threshold, -log10(max. number of false alarms), default 0.0",
    'b':[n_bins=16256]->n_bins
        "Number of bins in pseudo-ordering of gradient modulus (default 16256)",
    'm':[max_grad=260100]->max_grad
        "Gradient modulus in the highest bin (default 260100).",
    'v'->verb
        "Verbose mode. Print information while processing.",
    'x':x->x
        "Grow just one region starting from point x,y (to be used with -y).",
    'y':y->y
        "Grow just one region starting from point x,y (to be used with -x).",
    'r':regions<-regout
        "Show on the Cimage 'regions' the points used by each region.",
    'T'->ascii_reg
        "Print to standard output the list of points for each region.",
    'n':n->n
        "When using the option '-r' show just region number n.",
    'i':iout<-iout
        "Gives the result as an Cimage.",
    'F':fout<-fout
        "Output the Flists fout with the line segments.",
    'R':rout<-rout
        "Output the Flists rout with the rectangles.",
    'a'->arrow
        "Draw 'arrow heads' to the rectangles in -R option.",
    'A'->ascii
        "Output LS to standard output, one per line, as 'x1 y1 x2 y2 width'.",
    in->image
        "Input image.",
    out<-out
        "Line Segment output (5-Flist: x1,y1,x2,y2,width)."
  };
*/

#include "lsd.h"

#ifndef NO_MEGAWAVE
/* comment the following line to compile as a pure C language program,
   uncomment it to compile as a MegaWave2 module */
#include "mw.h"
#endif

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

#define NOTDEF   -1000.0
#define BIG_NUMBER 1.0e+300
#define M_3_2_PI 4.71238898038
#define M_2__PI  6.28318530718
#define NOTUSED 0
#define USED    1
#define NOTINI  2

#define MY_ROUND(f) (floorf((f) + 0.5))

/* some global variables */
int verbose = FALSE;

/*--------------------------------------------------------------------------*/
struct coorlist
{
  int x,y;
  struct coorlist * next;
};

/*--------------------------------------------------------------------------*/
struct point {int x,y;};

/*--------------------------------------------------------------------------*/
/* Global variables
 */
struct point * reg2;             /* list of points in a region */
int reg_size2;                   /* size of region in reg2 */
float * sum_l;                   /* weight sum on longitudinal direction */
float * sum_w;                   /* weight sum on lateral direction */
int sum_res = 1;                 /* resolution factor on weight sum */
int sum_offset;                  /* offset to center of weight sum */

/*--------------------------------------------------------------------------*/
void error(const char * msg)
{
  fprintf(stderr,"%s\n",msg);
  exit(1);
}

/*--------------------------------------------------------------------------*/
float dist(float x1, float y1, float x2, float y2)
{
  return sqrtf( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) );
}


/*--------------------------------------------------------------------------*/
/*-------------------------- n-tuple DATA TYPES ----------------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
void free_ntuple_float(ntuple_float in)
{
  if( in == NULL ) error("free_ntuple_float: NULL input.");
  if( in->values == NULL ) error("free_ntuple_float: NULL values in input.");
  free((void *)in->values);
  free((void *)in);
}

/*--------------------------------------------------------------------------*/
ntuple_float new_ntuple_float(int dim)
{
  ntuple_float n_tuple;

  n_tuple = (ntuple_float)malloc(sizeof(struct ntuple_float_s));
  if( n_tuple == NULL ) error("Not enough memory.");
  n_tuple->size = 0;
  n_tuple->max_size = 1;
  n_tuple->dim = dim;
  n_tuple->values = (float *)malloc( dim*n_tuple->max_size * sizeof(float) );
  if( n_tuple->values == NULL ) error("Not enough memory.");
  return n_tuple;
}

/*--------------------------------------------------------------------------*/
void enlarge_ntuple_float(ntuple_float n_tuple)
{
  if( n_tuple->max_size <= 0 ) error("enlarge_ntuple_float: invalid n-tuple.");
  n_tuple->max_size *= 2;
  n_tuple->values =
    (float *)realloc( (void *)n_tuple->values,
                      n_tuple->dim * n_tuple->max_size * sizeof(float) );
  if( n_tuple->values == NULL ) error("Not enough memory.");
}

/*--------------------------------------------------------------------------*/
void add_5tuple(ntuple_float out, float v1,float v2,float v3,float v4,float v5)
{
  if( out == NULL ) error("add_5tuple: invalid n-tuple input.");
  if( out->size == out->max_size ) enlarge_ntuple_float(out);
  if( out->values == NULL ) error("add_5tuple: invalid n-tuple input.");
  out->values[ out->size * out->dim + 0 ] = v1;
  out->values[ out->size * out->dim + 1 ] = v2;
  out->values[ out->size * out->dim + 2 ] = v3;
  out->values[ out->size * out->dim + 3 ] = v4;
  out->values[ out->size * out->dim + 4 ] = v5;
  out->size++;
}


/*--------------------------------------------------------------------------*/
/*--------------------------- IMAGE DATA TYPES -----------------------------*/
/*--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------*/
void free_image_char(image_char i)
{
  if( i == NULL ) error("free_image_char: NULL input.");
  if( i->data == NULL ) error("free_image_char: input image have no data.");
  free((void *)i->data);
  free((void *)i);
}

/*--------------------------------------------------------------------------*/
image_char new_image_char(int xsize,int ysize)
{
  image_char image;

  image = (image_char)malloc(sizeof(struct image_char_s));
  if( image == NULL ) error("Not enough memory.");
  image->data = (unsigned char *)calloc(xsize*ysize, sizeof(unsigned char));
  if( image->data == NULL ) error("Not enough memory.");

  image->xsize = xsize;
  image->ysize = ysize;

  return image;
}

/*--------------------------------------------------------------------------*/
image_char new_image_char_ini(int xsize,int ysize,int ini)
{
  image_char image = new_image_char(xsize,ysize);
  int N = xsize*ysize;
  int i;

  for(i=0; i<N; i++) image->data[i] = ini;

  return image;
}


/*--------------------------------------------------------------------------*/
void free_image_float(image_float i)
{
  if( i == NULL ) error("free_image_float: NULL input.");
  if( i->data == NULL ) error("free_image_float: input image have no data.");
  free((void *)i->data);
  free((void *)i);
}

/*--------------------------------------------------------------------------*/
image_float new_image_float(int xsize,int ysize)
{
  image_float image;

  image = (image_float)malloc(sizeof(struct image_float_s));
  if( image == NULL ) error("Not enough memory.");
  image->data = (float *)calloc(xsize*ysize, sizeof(float));
  if( image->data == NULL ) error("Not enough memory.");

  image->xsize = xsize;
  image->ysize = ysize;

  return image;
}

/*--------------------------------------------------------------------------*/
image_float new_image_float_ini(int xsize,int ysize,float ini)
{
  image_float image = new_image_float(xsize,ysize);
  int N = xsize*ysize;
  int i;

  for(i=0; i<N; i++) image->data[i] = ini;

  return image;
}


/*--------------------------------------------------------------------------*/
/*------------------------------- IMAGE I/O --------------------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
void skip_what_should_be_skipped(FILE * f)
{
  int c;
  do
    {
      while(isspace(c=getc(f))); /* skip spaces */
      if(c=='#') while((c=getc(f))!='\n'); /* skip comments */
    }
  while(c == '#');
  ungetc(c,f);
}

/*--------------------------------------------------------------------------*/
int get_num(FILE * f)
{
  int num, c;

  while(isspace(c=getc(f)));
  if(!isdigit(c)) error("Corrupted PGM file.");
  num = c - '0';
  while( isdigit(c=getc(f)) ) num = 10 * num + c - '0';

  return num;
}

/*--------------------------------------------------------------------------*/
image_char read_pgm_image_char(char * name)
{
  FILE * f;
  int c,bin,x,y;
  int xsize,ysize,depth;
  image_char image;

  /* open file */
  f = fopen(name,"r");
  if( f == NULL ) error("can't open input file.");

  /* read headder */
  if( getc(f) != 'P' ) error("Not a PGM file!");
  if( (c=getc(f)) == '2' ) bin = FALSE;
  else if( c == '5' ) bin = TRUE;
  else error("Not a PGM file!");
  skip_what_should_be_skipped(f);
  fscanf(f,"%d",&xsize);
  skip_what_should_be_skipped(f);
  fscanf(f,"%d",&ysize);
  skip_what_should_be_skipped(f);
  fscanf(f,"%d",&depth);

  /* get memory */
  image = new_image_char(xsize,ysize);
  image->xsize = xsize;
  image->ysize = ysize;

  /* read data */
  skip_what_should_be_skipped(f);
  for(y=0;y<ysize;y++)
    for(x=0;x<xsize;x++)
      image->data[ x + y * xsize ] = bin ? getc(f) : get_num(f);

  if(verbose) fprintf(stderr,"input image: xsize %d ysize %d depth %d\n",
                      image->xsize,image->ysize,depth);

  /* close file */
  fclose(f);

  return image;
}

/*--------------------------------------------------------------------------*/
image_float read_pgm_image_float(char * name)
{
  FILE * f;
  int c,bin,x,y;
  int xsize,ysize,depth;
  image_float image;

  /* open file */
  f = fopen(name,"r");
  if( f == NULL ) error("can't open input file.");

  /* read headder */
  if( getc(f) != 'P' ) error("Not a PGM file!");
  if( (c=getc(f)) == '2' ) bin = FALSE;
  else if( c == '5' ) bin = TRUE;
  else error("Not a PGM file!");
  skip_what_should_be_skipped(f);
  fscanf(f,"%d",&xsize);
  skip_what_should_be_skipped(f);
  fscanf(f,"%d",&ysize);
  skip_what_should_be_skipped(f);
  fscanf(f,"%d",&depth);

  /* get memory */
  image = new_image_float(xsize,ysize);
  image->xsize = xsize;
  image->ysize = ysize;

  /* read data */
  skip_what_should_be_skipped(f);
  for(y=0;y<ysize;y++)
    for(x=0;x<xsize;x++)
      image->data[ x + y * xsize ] = (float)(bin ? getc(f) : get_num(f));

  if(verbose) fprintf(stderr,"input image: xsize %d ysize %d depth %d\n",
                      image->xsize,image->ysize,depth);

  /* close file */
  fclose(f);

  return image;
}

/*--------------------------------------------------------------------------*/
void write_pgm_image_char(image_char image, char * name)
{
  FILE * f;
  int x,y,n;

  /* open file */
  f = fopen(name,"w");
  if( f == NULL ) error("can't open output file.");

  /* write headder */
  fprintf(f,"P2\n");
  fprintf(f,"%d %d\n",image->xsize,image->ysize);
  fprintf(f,"255\n");

  /* write data */
  for(n=1,y=0; y<image->ysize; y++)
    for(x=0; x<image->xsize; x++,n++)
      {
        fprintf(f,"%d ",image->data[ x + y * image->xsize ]);
        if(n==16)
          {
            fprintf(f,"\n");
            n = 0;
          }
      }

  /* close file */
  fclose(f);
}

/*--------------------------------------------------------------------------*/
void write_pgm_image_float(image_float image, char * name)
{
  FILE * f;
  int x,y,n;

  /* open file */
  f = fopen(name,"w");
  if( f == NULL ) error("can't open output file.");

  /* write headder */
  fprintf(f,"P2\n");
  fprintf(f,"%d %d\n",image->xsize,image->ysize);
  fprintf(f,"255\n");

  /* write data */
  for(n=1,y=0; y<image->ysize; y++)
    for(x=0; x<image->xsize; x++,n++)
      {
        fprintf(f,"%d ",(int)image->data[ x + y * image->xsize ]);
        if(n==16)
          {
            fprintf(f,"\n");
            n = 0;
          }
      }

  /* close file */
  fclose(f);
}


/*--------------------------------------------------------------------------*/
/*---------------------------- Image Operations ----------------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
void gaussian_kernel( ntuple_float kernel, float sigma, float offset )
{
  int i;
  float val;
  float sum = 0.0;

  /* compute gaussian kernel */
  kernel->size = 1;
  for(i=0;i<kernel->dim;i++)
    {
      val = ((float)i - offset) / sigma;
      kernel->values[i] = expf( -0.5 * val * val );
      sum += kernel->values[i];
    }

  /* normalization */
  for(i=0;i<kernel->dim;i++) kernel->values[i] /= sum;
}

/*--------------------------------------------------------------------------*/
image_float gaussian_sampler( image_float in, float scale, float sigma_scale )
{
  int N = (int)floorf( (float)(in->xsize) * scale ); /* final x-size */
  int M = (int)floorf( (float)(in->ysize) * scale ); /* final y-size */
  image_float aux = new_image_float(N,in->ysize);
  image_float out = new_image_float(N,M);
  float sigma = sigma_scale / scale;
  int h = (int)ceilf( sigma * sqrtf( 6.0 * logf(10.0) ) );
  int n = 1+2*h;
  ntuple_float kernel = new_ntuple_float(n);
  int x,y,xc,yc,i,j;
  float xx,yy,sum;
  int nx2 = 2 * in->xsize;
  int ny2 = 2 * in->ysize;

  /* x axis convolution */
  for(x=0;x<aux->xsize;x++)
    {
      xx = (float)x / scale;     /* corresponding x */
      xc = (int)floorf(xx+0.5);  /* corresponding x pixel */
      gaussian_kernel( kernel, sigma, (float)h + xx - (float)xc );

      for(y=0;y<aux->ysize;y++)
        {
          sum = 0.0;
          for(i=0;i<n;i++)
            {
              j = xc - h + i;
              while(j<0) j+=nx2;
              while(j>=nx2) j-=nx2;
              if(j>=in->xsize) j = nx2-1-j;
              sum += in->data[ j + y * in->xsize ] * kernel->values[i];
            }
          aux->data[ x + y * aux->xsize ] = sum;
        }
    }

  /* y axis convolution */
  for(y=0;y<out->ysize;y++)
    {
      yy = (float)y / scale;     /* corresponding y */
      yc = (int)floorf(yy+0.5);  /* corresponding y pixel */
      gaussian_kernel( kernel, sigma, (float)h + yy - (float)yc );

      for(x=0;x<out->xsize;x++)
        {
          sum = 0.0;
          for(i=0;i<n;i++)
            {
              j = yc - h + i;
              while(j<0) j+=ny2;
              while(j>=ny2) j-=ny2;
              if(j>=in->ysize) j = ny2-1-j;
              sum += aux->data[ x + j * aux->xsize ] * kernel->values[i];
            }
          out->data[ x + y * out->xsize ] = sum;
        }
    }

  /* free memory */
  free_ntuple_float(kernel);
  free_image_float(aux);

  return out;
}

/*--------------------------------------------------------------------------*/
void gaussian_filter(image_float image, float sigma)
{
  int offset = (int)ceilf( sigma * sqrtf( 6.0 * logf(10.0) ) );
  int n = 1 + 2 * offset;
  ntuple_float kernel = new_ntuple_float(n);
  image_float tmp = new_image_float(image->xsize,image->ysize);
  int x,y,i,j;
  int nx2 = 2*image->xsize;
  int ny2 = 2*image->ysize;
  float val;

  /* compute gaussian kernel */
  kernel->size = 1;
  for(i=offset;i<n;i++)
    {
      val = (float)(i-offset) / sigma;
      kernel->values[i] = kernel->values[n-1-i] = expf(-0.5*val*val);
    }
  /* normalization */
  for(val=0.0,i=n;i--;) val += kernel->values[i];
  for(i=n;i--;) kernel->values[i] /= val;

  /* x axis convolution */
  for(x=0;x<image->xsize;x++)
    for(y=0;y<image->ysize;y++)
      {
        val = 0.0;
        for(i=0;i<n;i++)
          {
            j = x - offset + i;
            while(j<0) j+=nx2;
            while(j>=nx2) j-=nx2;
            if(j>=image->xsize) j = nx2-1-j;
            val += image->data[ j + y * image->xsize ] * kernel->values[i];
          }
        tmp->data[ x + y * tmp->xsize ] = val;
      }

  /* y axis convolution */
  for(x=0;x<image->xsize;x++)
    for(y=0;y<image->ysize;y++)
      {
        val = 0.0;
        for(i=0;i<n;i++)
          {
            j = y - offset + i;
            while(j<0) j+=ny2;
            while(j>=nx2) j-=ny2;
            if(j>=image->ysize) j = ny2-1-j;
            val += tmp->data[ x + j * tmp->xsize ] * kernel->values[i];
          }
        image->data[ x + y * image->xsize ] = val;
      }

  /* free memory */
  free_ntuple_float(kernel);
  free_image_float(tmp);
}


/*--------------------------------------------------------------------------*/
/*----------------------------- Gradient Angle -----------------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/*
   compute the direction of the level line at each point.
   it returns:

   - an image_float with the angle at each pixel or NOTDEF.
   - the image_float 'modgrad' (a pointer is passed as argument)
     with the gradient magnitud at each point.
   - a list of pixels 'list_p' roughly ordered by gradient magnitude.
     (the order is made by classing points into bins by gradient magnitude.
      the parameters 'n_bins' and 'max_grad' specify the number of
      bins and the gradient modulus at the highest bin.)
   - a pointer 'mem_p' to the memory used by 'list_p' to be able to
     free the memory.
 */
image_float ll_angle( image_float in, float threshold,
                      struct coorlist ** list_p, void ** mem_p,
                      image_float * modgrad, int n_bins, int max_grad )
{
  image_float g = new_image_float(in->xsize,in->ysize);
  int n,p,x,y,adr,i;
  float com1,com2,gx,gy,norm2;
  /* variables used in pseudo-ordering of gradient mognitude */
  float f_n_bins = (float)n_bins;
  float f_max_grad = (float)max_grad;
  int list_count = 0;
  struct coorlist * list;
  struct coorlist ** range_l_s;
  struct coorlist ** range_l_e;
  struct coorlist * start;
  struct coorlist * end;

  if(verbose) fprintf(stderr,"gradient magnitud threshold: %g\n",threshold);

  threshold *= 4.0 * threshold;

  n = in->ysize;
  p = in->xsize;

  /* get memory for the image of gradient modulus */
  *modgrad = new_image_float(in->xsize,in->ysize);

  /* get memory for "ordered" coordinate list */
  list = (struct coorlist *)calloc(n*p,sizeof(struct coorlist));
  *mem_p = (void *)list;
  range_l_s = (struct coorlist **)calloc(n_bins,sizeof(struct coorlist *));
  range_l_e = (struct coorlist **)calloc(n_bins,sizeof(struct coorlist *));
  if( !list || !range_l_s || !range_l_e ) error("Not enough memory.");
  for(i=0;i<n_bins;i++) range_l_s[i] = range_l_e[i] = NULL;

  /* undefined on downright boundary */
  for(x=0;x<p;x++) g->data[(n-1)*p+x] = NOTDEF;
  for(y=0;y<n;y++) g->data[p*y+p-1]   = NOTDEF;

  /*** remaining part ***/
  for(x=0;x<p-1;x++)
    for(y=0;y<n-1;y++)
      {
        adr = y*p+x;

        /* norm 2 computation */
        com1 = in->data[adr+p+1] - in->data[adr];
        com2 = in->data[adr+1]   - in->data[adr+p];
        gx = com1+com2;
        gy = com1-com2;
        norm2 = gx*gx+gy*gy;

        (*modgrad)->data[adr] = sqrtf(norm2/4.0);

        if(norm2 <= threshold) /* norm too small, gradient no defined */
          g->data[adr] = NOTDEF;
        else
          {
            /* angle computation */
            g->data[adr] = atan2f(gx,-gy);
            
            /* store the point in the right bin,
               according to its norm */
            i = (int)(norm2 * f_n_bins / f_max_grad);
            if(i>=n_bins) i = n_bins-1;
            if( range_l_e[i]==NULL )
              range_l_s[i] = range_l_e[i] = list+list_count++;
            else
              {
                range_l_e[i]->next = list+list_count;
                range_l_e[i] = list+list_count++;
              }
            range_l_e[i]->x = x;
            range_l_e[i]->y = y;
            range_l_e[i]->next = NULL;
          }
      }

  /* make the list of points "ordered" by norm value */
  for(i=n_bins-1; i>0 && range_l_s[i]==NULL; i--);
  start = range_l_s[i];
  end = range_l_e[i];
  if(start!=NULL)
    for(i--;i>0; i--)
      if( range_l_s[i] != NULL )
        {
          end->next = range_l_s[i];
          end = range_l_e[i];
        }
  *list_p = start;

  /* free memory */
  free(range_l_s);
  free(range_l_e);

  return g;
}

/*--------------------------------------------------------------------------*/
/*
   find if the point x,y in angles have angle theta up to precision prec
 */
int isaligned(int x, int y, image_float angles, float theta, float prec)
{
  float a = angles->data[ x + y * angles->xsize ];

  if( a == NOTDEF ) return FALSE;

  /* it is assumed that theta and a are in the range [-pi,pi] */
  theta -= a;
  if( theta < 0.0 ) theta = -theta;
  if( theta > M_3_2_PI )
    {
      theta -= M_2__PI;
      if( theta < 0.0 ) theta = -theta;
    }

  return theta < prec;
}

/*--------------------------------------------------------------------------*/
float angle_diff(float a, float b)
{
  a -= b;
  while( a <= -M_PI ) a += 2.0*M_PI;
  while( a >   M_PI ) a -= 2.0*M_PI;
  if( a < 0.0 ) a = -a;
  return a;
}


/*--------------------------------------------------------------------------*/
/*-------------------------- NFA computation -------------------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/*
   Computes the logarithm of NFA to base 10.

   NFA = NT.b(n,k,p)
   the return value is log10(NFA)

   n,k,p - binomial parameters.
   logNT - logarithm of Number of Tests
 */
#define TABSIZE 100000
double nfa(int n, int k, double p, double logNT)
{
  static double inv[TABSIZE];   /* table to keep computed inverse values */
  double tolerance = 0.1;       /* an error of 10% in the result is accepted */
  double log1term,term,bin_term,mult_term,bin_tail,err;
  double p_term = p / (1.0-p);
  int i;

  if( n<0 || k<0 || k>n || p<0.0 || p>1.0 )
    error("wrong n, k or p values in nfa()");

  if( n==0 || k==0 ) return -logNT;
  if( n==k ) return -logNT - (double)n * log10(p);

  log1term = lgamma((double)n+1.0) - lgamma((double)k+1.0)
           - lgamma((double)(n-k)+1.0)
           + (double)k * log(p) + (double)(n-k) * log(1.0-p);

  term = exp(log1term);
  if(term==0.0) return -log1term / M_LN10 - logNT; /* just the first term */

  bin_tail = term;
  for(i=k+1;i<=n;i++)
    {
      bin_term = (double)(n-i+1) * ( i<TABSIZE ?
                   (inv[i] ? inv[i] : (inv[i]=1.0/(double)i)) : 1.0/(double)i );
      mult_term = bin_term * p_term;
      term *= mult_term;
      bin_tail += term;
      if(bin_term<1.0)
        {
          /* when bin_term<1 then mult_term_j<mult_term_i for j>i.
             then, the error on the binomial tail when truncated at
             the i term can be bounded by a geometric serie of form
             term_i * sum mult_term_i^j.                            */
          err = term * ( ( 1.0 - pow( mult_term, (double)(n-i+1) ) ) /
                         (1.0-mult_term) - 1.0 );

          /* one wants an error at most of tolerance*final_result, or:
             tolerance * abs(-log10(bin_tail)-logNT).
             now, the error that can be accepted on bin_tail is
             given by tolerance*final_result divided by the derivative
             of -log10(x) when x=bin_tail. that is:
             tolerance * abs(-log10(bin_tail)-logNT) / (1/bin_tail)
             finally, we truncate the tail if the error is less than:
             tolerance * abs(-log10(bin_tail)-logNT) * bin_tail        */
          if( err < tolerance * abs(-log10(bin_tail)-logNT) * bin_tail ) break;
        }
    }
  return -log10(bin_tail) - logNT;
}


/*--------------------------------------------------------------------------*/
/*------------------------- Rectangle structure ----------------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
struct rect /* line segment with width */
{
  float x1,y1,x2,y2;  /* first and second point of the line segment */
  float width;        /* rectangle width */
  float x,y;          /* center of the rectangle */
  float theta;        /* angle */
  float dx,dy;        /* vector with the line segment angle */
  float prec;         /* tolerance angle */
  double p;           /* probability of a point with angle within prec */
};

/*--------------------------------------------------------------------------*/
void rect_copy(struct rect * in, struct rect * out)
{
  out->x1 = in->x1;
  out->y1 = in->y1;
  out->x2 = in->x2;
  out->y2 = in->y2;
  out->width = in->width;
  out->x = in->x;
  out->y = in->y;
  out->theta = in->theta;
  out->dx = in->dx;
  out->dy = in->dy;
  out->prec = in->prec;
  out->p = in->p;
}

/*--------------------------------------------------------------------------*/
void add2image(image_char out, struct rect * r)
{
  float x = r->x1;
  float y = r->y1;
  int len = (int)ceilf(dist(r->x1,r->y1,r->x2,r->y2));
  int n;

  for(n=0; n<len; n++, x+=r->dx, y+=r->dy)
    out->data[ (int)x + (int)y * out->xsize] = 255;
}

/*--------------------------------------------------------------------------*/
/*
   rectangle points iterator
 */
typedef struct
{
  float vx[4];
  float vy[4];
  float ys,ye;
  int x,y;
} rect_iter;

/*--------------------------------------------------------------------------*/
float inter_low(float x, float x1, float y1, float x2, float y2)
{
  if( x1 > x2 || x < x1 || x > x2 )
    {
      fprintf(stderr,"inter_low: x %g x1 %g x2 %g.\n",x,x1,x2);
      error("Imposible situation.");
    }
  if( x1==x2 && y1<y2 ) return y1;
  if( x1==x2 && y1>y2 ) return y2;
  return y1 + (x-x1) * (y2-y1) / (x2-x1);
}

/*--------------------------------------------------------------------------*/
float inter_hi(float x, float x1, float y1, float x2, float y2)
{
  if( x1 > x2 || x < x1 || x > x2 )
    {
      fprintf(stderr,"inter_hi: x %g x1 %g x2 %g.\n",x,x1,x2);
      error("Imposible situation.");
    }
  if( x1==x2 && y1<y2 ) return y2;
  if( x1==x2 && y1>y2 ) return y1;
  return y1 + (x-x1) * (y2-y1) / (x2-x1);
}

/*--------------------------------------------------------------------------*/
void ri_del(rect_iter * iter)
{
  free(iter);
}

/*--------------------------------------------------------------------------*/
int ri_end(rect_iter * i)
{
  return (float)(i->x) > i->vx[2];
}

/*--------------------------------------------------------------------------*/
void ri_inc(rect_iter * i)
{
  if( (float)(i->x) <= i->vx[2] ) i->y++;

  while( (float)(i->y) > i->ye && (float)(i->x) <= i->vx[2] )
    {
      /* new x */
      i->x++;

      if( (float)(i->x) > i->vx[2] ) return; /* end of iteration */

      /* update lower y limit for the line */
      if( (float)i->x < i->vx[3] )
        i->ys = inter_low((float)i->x,i->vx[0],i->vy[0],i->vx[3],i->vy[3]);
      else i->ys = inter_low((float)i->x,i->vx[3],i->vy[3],i->vx[2],i->vy[2]);

      /* update upper y limit for the line */
      if( (float)i->x < i->vx[1] )
        i->ye = inter_hi((float)i->x,i->vx[0],i->vy[0],i->vx[1],i->vy[1]);
      else i->ye = inter_hi( (float)i->x,i->vx[1],i->vy[1],i->vx[2],i->vy[2]);

      /* new y */
      i->y=ceilf(i->ys);
    }
}

/*--------------------------------------------------------------------------*/
rect_iter * ri_ini(struct rect * r)
{
  float xmin = BIG_NUMBER;
  float vx[4],vy[4];
  int n,offset;
  rect_iter * i;

  i = (rect_iter *)malloc(sizeof(rect_iter));
  if(!i) error("ri_ini: Not enough memory.");

  vx[0] = r->x1 - r->dy * r->width / 2.0;
  vy[0] = r->y1 + r->dx * r->width / 2.0;
  vx[1] = r->x2 - r->dy * r->width / 2.0;
  vy[1] = r->y2 + r->dx * r->width / 2.0;
  vx[2] = r->x2 + r->dy * r->width / 2.0;
  vy[2] = r->y2 - r->dx * r->width / 2.0;
  vx[3] = r->x1 + r->dy * r->width / 2.0;
  vy[3] = r->y1 - r->dx * r->width / 2.0;

  if( r->x1 < r->x2 && r->y1 <= r->y2 ) offset = 0;
  else if( r->x1 >= r->x2 && r->y1 < r->y2 ) offset = 1;
  else if( r->x1 > r->x2 && r->y1 >= r->y2 ) offset = 2;
  else if( r->x1 <= r->x2 && r->y1 > r->y2 ) offset = 3;

  for(n=0; n<4; n++)
    {
      i->vx[n] = vx[(offset+n)%4];
      i->vy[n] = vy[(offset+n)%4];
    }

  /* starting point */
  i->x = (int)ceilf(i->vx[0]) - 1;
  i->y = (int)ceilf(i->vy[0]);
  i->ys = i->ye = -BIG_NUMBER;

  /* avance till first point */
  ri_inc(i);

  return i;
}

/*--------------------------------------------------------------------------*/
double rect_nfa(struct rect * rec, image_float angles, double logNT)
{
  rect_iter * i;
  int pts = 0;
  int alg = 0;
  double nfa_val;

  for(i=ri_ini(rec); !ri_end(i); ri_inc(i))
    if( i->x>=0 && i->y>=0 && i->x<angles->xsize && i->y<angles->ysize )
      {
        if(verbose) fprintf(stderr,"| %d %d ",i->x,i->y);
        ++pts;
        if( isaligned(i->x,i->y,angles,rec->theta,rec->prec) )
          {
            if(verbose) fprintf(stderr,"yes ");
            ++alg;
          }
      }
  ri_del(i);

  /*
  pts /= 4;
  alg /= 4;
  */

  nfa_val = nfa(pts,alg,rec->p,logNT);
  if(verbose) fprintf(stderr,"\npts %d alg %d p %g nfa %g\n",
                      pts,alg,rec->p,nfa_val);

  return nfa_val;
}


/*--------------------------------------------------------------------------*/
/*--------------------------------- REGIONS --------------------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
float get_theta( struct point * reg, int reg_size, float x, float y,
                 image_float modgrad, float reg_angle, float prec,
                 float * elongation )
{
  int i;
  float Ixx = 0.0;
  float Iyy = 0.0;
  float Ixy = 0.0;
  float lambda1,lambda2,tmp;
  float theta;
  float weight,sum;
  float a,b;

  if(reg_size <= 1) error("get_theta: region size <= 1.");


  /*----------- theta ---------------------------------------------------*/
  /*
      Region inertia matrix A:
         Ixx Ixy
         Ixy Iyy
      where
        Ixx = \sum_i y_i^2
        Iyy = \sum_i x_i^2
        Ixy = -\sum_i x_i y_i

      lambda1 and lambda2 are the eigenvalues, with lambda1 >= lambda2.
      They are found by solving the characteristic polynomial
      det(\lambda I - A) = 0.

      To get the line segment direction we want to get the eigenvector of
      the smaller eigenvalue. We have to solve a,b in:
        a.Ixx + b.Ixy = a.lambda2
        a.Ixy + b.Iyy = b.lambda2
      We want the angle theta = atan(b/a). I can be computed with
      any of the two equations:
        theta = atan( (lambda2-Ixx) / Ixy )
      or
        theta = atan( Ixy / (lambda2-Iyy) )

      When |Ixx| > |Iyy| we use the first, otherwise the second
      (just to get better numeric precision).
   */
  sum = 0.0;
  for(i=0; i<reg_size; i++)
    {
      weight = modgrad->data[ reg[i].x + reg[i].y * modgrad->xsize ];
      Ixx += ((float)reg[i].y - y) * ((float)reg[i].y - y) * weight;
      Iyy += ((float)reg[i].x - x) * ((float)reg[i].x - x) * weight;
      Ixy -= ((float)reg[i].x - x) * ((float)reg[i].y - y) * weight;
      sum += weight;
    }
  if( sum <= 0.0 ) error("get_theta: weights sum less or equal to zero.");
  Ixx /= sum;
  Iyy /= sum;
  Ixy /= sum;
  lambda1 = ( Ixx+Iyy + sqrtf( (Ixx-Iyy)*(Ixx-Iyy) + 4 * Ixy * Ixy ) ) / 2.0;
  lambda2 = ( Ixx+Iyy - sqrtf( (Ixx-Iyy)*(Ixx-Iyy) + 4 * Ixy * Ixy ) ) / 2.0;
  if( fabsf(lambda1) < fabsf(lambda2) )
    {
      fprintf(stderr,"Ixx %g Iyy %g Ixy %g lamb1 %g lamb2 %g - lamb1 < lamb2\n",
                      Ixx,Iyy,Ixy,lambda1,lambda2);
      tmp = lambda1;
      lambda1 = lambda2;
      lambda2 = tmp;
    }
  if(verbose) fprintf(stderr,"Ixx %g Iyy %g Ixy %g lamb1 %g lamb2 %g\n",
                      Ixx,Iyy,Ixy,lambda1,lambda2);

  *elongation = lambda1/lambda2;

  if( fabs(Ixx) > fabs(Iyy) )
    theta = atan2f( lambda2 - Ixx , Ixy );
  else
    theta = atan2f( Ixy, lambda2 - Iyy );

  /* the previous procedure don't cares orientations,
     so it could be wrong by 180 degrees.
     here is corrected if necessary */
  if( angle_diff(theta,reg_angle) > prec ) theta += M_PI;

  return theta;
}

/*--------------------------------------------------------------------------*/
float region2rect(struct point * reg, int reg_size, image_float modgrad,
                  float reg_angle, float prec, double p, struct rect * rec)
{
  float x,y,dx,dy,l,w,lf,lb,wl,wr,theta,weight,sum,sum_th,s,elongation;
  int i,n;
  int l_min,l_max,w_min,w_max;

  /* center */
  x = y = sum = 0.0;
  for(i=0; i<reg_size; i++)
    {
      weight = modgrad->data[ reg[i].x + reg[i].y * modgrad->xsize ];
      x += (float)reg[i].x * weight;
      y += (float)reg[i].y * weight;
      sum += weight;
    }
  if( sum <= 0.0 ) error("region2rect: weights sum equal to zero.");
  x /= sum;
  y /= sum;
  if(verbose) fprintf(stderr,"center x %g y %g\n",x,y);

  /* theta */
  theta = get_theta(reg,reg_size,x,y,modgrad,reg_angle,prec,&elongation);
  if(verbose) fprintf(stderr,"theta %g\n",theta);

  /* length and width */
  lf = lb = wl = wr = 0.5;
  l_min = l_max = w_min = w_max = 0;
  dx = cosf(theta);
  dy = sinf(theta);
  for(i=0; i<reg_size; i++)
    {
      l =  ((float)reg[i].x - x) * dx + ((float)reg[i].y - y) * dy;
      w = -((float)reg[i].x - x) * dy + ((float)reg[i].y - y) * dx;
      weight = modgrad->data[ reg[i].x + reg[i].y * modgrad->xsize ];

      n = (int)MY_ROUND( l * (float)sum_res );
      if(n<l_min) l_min = n;
      if(n>l_max) l_max = n;
      sum_l[sum_offset + n] += weight;

      n = (int)MY_ROUND( w * (float)sum_res );
      if(n<w_min) w_min = n;
      if(n>w_max) w_max = n;
      sum_w[sum_offset + n] += weight;
    }

  sum_th = 0.01 * sum; /* weight threshold for selecting region */
  for(s=0.0,i=l_min; s<sum_th && i<=l_max; i++) s += sum_l[sum_offset + i];
  lb = ( (float)(i-1) - 0.5 ) / (float)sum_res;
  for(s=0.0,i=l_max; s<sum_th && i>=l_min; i--) s += sum_l[sum_offset + i];
  lf = ( (float)(i+1) + 0.5 ) / (float)sum_res;

  sum_th = 0.01 * sum; /* weight threshold for selecting region */
  for(s=0.0,i=w_min; s<sum_th && i<=w_max; i++) s += sum_w[sum_offset + i];
  wr = ( (float)(i-1) - 0.5 ) / (float)sum_res;
  for(s=0.0,i=w_max; s<sum_th && i>=w_min; i--) s += sum_w[sum_offset + i];
  wl = ( (float)(i+1) + 0.5 ) / (float)sum_res;

  if(verbose) fprintf(stderr,"lb %g lf %g wr %g wl %g\n",lb,lf,wr,wl);

  /* free vector */
  for(i=l_min; i<=l_max; i++) sum_l[sum_offset + i] = 0.0;
  for(i=w_min; i<=w_max; i++) sum_w[sum_offset + i] = 0.0;

  rec->x1 = x + lb * dx;
  rec->y1 = y + lb * dy;
  rec->x2 = x + lf * dx;
  rec->y2 = y + lf * dy;
  rec->width = wl - wr;
  rec->x = x;
  rec->y = y;
  rec->theta = theta;
  rec->dx = dx;
  rec->dy = dy;
  rec->prec = prec;
  rec->p = p;

  if( rec->width < 1.0 ) rec->width = 1.0;

  return elongation;
}

/*--------------------------------------------------------------------------*/
void region_grow( int x, int y, image_float angles, struct point * reg,
                  int * reg_size, float * reg_angle, image_char used,
                  float prec, int radius,
                  image_float  modgrad, double p, int min_reg_size )
{
  float sumdx,sumdy;
  int iteration;
  int xx,yy,i;
  struct rect rec;
  int n;
  rect_iter * ii;
  float elongation;

  /* first point of the region */
  *reg_size = 1;
  reg[0].x = x;
  reg[0].y = y;
  *reg_angle = angles->data[x+y*angles->xsize];
  sumdx = cosf(*reg_angle);
  sumdy = sinf(*reg_angle);
  used->data[x+y*used->xsize] = USED;

  /* try neighbors as new region points */
  for(i=0; i<*reg_size; i++)
    for(xx=reg[i].x-radius; xx<=reg[i].x+radius; xx++)
      for(yy=reg[i].y-radius; yy<=reg[i].y+radius; yy++)
        if( xx>=0 && yy>=0 && xx<used->xsize && yy<used->ysize &&
            used->data[xx+yy*used->xsize] != USED &&
            isaligned(xx,yy,angles,*reg_angle,prec) )
          {
            /* add point */
            used->data[xx+yy*used->xsize] = USED;
            reg[*reg_size].x = xx;
            reg[*reg_size].y = yy;
            ++(*reg_size);

            /* update reg_angle */
            sumdx += cosf(angles->data[xx+yy*angles->xsize]);
            sumdy += sinf(angles->data[xx+yy*angles->xsize]);
            *reg_angle = atan2f(sumdy,sumdx);

            if( FALSE &&
                *reg_size > 2*min_reg_size && *reg_size % min_reg_size/2 == 0 )
              {
                elongation = region2rect(reg,*reg_size,modgrad,
                                         *reg_angle,prec,p,&rec);

                rec.width -= 2.0; /* to accept an error in width */

                if( rec.width <= 0.0 ) rec.width = 1.0;
                for( n=0, ii=ri_ini(&rec); !ri_end(ii); ri_inc(ii), ++n );

                if( (elongation > 20.0 && *reg_size < n/2) ||
                    *reg_size < n/4 ) return;
              }
          }

  if(verbose) /* print region points */
    {
      fprintf(stderr,"region found: %d points\n",*reg_size);
      for(i=0; i<*reg_size; i++) fprintf(stderr,"| %d %d ",reg[i].x,reg[i].y);
      fprintf(stderr,"\n");
    }
}

/*--------------------------------------------------------------------------*/
double rect_improve(struct rect * rec, float theta, image_float angles,
                    double logNT, double eps)
{
  struct rect r;
  double nfa_val,nfa_new;
  float delta = 0.5;
  float delta_2 = delta / 2.0;
  float delta_diag = 0.25;
  float new_theta;
  float x,y,len;
  int n;

  nfa_val = rect_nfa(rec,angles,logNT);

  if( nfa_val > eps ) return nfa_val;

  rect_copy(rec,&r);
  for(n=0; n<5; n++)
    {
      r.p /= 2.0;
      r.prec = M_PI * r.p;
      nfa_new = rect_nfa(&r,angles,logNT);
      if( nfa_new > nfa_val )
        {
          nfa_val = nfa_new;
          rect_copy(&r,rec);
        }
    }

  if( nfa_val > eps ) return nfa_val;

  rect_copy(rec,&r);
  for(n=0; n<5; n++)
    {
      if( (r.width - delta) >= 0.5 )
        {
          r.width -= delta;
          nfa_new = rect_nfa(&r,angles,logNT);
          if( nfa_new > nfa_val )
            {
              rect_copy(&r,rec);
              nfa_val = nfa_new;
            }
        }
    }

  if( nfa_val > eps ) return nfa_val;

  rect_copy(rec,&r);
  for(n=0; n<5; n++)
    {
      if( (r.width - delta) >= 0.5 )
        {
          r.x1 += -r.dy * delta_2;
          r.y1 +=  r.dx * delta_2;
          r.x2 += -r.dy * delta_2;
          r.y2 +=  r.dx * delta_2;
          r.width -= delta;
          nfa_new = rect_nfa(&r,angles,logNT);
          if( nfa_new > nfa_val )
            {
              rect_copy(&r,rec);
              nfa_val = nfa_new;
            }
        }
    }

  if( nfa_val > eps ) return nfa_val;

  rect_copy(rec,&r);
  for(n=0; n<5; n++)
    {
      if( (r.width - delta) >= 0.5 )
        {
          r.x1 -= -r.dy * delta_2;
          r.y1 -=  r.dx * delta_2;
          r.x2 -= -r.dy * delta_2;
          r.y2 -=  r.dx * delta_2;
          r.width -= delta;
          nfa_new = rect_nfa(&r,angles,logNT);
          if( nfa_new > nfa_val )
            {
              rect_copy(&r,rec);
              nfa_val = nfa_new;
            }
        }
    }

  if( nfa_val > eps ) return nfa_val;

  rect_copy(rec,&r);
  for(n=0; n<5; n++)
    {
      r.p /= 2.0;
      r.prec = M_PI * r.p;
      nfa_new = rect_nfa(&r,angles,logNT);
      if( nfa_new > nfa_val )
        {
          nfa_val = nfa_new;
          rect_copy(&r,rec);
        }
    }

  return nfa_val;
}


/*--------------------------------------------------------------------------*/
/*------------------------ LINE SEGMENT DETECTION --------------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
void LineSegmentDetection( image_float in, float q, int d,
                           double eps, int * x, int * y,
                           FILE * fout, FILE * arout, ntuple_float out,
                           image_char iout, image_char reg_out, int * reg_n,
                           int n_bins, int max_grad,
                           float scale, float sigma_scale )
{
  float rho = q/sinf(M_PI/(float)d); /* threshold on the gradient */
  struct coorlist * list_p;
  void * mem_p;
  image_float modgrad;
  image_float filtered_in = NULL;
  image_float angles;
  image_char used;
  int xsize,ysize;
  struct point * reg;
  int reg_size;
  float reg_angle;
  float prec = M_PI/(float)d;
  double p = 1.0/(double)d;
  double nfa_val;
  double logNT;
  struct rect rec;
  int radius = 1;
  int i;
  int segnum = 0;
  rect_iter * ri;
  int min_reg_size;

  /* scale (if necesary) and angles computation */
  if( scale != 1.0 )
    {
      filtered_in = gaussian_sampler( in, scale, sigma_scale );
      angles = ll_angle( filtered_in, rho, &list_p, &mem_p,
                         &modgrad, n_bins, max_grad );
      xsize = filtered_in->xsize;
      ysize = filtered_in->ysize;
    }
  else
    {
      angles = ll_angle(in,rho,&list_p,&mem_p,&modgrad,n_bins,max_grad);
      xsize = in->xsize;
      ysize = in->ysize;
    }
  logNT = 5.0*(log10((double)xsize)+log10((double)ysize))/2.0;
  min_reg_size = -logNT/log10(p); /* minimal number of point that can
                                     give a meaningful event */

  /* get memory */
  if(verbose) fprintf(stderr,"getting memory.\n");
  used = new_image_char_ini(xsize,ysize,NOTUSED);
  reg = (struct point *)calloc(xsize * ysize, sizeof(struct point));
  reg2 = (struct point *)calloc(xsize * ysize, sizeof(struct point));
  sum_offset = sum_res*(int)ceil(hypot((double)xsize,(double)ysize))+2;
  sum_l = (float *)calloc(2*sum_offset,sizeof(float));
  sum_w = (float *)calloc(2*sum_offset,sizeof(float));
  if( !reg || !reg2 || !sum_l || !sum_w ) error("Not enough memory!\n");
  for(i=0;i<2*sum_offset;i++) sum_l[i] = sum_w[i] = 0;

  /* just start at point x,y option */
  if( x && y && *x > 0 && *y > 0 && *x < (xsize-1) && *y < (ysize-1) )
    {
      if(verbose) fprintf(stderr,"starting only at point %d,%d.\n",*x,*y);
      list_p = (coorlist*)mem_p;
      list_p->x = *x;
      list_p->y = *y;
      list_p->next = NULL;
    }

  /* search for line segments */
  for(;list_p; list_p = list_p->next )
    if( (used->data[ list_p->x + list_p->y * used->xsize ] == NOTUSED &&
         angles->data[ list_p->x + list_p->y * angles->xsize ] != NOTDEF) )
      {
        if(verbose)
          fprintf(stderr,"try to find a line segment starting on %d,%d.\n",
                  list_p->x,list_p->y);

        /* find the region of conexed point and ~equal angle */
        region_grow( list_p->x, list_p->y, angles, reg, &reg_size,
                     &reg_angle, used, prec, radius,
                     modgrad, p, min_reg_size );

        /* just process regions with a minimun number of points */
        if( reg_size < min_reg_size )
          {
            if(verbose) fprintf(stderr,"region too small, discarded.\n");
            for(i=0; i<reg_size; i++)
              used->data[reg[i].x+reg[i].y*used->xsize] = NOTINI;
            continue;
          }

        if(verbose) fprintf(stderr,"rectangular approximation of region.\n");
        region2rect(reg,reg_size,modgrad,reg_angle,prec,p,&rec);
        
        if(verbose) fprintf(stderr,"improve rectangular approximation.\n");
        nfa_val = rect_improve(&rec,reg_angle,angles,logNT,eps);
        
        if( nfa_val > eps )
          {
            if(verbose) fprintf(stderr,"line segment found! num %d nfa %g\n",
                                segnum,nfa_val);

            /*
              1.0 must be added to the output coordinates for two factors:

              1) the gradient is computed using a 2x2 window so the
                 computed gradient is valid at a point with offset 0.5,0.5

              2) an offset of 0.5,0.5 is added to the output value so
                 that (in a visualization with software like fkview,
                 where the lower left corner of 0,0 pixel have
                 coordinates 0,0) a line segment starting at
                 coordinates 0,0 will be see as starting at the center
                 of the pixel 0,0.
            */
            rec.x1 += 1.0; rec.y1 += 1.0;
            rec.x2 += 1.0; rec.y2 += 1.0;

            if( scale != 1.0)
              {
                rec.x1 /= scale;
                rec.y1 /= scale;
                rec.x2 /= scale;
                rec.y2 /= scale;
                rec.width /= scale;
              }

            if(fout) fprintf(fout,"%f %f %f %f %f\n",
                             rec.x1,rec.y1,rec.x2,rec.y2,rec.width);
            if(out) add_5tuple(out,rec.x1,rec.y1,rec.x2,rec.y2,rec.width);
            if(iout) add2image(iout,&rec);
            if( reg_out && (!reg_n || segnum == *reg_n) )
              for(i=0; i<reg_size; i++)
                  reg_out->data[reg[i].x+reg[i].y*reg_out->xsize] = 255;
            if(arout)
              {
                fprintf(arout,"region %d:",segnum);
                for(i=0; i<reg_size; i++)
                  fprintf(arout," %d,%d;",reg[i].x,reg[i].y);
                fprintf(arout,"\n");
              }

            ++segnum;
          }
        else
          for(i=0; i<reg_size; i++)
            used->data[reg[i].x+reg[i].y*used->xsize] = NOTINI;
      }

  /* free memory */
  if( filtered_in != NULL ) free_image_float(filtered_in);
  free_image_float(angles);
  free_image_float(modgrad);
  free_image_char(used);
  free((void *)reg);
  free((void *)reg2);
  free((void *)sum_l);
  free((void *)sum_w);
  free((void *)mem_p);
}


/*--------------------------------------------------------------------------*/
/*---------------------- MEGAWAVE RELATED FUNCTIONS ------------------------*/
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
#ifdef MW_FLG
image_char image_char_from_Cimage(Cimage in)
{
  image_char image;

  if(in == NULL) return NULL;
  image = (image_char)malloc(sizeof(struct image_char_s));
  if( image == NULL ) error("Not enough memory.");
  image->data = (unsigned char *)in->gray;
  image->xsize = in->ncol;
  image->ysize = in->nrow;

  return image;
}
#endif

/*--------------------------------------------------------------------------*/
#ifdef MW_FLG
Cimage Cimage_from_image_char(Cimage out, image_char in)
{
  out = mw_change_cimage(out,0,0);
  if( out == NULL ) error("Not enough memory.");
  out->ncol = in->xsize;
  out->nrow = in->ysize;
  out->gray = in->data;
  return out;
}
#endif

/*--------------------------------------------------------------------------*/
#ifdef MW_FLG
image_float image_float_from_Fimage(Fimage in)
{
  image_float image;

  if(in == NULL) return NULL;
  image = (image_float)malloc(sizeof(struct image_float_s));
  if( image == NULL ) error("Not enough memory.");
  image->data = (float *)in->gray;
  image->xsize = in->ncol;
  image->ysize = in->nrow;

  return image;
}
#endif

/*--------------------------------------------------------------------------*/
#ifdef MW_FLG
Fimage Fimage_from_image_float(Fimage out, image_float in)
{
  out = mw_change_fimage(out,0,0);
  if( out == NULL ) error("Not enough memory.");
  out->ncol = in->xsize;
  out->nrow = in->ysize;
  out->gray = in->data;
  return out;
}
#endif

/*--------------------------------------------------------------------------*/
#ifdef MW_FLG
Flist Flist_from_n_tuple_float(Flist out, ntuple_float in)
{
  out = mw_change_flist(out,0,0,0);
  if( out == NULL ) error("Not enough memory.");
  out->size = in->size;
  out->max_size = in->max_size;
  out->dim = in->dim;
  out->values = in->values;
  return out;
}
#endif

/*--------------------------------------------------------------------------*/
/*
  add a line segment given by start and end coordintes to a Flists
 */
#ifdef MW_FLG
void add2Flists(out,x1,y1,x2,y2)
Flists out;
float x1,y1,x2,y2;
{
  Flist aux;

  aux = mw_change_flist( NULL, 2, 2, 2 );
  if( aux == NULL ) error("Not enough memory.");
      
  aux->values[0] = x1;
  aux->values[1] = y1;
  aux->values[2] = x2;
  aux->values[3] = y2;
  
  if( out->size == out->max_size )
    if( mw_enlarge_flists(out) == NULL ) error("Not enough memory.");
  out->list[out->size++] = aux;
}
#endif

/*--------------------------------------------------------------------------*/
/*
  add a degrade given by start and end coordintes to a Flists
 */
#ifdef MW_FLG
void add_rect2Flists(Flists out,float * r,int arrow)
{
  Flist aux;
  int i;
  float theta = atan2f( r[3] - r[1], r[2] - r[0] );
  float dx = cosf(theta);
  float dy = sinf(theta);

  if(arrow) aux = mw_change_flist( NULL, 6, 6, 2 );
  else aux = mw_change_flist( NULL, 5, 5, 2 );
  if( aux == NULL ) error("Not enough memory.");
      
  i=0;

  aux->values[i++] = r[0] - dy * r[4] / 2.0;
  aux->values[i++] = r[1] + dx * r[4] / 2.0;
  aux->values[i++] = r[2] - dy * r[4] / 2.0;
  aux->values[i++] = r[3] + dx * r[4] / 2.0;

  if(arrow)
    {
      aux->values[i++] = r[2] + dx * r[4] / 2.0;
      aux->values[i++] = r[3] + dy * r[4] / 2.0;
    }

  aux->values[i++] = r[2] + dy * r[4] / 2.0;
  aux->values[i++] = r[3] - dx * r[4] / 2.0;
  aux->values[i++] = r[0] + dy * r[4] / 2.0;
  aux->values[i++] = r[1] - dx * r[4] / 2.0;
  aux->values[i++] = aux->values[0];
  aux->values[i++] = aux->values[1];

  if( out->size == out->max_size )
    if( mw_enlarge_flists(out) == NULL ) error("Not enough memory.");
  out->list[out->size++] = aux;
}
#endif


/*--------------------------------------------------------------------------*/
/*                           MEGAWAVE2'S MAIN                               */
/*--------------------------------------------------------------------------*/
#if 0
void lsd( scale, sigma_scale, q, d, eps, n_bins, max_grad, verb, x, y,
          regout, ascii_reg, n, iout, fout, rout, arrow, ascii, image, out )
float  *scale;
float  *sigma_scale;
float  *q;
int    *d;
double *eps;
int    *n_bins;
int    *max_grad;
char   *verb;
int    *x;
int    *y;
Cimage regout;
char   *ascii_reg;
int    *n;
Cimage iout;
Flists fout;
Flists rout;
char   *arrow;
char   *ascii;
Fimage image;
Flist  out;
{
  image_float in = image_float_from_Fimage(image);
  image_char image_out = NULL;
  image_char image_reg = NULL;
  ntuple_float segs = new_ntuple_float(5);
  int i;
  if(verb) verbose = TRUE;
  if(iout) image_out = new_image_char_ini(image->ncol,image->nrow,0);
  if(regout) image_reg = new_image_char_ini(image->ncol,image->nrow,0);

  LineSegmentDetection( in, *q, *d, *eps, x, y, (ascii?stdout:NULL),
                        (ascii_reg?stdout:NULL), segs, image_out, image_reg,
                        n, *n_bins, *max_grad, *scale, *sigma_scale );

  /* ----------- the rest is to change the output format ----------- */

  /* Flist output */
  out = Flist_from_n_tuple_float(out,segs);

  /* Flists output */
  if(fout)
    {
      fout = mw_change_flists(fout,0,0);
      for(i=0;i<segs->size;i++)
        add2Flists( fout, segs->values[i*segs->dim + 0],
                          segs->values[i*segs->dim + 1],
                          segs->values[i*segs->dim + 2],
                          segs->values[i*segs->dim + 3] );
    }

  /* Rectangles Flists output */
  if(rout)
    {
      rout = mw_change_flists(rout,0,0);
      for(i=0;i<segs->size;i++)
        add_rect2Flists(rout,segs->values+i*segs->dim,(int)arrow);
    }

  /* image output */
  if(iout) iout = Cimage_from_image_char(iout,image_out);

  /* regions image output */
  if(regout) regout = Cimage_from_image_char(regout,image_reg);

  /* free memory */
  /* free_image_float(in); */ /* this is not good because free mw input image */
  free((void *)in); /* this is better */

  if(image_out) free(image_out); /* data should not be freed, used in output */
  if(image_reg) free(image_reg); /* data should not be freed, used in output */
}
#endif


/*--------------------------------------------------------------------------*/
/*                                C MAIN                                    */
/*--------------------------------------------------------------------------*/
#if 0
int main(int argc,char ** argv)
{
  FILE * input;
  FILE * output;
  image_float in;

  /* LSD parameters */
  float q = 2.0;       /* Bound to the quantization error on the
                          gradient norm. */
  int d = 8;           /* Number of allowed gradient directions. */
  double eps = 0.0;    /* Detection threshold, -log10(NFA). */
  int n_bins = 16256;  /* Number of bins in pseudo-ordering of gradient modulus.
                          This default value is selected to work good
                          on images with gray levels in [0,255]. */
  int max_grad=260100; /* Gradient modulus in the highest bin. The default
                          value corresponds to the highest gradient modulus
                          on images with gray levels in [0,255]. */

  if( argc <= 1)
    {
      fprintf(stderr,"lsd - line segment detector\n");
      fprintf(stderr,"rafael grompone von gioi - 2008\n\n");
      fprintf(stderr,"use: lsd in.pgm [out.txt]\n");
      exit(1);
    }

  /* read input file */
  in = read_pgm_image_float(argv[1]);
  
  /* output file */
  if( argc > 2 ) output = fopen(argv[2],"w");
  else output = stdout;

  /* execute LSD */
  LineSegmentDetection(in,q,d,eps,NULL,NULL,output,NULL,NULL,NULL,NULL,NULL,
                       n_bins,max_grad,1.0,0.9);

  free_image_float(in);
  fclose(output);
}
#endif
/*--------------------------------------------------------------------------*/
