/*---------------------------------------------------------------------------

  LSD - Line Segment Detector on digital images

  Copyright 2007,2008 rafael grompone von gioi (grompone@gmail.com)

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  MA 02110-1301, USA.

  ---------------------------------------------------------------------------*/

#define NO_MEGAWAVE

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <ctype.h>

typedef struct image_char_s
{
  unsigned char * data;
  int xsize,ysize;
} * image_char;

void free_image_char(image_char i);
image_char new_image_char(int xsize,int ysize);

/*--------------------------------------------------------------------------*/
typedef struct image_float_s
{
  float * data;
  int xsize,ysize;
} * image_float;

void free_image_float(image_float i);
image_float new_image_float(int xsize,int ysize);

/*--------------------------------------------------------------------------*/
typedef struct ntuple_float_s
{
  int size;
  int max_size;
  int dim;
  float * values;
} * ntuple_float;

void free_ntuple_float(ntuple_float in);
ntuple_float new_ntuple_float(int dim);

void LineSegmentDetection( image_float in, float q, int d,
                           double eps, int * x, int * y,
                           FILE * fout, FILE * arout, ntuple_float out,
                           image_char iout, image_char reg_out, int * reg_n,
                           int n_bins, int max_grad,
                           float scale, float sigma_scale );
