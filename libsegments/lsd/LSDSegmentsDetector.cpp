#include "lsd/LSDSegmentsDetector.hpp"

#include "lsd.h"
#include "../Segment.hpp"
#include "../utils/Image.hpp"

using namespace libsegments;
using namespace libsegments::lsd;


LSDSegmentsDetector::LSDSegmentsDetector() : m_minimalSegmentLength(15)
{
  
}

std::vector< Segment > LSDSegmentsDetector::detectSegments( const cv::Mat& _image )
{
  image_float in = new_image_float( _image.cols, _image.rows );
  
  for(int y = 0; y < in->ysize; y++ )
  {
    for(int x = 0; x < in->xsize; x++ )
    {
      in->data[ x + y * in->xsize ] = utils::image::getPixelValue<uchar>( _image, x, y, 0);
    }
  }

  
  /* LSD parameters */
  float q = 2.0;       /* Bound to the quantization error on the
                          gradient norm. */
  int d = 8;           /* Number of allowed gradient directions. */
  double eps = 0.0;    /* Detection threshold, -log10(NFA). */
  int n_bins = 16256;  /* Number of bins in pseudo-ordering of gradient modulus.
                          This default value is selected to work good
                          on images with gray levels in [0,255]. */
  int max_grad=260100; /* Gradient modulus in the highest bin. The default
                          value corresponds to the highest gradient modulus
                          on images with gray levels in [0,255]. */
  
  ntuple_float segs = new_ntuple_float(5);
  
  LineSegmentDetection( in, q, d, eps, 0, 0,
                           0, 0, segs, 0, 0, 0,
                           n_bins, max_grad, 1.0, 0.9 );
  
  
  std::vector< Segment > results;
  
  double ml = minimalSegmentLength() * minimalSegmentLength();
  for(int i = 0; i < segs->size; ++i)
  {
    float* seg = segs->values + 5 * i;
    Segment s;
    s.x1 = seg[0];
    s.y1 = seg[1];
    s.x2 = seg[2];
    s.y2 = seg[3];
    double dx = s.x1 - s.x2;
    double dy = s.y1 - s.y2;
    if( (dx * dx + dy * dy) > ml )
    {
      results.push_back( s );
    }
  }
  
  
  free_image_float( in );
  
  return results;
}

