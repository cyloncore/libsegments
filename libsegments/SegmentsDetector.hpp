#ifndef _LIBSEGMENTS_SEGMENTS_DETECTOR_HPP_
#define _LIBSEGMENTS_SEGMENTS_DETECTOR_HPP_

#include <opencv2/opencv.hpp>

namespace libsegments {
  struct Segment;
  class SegmentsDetector
  {
    public:
      enum class Algorithm
      {
        Chaining, DSeg, HDSeg, Hough, LSD, ELSED
      };
    public:
      SegmentsDetector();
      virtual ~SegmentsDetector();
      virtual std::vector< Segment > detectSegments( const cv::Mat& _image ) = 0;
    public:
      static SegmentsDetector* create(Algorithm _algorithm);
  };
}

#endif
