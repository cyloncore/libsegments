#ifndef _HSEG_HOUGHSEGMENTSDETECTOR_H_
#define _HSEG_HOUGHSEGMENTSDETECTOR_H_

#include <vector>

#include "../SegmentsDetector.hpp"
#include "../utils/Macros.hpp"

namespace libsegments {
  struct Segment;
  namespace hough {
    /**
     * @ingroup hseg
     * Detect segments using the Probabilistic Hough method.
     */
    class HoughSegmentsDetector : public SegmentsDetector {
      public:
        HoughSegmentsDetector();
        std::vector< Segment > detectSegments( const cv::Mat& _image );
      public:
        /// Distance resolution in pixel-related units.
        LS_DEFINE_PARAM_RW(double, rho, setRho);
        /// Angle resolution measured in radians.
        LS_DEFINE_PARAM_RW(double, theta, setTheta);
        /// Threshold parameter. A line is returned by the function if the corresponding accumulator value is greater than threshold.
        LS_DEFINE_PARAM_RW(int, threshold, setThreshold);
        /// The minimum line length.
        LS_DEFINE_PARAM_RW(double, minimumLineLength, setMinimumLineLength);
        /// maximum gap between line segments lieing on the same line to treat them as the single line segment
        LS_DEFINE_PARAM_RW(double, joinThreshold, setJoinThreshold);
        LS_DEFINE_PARAM_RW(double, cannyThreshold1, setCannyThreshold1);
        LS_DEFINE_PARAM_RW(double, cannyThreshold2, setCannyThreshold2);
        LS_DEFINE_PARAM_RW(int, apertureSize, setApertureSize);
        
    };
    
  }
}

#endif
