#include "HoughSegmentsDetector.hpp"


#include "../Segment.hpp"

using namespace libsegments;
using namespace libsegments::hough;

HoughSegmentsDetector::HoughSegmentsDetector() :
  m_rho(1), m_theta(CV_PI/180), m_threshold(50), m_minimumLineLength(50), m_joinThreshold(10), m_cannyThreshold1(50), m_cannyThreshold2(200), m_apertureSize(3)
{
}

std::vector< Segment > HoughSegmentsDetector::detectSegments( const cv::Mat& _image )
{
  cv::Mat dst;
  cv::Canny(_image, dst, m_cannyThreshold1, m_cannyThreshold2, m_apertureSize );

  std::vector<cv::Vec4i> linesP;  
  cv::HoughLinesP( dst, linesP, m_rho, m_theta, m_threshold, m_minimumLineLength, m_joinThreshold );
  
  std::vector< Segment > results;
  for(cv::Vec4i v : linesP)
  {
    Segment s;
    s.x1 = v[0];
    s.y1 = v[1];
    s.x2 = v[2];
    s.y2 = v[3];
    results.push_back( s );
  }
  // FIXME Cleanup the IPLimages and storage
  return results;
}
