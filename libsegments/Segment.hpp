#ifndef SEGMENT_HPP
#define SEGMENT_HPP

namespace libsegments {
    struct Segment {
      double x1, y1, x2, y2;
    };
}

#endif
