/* $Id: StaticPredictor.cpp 3313 2008-11-12 14:53:14Z cberger $ */

#include "StaticPredictor.hpp"
#include "../dseg/SegmentHypothesis.hpp"
#include "../dseg/LineFitterKalman2.hpp"

using namespace libsegments::dseg;
using namespace libsegments::dsegtracker;

StaticPredictor::StaticPredictor( double _posError, double _angleError ) : m_posError(_posError), m_angleError( _angleError )
{
}

SegmentHypothesis* StaticPredictor::predictionFor( const SegmentHypothesis* _sh) const
{
  double x1 = _sh->x1();
  double y1 = _sh->y1();
  double x2 = _sh->x2();
  double y2 = _sh->y2();
  
  double x_o = 0.5 * (x1 + x2);
  double y_o = 0.5 * (y1 + y2);
  
  double angle = _sh->lineFitter().angle();
  
  SegmentHypothesis* segHyp = new SegmentHypothesis( x_o, y_o, angle );
  segHyp->setExtremity1( _sh->x1(), _sh->y1() );
  segHyp->setExtremity2( _sh->x2(), _sh->y2() );
  
  segHyp->setUncertainty( m_angleError, m_posError, m_angleError, m_posError );
  
  return segHyp;
}
