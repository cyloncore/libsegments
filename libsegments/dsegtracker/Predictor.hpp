/* $Id: Predictor.hpp 3955 2009-08-03 09:56:01Z cberger $ */

#ifndef _PREDICTOR_HPP_
#define _PREDICTOR_HPP_

namespace libsegments {
  namespace dseg {
    class SegmentHypothesis;
  }
  namespace dsegtracker {
    /**
     * @ingroup dseg
     * This class defines a predictor of the position of a segment in an image.
     * This is used by the tracker.
     */
    class Predictor {
      public:
        virtual ~Predictor();
        /**
         * Creates a new segment hypothesis that can be used to track the segment.
         */
        virtual dseg::SegmentHypothesis* predictionFor( const dseg::SegmentHypothesis* ) const = 0;
    };
  }
}

#endif
