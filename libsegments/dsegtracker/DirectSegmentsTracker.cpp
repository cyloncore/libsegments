/* $Id: DirectSegmentsTracker.cpp 4091 2009-09-03 19:41:52Z cberger $ */

#include "DirectSegmentsTracker.hpp"


#include "../dseg/SegmentProbabilityEstimator.hpp"
#include "../dseg/SegmentsSet.hpp"
#include "../dseg/SegmentHypothesis.hpp"
#include "Predictor.hpp"
#include "../dseg/LineFitterKalman2.hpp"
#include "../dseg/GrowStopCondition.hpp"
#include "../dseg/Debug.hpp"
#include "../utils/Utils.hpp"
#include "../dseg/GradientStatsDescriptor.hpp"
#include "../utils/Macros_p.hpp"
#include "../utils/Image.hpp"

using namespace libsegments::dseg;
using namespace libsegments::dsegtracker;

class TrackerGrowStopCondition : public GrowStopCondition {
  public:
    TrackerGrowStopCondition(double _lastPositiveReachedT, double _lastNegativeReachedT ) :  m_lastPositiveReachedT(0.0), m_lastNegativeReachedT(0.0), m_lastPositiveT( _lastPositiveReachedT ), m_lastNegativeT( _lastNegativeReachedT )
    {
      LS_ASSERT( m_lastPositiveT >= m_lastNegativeT, "bug" );
    }
    virtual ~TrackerGrowStopCondition() {}
    virtual bool shouldStopPositiveSearch() const
    {
      return m_lastPositiveReachedT > m_lastPositiveT;
    }
    virtual bool shouldStopNegativeSearch() const
    {
      return m_lastNegativeReachedT < m_lastNegativeT;
    }
    virtual void reset()
    {
    }
    virtual void reached( double t )
    {
      if( t < 0.0  )
      {
        m_lastNegativeReachedT = t;
      } else if(t > 0.0 ) {
        m_lastPositiveReachedT = t;
      }
    }
  private:
    double m_lastPositiveReachedT;
    double m_lastNegativeReachedT;
    double m_lastPositiveT;
    double m_lastNegativeT;
};

DirectSegmentsTracker::DirectSegmentsTracker() : m_adjustPrediction( true )
{
  setMaximalSearchLength( 100.0 );
}

void DirectSegmentsTracker::trackSegment( const cv::Mat& _image, const SegmentsSet& _initialSegments, const Predictor* _predictor, SegmentsSet& _segmentStorage )
{
  int width = _image.cols;
  int height = _image.rows;
  cv::Mat dx( height, width, CV_MAKETYPE(CV_16S, 1) );
  cv::Mat dy( height, width, CV_MAKETYPE(CV_16S, 1) );
  cv::Mat proba( height, width, CV_MAKETYPE(CV_32F, 1) );
  cv::Mat inasegment( height, width, CV_MAKETYPE(CV_32S, 1) );
  
  memset( inasegment.data, 0, inasegment.rows * inasegment.step);
  
  cv::Sobel( _image, dx, CV_16S, 1, 0, 3 );
  cv::Sobel( _image, dy, CV_16S, 0, 1, 3 );
  
  // Compute maximum probability
  segmentProbabilityEstimator()->compute( dx, dy, proba  );
  
  // compute angle
  
  for( std::size_t i = 0; i < _initialSegments.count(); ++i)
  {
    const SegmentHypothesis* sh = _initialSegments.segmentAt( i );
    segCount = sh->id();
    
    LS_DEBUG_COND(DEBUG_COND, "==========================================================" );
    LS_DEBUG_COND(DEBUG_COND, sh->lineFitter().pointAt( 0 ) );
    
    SegmentHypothesis* trackHyp = _predictor->predictionFor( sh );
    
    
    if( trackHyp and ( TRACK_COND ) )
    {
      double angleHyp = trackHyp->lineFitter().angle();
//       double angleHypCov = trackHyp->lineFitter().angleCov();
    
#ifndef NDEBUG
      {
      const Eigen::Vector2d& p = sh->lineParameters();
      double x0, y0;
      projection(x0, y0, trackHyp->lineFitter().pointAt(0)(0), trackHyp->lineFitter().pointAt(0)(1), p(0), p(1));
      LS_DEBUG_COND(DEBUG_COND, trackHyp->lineFitter().pointAt(0) << " -> " << x0 << " " << y0 );
      }
#endif
      // Compute the extremum of t in negative and positive direction
      double lastPositiveT, lastNegativeT;
      trackHyp->computeTMinMax( lastNegativeT, lastPositiveT);
      
      GrowSegmentContext gsc;
      gsc.segHyp = trackHyp;
      gsc.proba = proba;
      gsc.dx = dx;
      gsc.dy = dy;
      gsc.inasegment = &inasegment; // TODO is it needed to check wether segments are in an other segments while tracking
      gsc.k_p = 1;
      gsc.k_n = -1;
      TrackerGrowStopCondition tgsc( lastPositiveT, lastNegativeT );
      gsc.growStopCondition = &tgsc;
      
      double x0, y0;
      if( adjustPrediction() )
      {
        double angle;
#if 1
        if( findInitialGuesses( gsc, sh->gradientDescriptor(), lastNegativeT, lastPositiveT, x0, y0, angle) )
#else
        if( findInitialGuess( gsc, sh->gradientDescriptor(), 0.0, x0, y0, angle, step ) )
#endif
        {
          LS_DEBUG_COND(DEBUG_COND, x0 << " " << y0 );
          trackHyp->addPoint(0.0, x0, y0, angle, 1.0 );
//     LS_DEBUG_COND(DEBUG_COND, sh->id() << " " << success << " " << trackHyp->countPoints() );
        } else {
          delete trackHyp;
          trackHyp = 0;
        }
      } else {
        Eigen::Vector2d pt0 = trackHyp->lineFitter().pointAt( 0 );
        x0 = pt0(0);
        y0 = pt0(1);
      }
      
      if( trackHyp )
      {
        gsc.x_1 = x0;
        gsc.x_2 = x0;
        gsc.y_1 = y0;
        gsc.y_2 = y0;
        bool success = growSegment( gsc );
        if( success and trackHyp->countPoints() > 1)
        {
          trackHyp->setExtremity1( gsc.x_1, gsc.y_1 );
          trackHyp->setExtremity2( gsc.x_2, gsc.y_2 );
          trackHyp->setId( sh->id() );
          if( cos( trackHyp->lineFitter().angle() - angleHyp ) > 0.95 ) // TODO Check validity using the covariance
          {
            _segmentStorage.addSegment( trackHyp );
          } else {
            delete trackHyp;
          }
        } else {
          delete trackHyp;
        }
      }
    }
  }
}

#define GUESSPOINTS 20

#define ONE_POINT_ONE_POSSIBILITY \
  if( countEnabled == 1 ) \
  { \
    for(int i = 0; i < GUESSPOINTS; ++i ) \
    { \
      Guess& currentGuess = guesses[i]; \
      if( currentGuess.enabled ) \
      { \
        _x0 = currentGuess.x0; \
        _y0 = currentGuess.y0; \
        _angle = currentGuess.angle; \
        return true; \
      } \
    } \
    LS_ERROR("Impossible error"); \
  }

inline double median( double* values, int count )
{
  std::sort( values, values + count );
  int idx = count / 2;
  if( count & 1 )
  {
    return values[ idx ];
  } else {
    return (values[ idx - 1 ] + values[ idx ]) * 0.5;
  }
}

bool DirectSegmentsTracker::findInitialGuesses( GrowSegmentContext& _context, const GradientStatsDescriptor& _gradientDescriptor, double _tMin, double _tMax, double& _x0, double& _y0, double& _angle ) const
{
//   LS_DEBUG_COND(DEBUG_COND, "FindInitialGuesses " << segCount );
  double step = (_tMax - _tMin) / GUESSPOINTS;
  struct Guess {
    bool enabled;
    double x0, y0, angle, step, dx0, dy0, cosAngle;
  };
  double cosAngles[GUESSPOINTS];
  Guess guesses[GUESSPOINTS];
  int countEnabled = 0;
  for(int i = 0;  i < GUESSPOINTS; ++i )
  {
    double t = _tMin + i * step;
    Guess& currentGuess = guesses[i];
    currentGuess.enabled = findInitialGuess( _context, _gradientDescriptor, t, currentGuess.x0, currentGuess.y0, currentGuess.angle, currentGuess.step );
    if( currentGuess.enabled )
    {
      Eigen::Vector2d v0 = _context.segHyp->lineFitter().pointAt( t );
      LS_DEBUG_COND( DEBUG_COND, t << " " << _context.segHyp->lineFitter().errorAt( t ) << " " << v0 << " " << currentGuess.x0 << " " << currentGuess.y0 );
      // Compute the cosinus
      currentGuess.cosAngle = cos( currentGuess.angle );
      // Fill the cosAngles array
      cosAngles[ countEnabled ] = currentGuess.cosAngle;
      // Compute the translation
      currentGuess.dx0 = currentGuess.x0 - v0(0);
      currentGuess.dy0 = currentGuess.y0 - v0(1);
      // Increment the number of initial guesses found
      ++countEnabled;
    }
  }
  LS_DEBUG_COND( DEBUG_COND, countEnabled );
  if( countEnabled == 0 ) return false;
  // Only one point, which means only one possiblity
  ONE_POINT_ONE_POSSIBILITY;
  // Compute the median of the cosinus of the angle
  double medianCosAngle = median( cosAngles, countEnabled );
  // Init arrays for computing the median of the dx0s and dy0s
  _angle = 0.0;
  double dx0s[GUESSPOINTS];
  double dy0s[GUESSPOINTS];;
  countEnabled = 0;
  // Look for the median of dx0s and dy0s
  for( int i = 0; i < GUESSPOINTS; ++i )
  {
    Guess& currentGuess = guesses[i];
    if( currentGuess.enabled )
    {
      LS_DEBUG_COND( DEBUG_COND, currentGuess.cosAngle << " " << medianCosAngle << " " << cos(currentGuess.cosAngle - medianCosAngle) );
      if( fabs(currentGuess.cosAngle - medianCosAngle) > 0.05 )
      { // Is to far from the angle median, drop the measure
        currentGuess.enabled = false;
      } else {
        dx0s[countEnabled] = currentGuess.dx0;
        dy0s[countEnabled] = currentGuess.dy0;
        ++countEnabled;
        _angle += currentGuess.cosAngle;
      }
    }
  }
  LS_DEBUG_COND( DEBUG_COND, countEnabled );
  if( countEnabled == 0 ) return false;
  // Only one point, which means only one possiblity
  ONE_POINT_ONE_POSSIBILITY
  
  // Compute angle
  _angle /= countEnabled;
  _angle = acos( _angle );
  
  // Compute dxs median
  double medianDX0 = median( dx0s, countEnabled );
  double medianDY0 = median( dy0s, countEnabled );
  
  // Compute x0 and y0
  countEnabled = 0;
  _x0 = 0;
  _y0 = 0;
  for( int i = 0; i < GUESSPOINTS; ++i )
  {
    Guess& currentGuess = guesses[i];
    LS_DEBUG_COND( DEBUG_COND, currentGuess.enabled << " " << currentGuess.dx0 << " - " << medianDX0 << " = | " << fabs( currentGuess.dx0 - medianDX0) << " | " << currentGuess.dy0 << " - " << medianDY0 << " = | " << fabs( currentGuess.dy0 - medianDY0 ) );
    if( currentGuess.enabled and fabs( currentGuess.dx0 - medianDX0) < 1.0 and fabs( currentGuess.dy0 - medianDY0 ) < 1.0 )
    {
      ++countEnabled;
      _x0 += currentGuess.dx0;
      _y0 += currentGuess.dy0;
    }
  }
  LS_DEBUG_COND( DEBUG_COND, countEnabled );
  if( countEnabled == 0 ) return false;
  Eigen::Vector2d v = _context.segHyp->lineFitter().pointAt( 0 );
  _x0 /= countEnabled;
  _x0 += v(0);
  _y0 /= countEnabled;
  _y0 += v(1);
//   JFR_DEBUG( segCount << " " << _context.segHyp->lineFitter().pointAt( 0 ) << " " << _context.segHyp->lineFitter().angle() );
//   JFR_DEBUG( _x0 << " " << _y0 << " " << _angle );
  return true;
}

bool DirectSegmentsTracker::findInitialGuess( GrowSegmentContext& _context, const GradientStatsDescriptor& _gradientDescriptor, double _t, double& _x0, double& _y0, double& _angle, double & _step ) const
{
  LS_DEBUG_COND(DEBUG_COND, segCount );
  Eigen::Vector2d v0 = _context.segHyp->lineFitter().pointAt( _t );
  int searchNbSteps_ = searchNbSteps();
  double sigmaPos = _context.segHyp->lineFitter().errorAt( _t );
  _step =  sigmaPos / searchNbSteps_;
  adjustStep( _step, searchNbSteps_);
  
  LS_DEBUG_COND(DEBUG_COND, sigmaPos );
  sigmaPos /= 3.0;
  sigmaPos *= 2.0 * sigmaPos;
  sigmaPos = 1.0 / sigmaPos;
  
  const Eigen::Vector2d& _normal = _context.segHyp->lineFitter().vecNormal();
  Eigen::Vector2d searchStep = _step * _normal;
  Eigen::Vector2d max;
  max(0) = _context.dx.cols - 1;
  max(1) = _context.dy.rows - 1;
  double hypAngle = _context.segHyp->lineFitter().angle();
  double meanGradients = _gradientDescriptor.meanGradients();
  double bestCoeff = 0.0;
  bool success = false;
  
  
  for( int i = -searchNbSteps_; i <= searchNbSteps_; ++i )
  {
    Eigen::Vector2d vi = v0 + i * searchStep;
    if( check2( vi, max, 0.5) )
    {
      double localAngle = utils::image::sampleAngle( _context.dx, _context.dy, vi(0), vi(1) );
      Eigen::Vector2d localGradOrientation;
      localGradOrientation(0) = cos(localAngle);
      localGradOrientation(1) = sin(localAngle);
      Eigen::Vector2d vim1 = vi - localGradOrientation;
      Eigen::Vector2d vip1 = vi + localGradOrientation;
      if( check2( vim1, max, 0.5) and check2( vip1, max, 0.5 ) )
      {
        double probaim1 = utils::image::getSubPixelValue<float>( _context.proba, vim1(0) - 0.5, vim1(1) - 0.5, 0, utils::image::INTERP_CUBIC );
        double probai = utils::image::getSubPixelValue<float>( _context.proba, vi(0) - 0.5, vi(1) - 0.5, 0, utils::image::INTERP_CUBIC );
        double probaip1 = utils::image::getSubPixelValue<float>( _context.proba, vip1(0) - 0.5, vip1(1) - 0.5, 0, utils::image::INTERP_CUBIC );
        LS_DEBUG_COND(DEBUG_COND, i << " vi = " << vi << " probaim1 = " << probaim1 << " probai = " << probai << " probaip1 = " << probaip1 );
        if( probai > probaim1 and probai > probaip1 )
        {
          double coeff = cos( localAngle - hypAngle ) * ratio( probai, meanGradients ) /* * exp( - i * i * sigmaPos ) */;
            LS_DEBUG_COND(DEBUG_COND, i << " coeff = " << coeff << " bestCoeff = " << bestCoeff << " localAngle = " << localAngle << " hypAngle = " << hypAngle << " probai = " << probai << " meanGradients = " << meanGradients << " exp( - i * i * sigmaPos ) =  " << exp( - i * i * sigmaPos ));
          if( coeff > bestCoeff )
          {
            bestCoeff = coeff;
            _x0 = vi(0);
            _y0 = vi(1);
            _angle = localAngle;
            LS_DEBUG_COND(DEBUG_COND, _x0 << " " << _y0 );
            success = true;
          }
        }
      }
    }
  }
  return success;
}
