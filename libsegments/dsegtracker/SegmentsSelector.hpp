/* $Id: SegmentsSelector.hpp 3955 2009-08-03 09:56:01Z cberger $ */

namespace libsegments {
  namespace dseg {
    class SegmentsSet;
  }
  namespace dsegtracker {
    /**
     * This class is used to select Segments for tracking. This step is done
     * to eliminate segments that are too similar from each other and would
     * cause confusion to the tracker.
     * @ingroup dseg
     */
    class SegmentsSelector {
      public:
        SegmentsSelector();
        ~SegmentsSelector();
      public:
        dseg::SegmentsSet select( dseg::SegmentsSet& _segmentSet, dseg::SegmentsSet* _keep = 0 );
    };
  }
}
