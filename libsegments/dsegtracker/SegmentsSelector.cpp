/* $Id: SegmentsSelector.cpp 3656 2009-02-26 16:19:38Z cberger $ */

#include "SegmentsSelector.hpp"
#include "../dseg/SegmentsSet.hpp"
#include "../dseg/SegmentHypothesis.hpp"
#include "../dseg/LineFitterKalman2.hpp"

#include <map>

using namespace libsegments::dseg;
using namespace libsegments::dsegtracker;
        
SegmentsSelector::SegmentsSelector()
{
  
}

SegmentsSelector::~SegmentsSelector()
{
  
}

SegmentsSet SegmentsSelector::select( SegmentsSet& _segmentSet, SegmentsSet* _keep )
{
  SegmentsSet result;
  std::map< double, SegmentHypothesis* > segments;
  for( unsigned int i = 0; i < _segmentSet.count(); ++i)
  {
    SegmentHypothesis* sh = _segmentSet.segmentAt( i );
    bool accept = true;
    if( not _keep or _keep->segmentId( sh->id() ) == 0 )
    {
      double length = 0.0;
      for( unsigned int j = 0; j < _segmentSet.count(); ++j)
      {
        if( i != j )
        {
          SegmentHypothesis* sh2 = _segmentSet.segmentAt( j );
          if( cos( sh->lineFitter().angle() - sh2->lineFitter().angle() ) > 0.95
              and sh->squareLength() < ( length += sh2->squareLength())
              and sh->lineFitter().distance( sh2->x1(), sh2->y1() ) < 5.0
              and sh->lineFitter().distance( sh2->x2(), sh2->y2() ) < 5.0 )
          {
            accept = false;
            break;
          }
        }
      }
    }
    if( accept )
    {
      result.addSegment( sh );
    }
  }
  return result;
}
