/* $Id: DirectSegmentsTracker.hpp 3955 2009-08-03 09:56:01Z cberger $ */

#ifndef _DIRECT_SEGMENTS_TRACKER_HPP_
#define _DIRECT_SEGMENTS_TRACKER_HPP_

#include "../dseg/DirectSegmentsBase.hpp"
#include "../utils/Macros.hpp"

namespace libsegments {
  namespace dseg {
    class GradientStatsDescriptor;
    struct GrowSegmentContext;
  }
  namespace dsegtracker {
    class Predictor;
    /**
     * @ingroup dseg
     * This class implement a tracker of a segment, that were first detected using
     * (\ref DirectSegmentsDector or \ref HierarchicalDirectSegmentsDector ).
     */
    class DirectSegmentsTracker : public dseg::DirectSegmentsBase {
      public:
        DirectSegmentsTracker();
        /**
         * @param _image the current image
         * @param _initialSegments the segment detected in the previous image
         * @param _predictor a predictor of the position of the segment from the previous image
         *                   to the current
         * @param _segmentStorage the segment that are successfully tracked are added to this storage
         */
        void trackSegment( const cv::Mat& _image, const dseg::SegmentsSet& _initialSegments, const Predictor* _predictor, dseg::SegmentsSet& _segmentStorage );
      public:
        /**
         * Define wether the tracker should adjust the prediction, or should use it directly
         */
        LS_DEFINE_PARAM_RW( bool, adjustPrediction, setAdjustPrediction);
      private:
        bool findInitialGuesses( dseg::GrowSegmentContext& _gsc, const dseg::GradientStatsDescriptor& _gradientDescriptor, double _tMin, double _tMax, double& _x0, double& _y0, double& _angle ) const;
        bool findInitialGuess( dseg::GrowSegmentContext& _gsc, const dseg::GradientStatsDescriptor& _gradientDescriptor, double _t, double& _x0, double& _y0, double& _angle, double & _step ) const;
    };
  }
}

#endif
