/* $Id: StaticPredictor.hpp 3276 2008-10-24 10:55:58Z cberger $ */

#include "Predictor.hpp"

namespace libsegments {
  namespace dsegtracker {
    /**
     * @ingroup dseg
     * It's a \ref Predictor that "grow" the covariance of the predictor from a given amount.
     */
    class StaticPredictor : public Predictor {
      public:
        StaticPredictor( double _posError, double _angleError );
        virtual dseg::SegmentHypothesis* predictionFor( const dseg::SegmentHypothesis* ) const;
      private:
        double m_posError, m_angleError;
    };
  }
}
