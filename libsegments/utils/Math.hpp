#include <Eigen/Core>

namespace libsegments {
  namespace utils {
    namespace math {
      inline Eigen::Vector2d eigenValues( const Eigen::Matrix2d& m)
      {
        double tr = m(0,0) + m(1,1);
        double diff = m(0,0) - m(1,1);
        double sq = sqrt( 4 * m(0,1) * m(1,0) + diff * diff );
        Eigen::Vector2d v;
        v(0) = 0.5 * ( tr + sq );
        v(1) = 0.5 * ( tr - sq );
        return v;
      }
    }
  }
}
