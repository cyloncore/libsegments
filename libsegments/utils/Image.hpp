#include "Macros_p.hpp"

#include <opencv2/opencv.hpp>

namespace libsegments {
  namespace utils {
    namespace image {

      const int cubicKernelResolution = 255;
      extern const double cubicKernel1[256];
      extern const double cubicKernel2[256];

      enum InterpolationType {
          INTERP_NEAREST,
          INTERP_BILINEAR,
          INTERP_CUBIC
      };

      inline double cubicKernelI( int _t, int _idx )
      {
        switch( _t )
        {
          case -1:
            return cubicKernel2[_idx];
          case 0:
            return cubicKernel1[_idx];
          case 1:
            return cubicKernel1[cubicKernelResolution - _idx];
          case 2:
            return cubicKernel2[cubicKernelResolution - _idx];
        }
        return 0;
      }
      inline double cubicKernel( int _t, double _dt )
      {
        int idx =int (_dt * cubicKernelResolution );
        return cubicKernelI( _t, idx );
      }

      template<typename channeltype>
      channeltype getPixelValue( const cv::Mat& mat, int x_, int y_, int channel_ = 0) {
        LS_ASSERT(x_ >= 0 && y_ >= 0 && x_ < mat.cols && y_ < mat.rows, "getPixelValue pixel coordinates are not in the image.");
        LS_ASSERT( 0 <= channel_ && channel_ < mat.channels(), "v wrong channel parameter ");
        channeltype const *pxPtr;
        pxPtr = reinterpret_cast<channeltype const *>(mat.ptr(y_));
        return *(pxPtr + x_*mat.channels() + channel_);
      }


      template<typename channeltype>
      double getSubPixelValue(const cv::Mat& mat, double x_, double y_, int channel_, InterpolationType _interpolation = INTERP_BILINEAR ) {
        int height_ = mat.rows;
        int width_ = mat.cols;
        LS_ASSERT(x_ >= 0. && y_ >= 0. && x_ <= (double)width_ - 1 && y_ <= (double)height_ - 1, 
                    "JfrImage::pixelValue pixel coordinates are not in the image.");
        LS_ASSERT( 0 <= channel_ && channel_ < mat.channels(), "JfrImage::getSubpixelValue wrong channel parameter ");
        if( x_ == mat.cols - 1) x_ -= 1e-6;
        if( y_ == mat.rows - 1) y_ -= 1e-6;

        switch( _interpolation )
        {
          case INTERP_NEAREST:
          {
            return getPixelValue<channeltype>( mat, (int)nearbyint( x_ ), (int)nearbyint( y_ ), channel_ );
          }
          case INTERP_BILINEAR:
          {
            int xi = (int)floor(x_);
            int yi = (int)floor(y_);
        
            double dx = x_-floor(x_);
            double dy = y_-floor(y_);
            double c11 = (1-dx)*(1-dy);
            double c12 = (1-dy)*dx;
            double c21 = (1-dx)*dy;
            double c22 = dx*dy;
        
            return (
                c11 * getPixelValue<channeltype>(mat, xi,yi, channel_)
                + c12 * getPixelValue<channeltype>(mat, xi+1,yi, channel_)
                + c21 * getPixelValue<channeltype>(mat, xi,yi+1, channel_)
                + c22 * getPixelValue<channeltype>(mat, xi+1,yi+1, channel_));
          }
          case INTERP_CUBIC:
          {
            double result = 0.0;
            int xi = int(x_);
            int yi = int(y_);
            double dx = x_ - xi;
            int idxX =int( dx * cubicKernelResolution );
            double dy = y_ - yi;
            int idxY =int( dy * cubicKernelResolution );
            int channelsCount = mat.channels();
            int offset = (xi - 1) * channelsCount + channel_;
            for(int v = -1; v < 3; ++v)
            {
              int yt = yi + v;
              if( yt >= 0 and yt < height_ )
              {
                channeltype const *pxPtr = reinterpret_cast<channeltype const *>(mat.ptr(yt));
                pxPtr += offset;
                
                double ycoef = cubicKernelI( v, idxY);
                for(int u = -1; u < 3; ++u, pxPtr += channelsCount)
                {
                  int xt = xi + u;
                  if( xt >= 0 and xt < width_ ) // TODO use a better value than 0 when out of range
                  {
                    result += ycoef * cubicKernelI( u, idxX ) * *pxPtr;
                  }
                }
              }
            }
            return result;
          }
        }
        return -1;
      }

      template<typename channeltype>
      void setPixelValue( cv::Mat& mat, channeltype val_, int x_, int y_, int channel_) {
        LS_ASSERT(x_ >= 0 && y_ >= 0 && x_ < mat.cols && y_ < mat.rows, 
            "JfrImage::pixelValue pixel coordinates are not in the image.");
        LS_ASSERT( 0 <= channel_ && channel_ < mat.channels(), "JfrImage::pixelValue "
            "wrong channel parameter ");
        channeltype *pxPtr = reinterpret_cast<channeltype*>(mat.ptr(y_));
        *(pxPtr+ x_ * mat.channels() + channel_) = val_;
      }

      inline double sampleAngle( const cv::Mat& _dx, const cv::Mat& _dy, double x_, double y_ )
      {
        double dx = getSubPixelValue<short>( _dx, x_ - 0.5, y_ - 0.5, 0, INTERP_CUBIC);
        double dy = getSubPixelValue<short>( _dy, x_ - 0.5, y_ - 0.5, 0, INTERP_CUBIC);
        return atan2(dy, dx);
      }
      inline bool compHeader(const cv::Mat& _img1, const cv::Mat& _img2) {
        return (_img1.cols == _img2.cols && 
          _img1.rows == _img2.rows &&
          _img1.channels() == _img2.channels() &&
          _img1.depth() == _img2.depth());
      }
      void copy( const cv::Mat& _src, cv::Mat& _dst, int _x_src = 0, int _y_src = 0, int _x_dst = 0, int _y_dst = 0, int _width = -1, int _height = -1 );
    }
  }
}
