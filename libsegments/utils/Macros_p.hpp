/** \file Macros_p.hpp Defines usefull macros. All the debug macro are
 * disabled when -DLS_NDEBUG is passed to the compiler.
 *
 * \ingroup utils
 */

#ifndef LIBSEGMENTS_MACRO_P_H_
#define LIBSEGMENTS_MACRO_P_H_

#include <cstdlib>
#include <string>
#include <iostream>
#include <sstream>

namespace libsegments {
  namespace utils {
    namespace details {

      struct ForeachInfoBase {};
  
      template<typename T>
      struct ForeachInfo : public ForeachInfoBase {
        inline ForeachInfo( const T& t ) : it( t.begin() ), end( t.end() ), count(0)
        {
        }
        inline bool finished() const
        {
          return it == end;
        }
        inline void next() const
        {
          count = 0;
          ++it;
        }
        mutable typename T::const_iterator it, end;
        mutable int count;
      };
      template <typename T>
      inline ForeachInfo<T> CreateForeachInfo(const T& t)
      {
        return ForeachInfo<T>( t );
      }
  
      template <typename T>
      inline const ForeachInfo<T> *getForeachInfo(const ForeachInfoBase *base, const T *)
      {
        return static_cast<const ForeachInfo<T> *>(base); 
      }
      template <typename T>
      inline T* getForeachType(const T &) { return 0; }
    }
  }
}

/**
 * This macro allow to easily loop over all the elements of a vector, without writing the annoying iterators.
 * @code
 * std::vector\< int \> vec;
 * LS_FOREACH( int var, vec )
 * {
 *  LS_DEBUG( var );
 * }
 * std::vector\< CoolObject \> coolObjects;
 * LS_FOREACH( CoolObject& coolObject, coolObjects )
 * {
 *  coolObject.soSomethingCool();
 * }
 * @endcode
 */
#define LS_FOREACH(var, cont ) \
  for( const libsegments::utils::details::ForeachInfoBase&  contInfo = libsegments::utils::details::CreateForeachInfo( cont ); \
       not libsegments::utils::details::getForeachInfo( &contInfo, true ? 0 : libsegments::utils::details::getForeachType(cont) )->finished(); \
       libsegments::utils::details::getForeachInfo( &contInfo, true ? 0 : libsegments::utils::details::getForeachType(cont) )->next() ) \
    for( var = *libsegments::utils::details::getForeachInfo( &contInfo, true ? 0 : libsegments::utils::details::getForeachType(cont) )->it; \
         libsegments::utils::details::getForeachInfo( &contInfo, true ? 0 : libsegments::utils::details::getForeachType(cont) )->count < 1; \
         ++libsegments::utils::details::getForeachInfo( &contInfo, true ? 0 : libsegments::utils::details::getForeachType(cont) )->count )

/**
 * Display a variable name in a flow and its value.
 * 
 * For instance:
 * @code
 * int a = 1;
 * LS_DEBUG( LS_PP_VAR( a ) );
 * @endcode
 * will display "a = 1 ".
 */
#define LS_PP_VAR( var ) #var << " = " << var << " "


/** Throw \a ExceptionName with ID \a id along with \a message. The
 * constructor of the class \a ExceptionName must have signature
 * compatible with ExceptionName(id, message, __FILE__,
 * __LINE__). This is the case for the exception class which is
 * generated with a module, but may not be the case for user defined
 * exceptions.
 */
#define LS_ERROR(message) \
  {\
    std::ostringstream s; \
    std::cerr << __FILE__ << ":" << __LINE__ << " => " << message << std::endl; \
    abort();	\
  }

/** When LS_NDEBUG is defined, contract programming checks, debug
 * messages and trace information are disabled.
 */
#ifndef LS_NDEBUG

/** If \a predicate is \c FALSE abort and display \a message.
 */
#  define LS_ASSERT(predicate, message)				\
  if (!(predicate)) {							\
    LS_ERROR(message << " (" << #predicate << ")") \
  }

/** Send \a message to the debug stream with level
 * DebugStream::Warning. \c operator<< can be used to format the
 * message.
 *
 * \code
 *  if (d < EPSILON)
 *    WARNING("Small value, check numerical stability - d=" << d); 
 *
 * output:
 *  W:pipo/test.cpp:55: Small value, check numerical stability - d= 1e-12 
 * \endcode
 */
#define LS_WARNING(message)                                                                                  \
    {                                                                                                     \
      std::cerr << "Warning, " << __FILE__ << ":" << __LINE__ << " => " << message << std::endl; \
    }


/** Tell the debug stream to start a new line of debug
 * \code
 *  DEBUG_BEGIN()
 *  DEBUG_SEND("The values of i are "
 *  for(int i = 0; i < 3; i ++)
 *    DEBUG_SEND( i << " "); 
 *  DEBUG_END()
 * 
 * output:
 *  D:pipo/test.cpp:55: The value of i are 0 1 2
 * \endcode
 */
#define LS_DEBUG_BEGIN()                                                 \
{                                                                     \
    std::cerr << "Debug, " << __FILE__ << ":" << __LINE__ << " => ";  \
}

/**
 * Use after a call to LS_DEBUG_BEGIN() to send \a message to
 * the debug stream
 */
#define LS_DEBUG_SEND(message)   \
{                             \
    std::cerr << message;     \
}

/**
 * Use when you want to close a line of debug
 */
#define LS_DEBUG_END()                   \
{                                     \
    std::cerr << std::endl;  \
}


/** Send \a message to the debug stream with level
 * DebugStream::Debug. \c operator<< can be used to format the
 * message.
 *
 * \code
 *  LS_DEBUG("The value of i is " << i); 
 *
 * output:
 *  D:pipo/test.cpp:55: The value of i is 2
 * \endcode
 */
#define LS_DEBUG(message)            \
  {                                  \
    LS_DEBUG_BEGIN()                 \
    LS_DEBUG_SEND(message)           \
    LS_DEBUG_END()                   \
  }

/**
 * Send \a message to the debug stream with level
 * DebugStream::Debug, if the condition \a test
 * is true. \c operator<< can be used to format the
 * message.
 */
#define LS_DEBUG_COND(test, message)   \
  if( (test) ) LS_DEBUG( message);

#else // NDEBUG
#  define LS_ASSERT(predicate, message) ((void)0)

#  ifndef LS_QUIET
#    define LS_WARNING(message)                                                                              \
  {                                                                                                       \
      std::cerr << "Warning, " << __FILE__ << ":" << __LINE__ << " => " << message << std::endl; \
  }
#  else // LS_QUIET
#    define LS_WARNING(message) ((void)0)
#  endif // LS_QUIET

#  define LS_DEBUG_BEGIN()  ((void)0)
#  define LS_DEBUG_SEND(message)  ((void)0)
#  define LS_DEBUG_END()  ((void)0)

#  define LS_DEBUG(message) ((void)0)
#  define LS_DEBUG_COND(test, message) ((void)0)
#endif // LS_NDEBUG

#endif // LIBSEGMENTS_MACRO_P_H_
