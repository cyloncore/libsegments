/* $Id: $ */

#ifndef _PYRAMID_HPP_
#define _PYRAMID_HPP_

#include <vector>

#include "Macros_p.hpp"
#include "Image.hpp"

namespace libsegments {
  namespace utils {
    namespace image {
      
      /*!
      * base class for pyramidal scalespace representations.
      *
      * 
      * 
      * \ingroup image
      */
      class Pyramid {
      public:
        typedef std::vector<cv::Mat>::iterator iterator;
        typedef std::vector<cv::Mat>::const_iterator const_iterator;
        iterator begin() { return p_pyr.begin(); };
        const_iterator begin() const { return p_pyr.begin(); };
        iterator end() { return p_pyr.end(); };
        const_iterator end() const { return p_pyr.end(); };

        /// Default constructor
        Pyramid(int levels=0) : p_pyr(levels) {}; 

        /// build constructor
        Pyramid(cv::Mat const & img, int levels) {
    build(img,levels);
        }

        /// Copy constructor
        Pyramid(Pyramid const & pyr) : p_pyr(pyr.size()){
    p_pyr = pyr.p_pyr;
        };

        /// Default destructor
        virtual ~Pyramid() {};

        /*!
        * clone pyramid
        */
        Pyramid clone() const {
    Pyramid pyr(size());
    for(unsigned int i=0; i<size(); i++)
      pyr[i] = p_pyr[i].clone();
    return pyr;
        }
          
        /*!
        * return size of the Pyramid
        */
        size_t size() const {
    return p_pyr.size();
        }

        /*!
        * @return true if all images in pyramid are intialized
        */
        bool check() const {
    if ( size() == 0 ) {
      LS_DEBUG("Empty pyramid");
      return false;
    }
    for(size_t i=0; i<size(); i++) {
      LS_DEBUG("level " << i << ":" << p_pyr[i]);
      if (p_pyr[i].empty()) 
        return false;
    }
    return true;
        }

        /*!
        * image accessor
        */
        cv::Mat & operator[] (int l) {
    return p_pyr[l];
        };

        /*!
        * const image accessor
        */
        cv::Mat const & operator[] (int l) const {
    return p_pyr[l];
        };

        /*!
        * get pixel value for given level.
        * @optional ref_l reference level of (x,y) [default: 0]
        */
        template<typename type>
        type getPixelValue(int x, int y, int l, int ref_l=0,int channel=0) const;

        /*!
        * get subpixel value for given level
        * @optional ref_l reference level of (x,y) [default: 0]
        */
        template<typename type>
        double getSubPixelValue(double x, double y, int l,int ref_l=0,int channel=0) const;

        /*!
        * Pyramid assignation
        */
        Pyramid& operator= (Pyramid const & pyr) {
    p_pyr = pyr.p_pyr;
    return *this;
        };

        /*!
        *  check if given image and p_pyr[0] have the same characteristics
        */
        bool operator==(cv::Mat const & img) const {
    return ( !p_pyr[0].empty() ? ( compHeader(p_pyr[0], img) ) : false);
        }
        
        /*!
        * check if given pyramid and p_pyr have the same characteristics
        */
        bool operator==(Pyramid const & pyr) const {
    return ( size() == 0 ? ( !p_pyr[0].empty() ? ( compHeader(p_pyr[0], pyr[0]) && 
                    p_pyr.size() == pyr.size() ) : false ) : false);
        }
        
        /*!
        * build the pyramid. Here, it uses a gaussian 5x5 kernel filter
        */
        virtual void build(cv::Mat const & img, int l=0);
        
        
      protected:
        std::vector<cv::Mat> p_pyr;
      }; /* class Pyramid */

      

      /*************************************************************************/
      /*************************************************************************/
      
      template<typename type>
      type Pyramid::getPixelValue(int x, int y, int l, int ref_l, int channel) const {
        LS_ASSERT( p_pyr.size() > 0, "Pyramid::getPixelValue: empty pyramid");
        LS_ASSERT(l < p_pyr.size(), "Pyramid::getPixelValue: level parameter out of range");
        LS_ASSERT( ( ref_l >= 0 && ref_l <= l ), "Pyramid::getPixelValue: incompatible level and reference level");
        if ( ref_l == l ) 
    return getPixelValue<type>(p_pyr[l], x,y,channel);
        int ii = x >> (l-ref_l), jj = y >> (l-ref_l);
        int ww = p_pyr[l].cols - 1, hh = p_pyr[l].rows - 1;
        if (ii > ww) ii = ww; if (jj > hh) jj = hh;
        return getPixelValue<type>(p_pyr[l], ii, jj, channel);
      }
      
      template<typename type>
      double Pyramid::getSubPixelValue(double x, double y, int l, int ref_l, int channel) const {
        LS_ASSERT( p_pyr.size() > 0, "Pyramid::getPixelValue: empty pyramid");
        LS_ASSERT(l < p_pyr.size(), "Pyramid::getPixelValue: level parameter out of range");
        LS_ASSERT( ( ref_l >= 0 && ref_l <= l ), "Pyramid::getSubPixelValue: incompatible level and reference level");
        if ( ref_l == l )
    return getSubPixelValue<type>(p_pyr[l], x,y,channel);
        double fac = double(1 << (l-ref_l));
        double ii = x / fac, jj = y / fac;
        int ww = p_pyr[l].cols - 1, hh = p_pyr[l].rows - 1;
        if (ii > ww) ii = ww; if (jj > hh) jj = hh;
        return getSubPixelValue<type>(p_pyr[l], ii, jj, channel);
      }
      
      
      /*!
      *  Gaussian Pyramid class
      */
      class GaussianPyramid : public Pyramid {
      public:
        GaussianPyramid (int l=0, double s=1.2, double s0=1.0) : Pyramid(l), sigma(s), sigma0(s0) {};
        virtual ~GaussianPyramid () {};

        virtual void build(cv::Mat const & img, int l=0);

      private:
        double sigma;
        double sigma0;
      };
      
      /**
      * Linear Pyramid class
      */
      class LinearPyramid : public Pyramid {
      public:
        LinearPyramid( int _levels) : Pyramid(_levels )
        {
        }
        virtual ~LinearPyramid() {}
        virtual void build( cv::Mat const & img, int l=0);
      };
    
    } /* namespace image */
  } /* namespace utils */
} /* namespace libsgments */


#endif /* _PYRAMID_HPP_ */
