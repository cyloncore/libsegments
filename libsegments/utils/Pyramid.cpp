/* $Id$ */

#include "Pyramid.hpp"

#include <opencv2/imgproc.hpp>

using namespace std;
using namespace libsegments::utils::image;

void Pyramid::build(cv::Mat const & img, int l) {
  if ( p_pyr.size() == 0 ) {
    LS_ASSERT( l!=0, "Pyramid::build: pyramid of size 0. you must provide a number of levels" );
    p_pyr.resize(l);
  }
  if ( p_pyr[0].empty()) {
    // creation of the pyramid
    p_pyr[0] = img.clone();
    for(std::size_t i = 1; i < p_pyr.size(); i++) {
      p_pyr[i].create(p_pyr[i-1].rows/2, p_pyr[i-1].cols/2, p_pyr[i-1].type());
      cv::pyrDown(p_pyr[i-1], p_pyr[i]);
    }
  } else if ( !compHeader(p_pyr[0], img) || l != 0 ) {
    // recompute the pyramid
    int levels = p_pyr.size();
    if ( l!=0 ) 
      levels = l;
    p_pyr.clear();
    p_pyr.resize(levels);
    p_pyr[0] = img.clone();
    for(size_t i = 1; i < p_pyr.size(); i++) {
      p_pyr[i].create(p_pyr[i-1].rows/2, p_pyr[i-1].cols/2, p_pyr[i-1].type());
      cv::pyrDown(p_pyr[i-1], p_pyr[i]);
    }
  } else {
    // update 
    copy(img, p_pyr[0]);
    for(size_t i = 1; i < p_pyr.size(); i++) {
      cv::pyrDown(p_pyr[i-1], p_pyr[i]);
    }
  }
}


void GaussianPyramid::build(cv::Mat const & img, int l) {
  if ( p_pyr.size() == 0 ) {
    LS_ASSERT( l!=0, "Pyramid::build: pyramid of size 0. you must provide a number of levels" );
    p_pyr.resize(l);
  }

  if ( p_pyr[0].empty()) {
    // creation of the pyramid
    p_pyr[0] = img.clone();
    for(size_t i = 1; i < p_pyr.size(); i++) {
      p_pyr[i] = p_pyr[i-1].clone();
      cv::GaussianBlur(p_pyr[i],p_pyr[i], cv::Size(0, 0), sigma*sigma0, sigma*sigma0);
      cv::resize(p_pyr[i], p_pyr[i], cv::Size(p_pyr[i].cols/2, p_pyr[i].rows/2), 0, 0, cv::INTER_LINEAR);
    }
  } else if ( !compHeader(p_pyr[0], img) || l != 0 ) {
    // recompute the pyramid
    int levels = p_pyr.size();
    if ( l!=0 ) 
      levels = l;
    p_pyr.clear();
    p_pyr.resize(levels);
    p_pyr[0] = img.clone();
    for(size_t i=1; i<p_pyr.size(); i++) {
      p_pyr[i] = p_pyr[i-1].clone();
      cv::GaussianBlur(p_pyr[i],p_pyr[i], cv::Size(0, 0), sigma*sigma0, sigma*sigma0);
      cv::resize(p_pyr[i], p_pyr[i], cv::Size(p_pyr[i].cols/2, p_pyr[i].rows/2), 0, 0, cv::INTER_LINEAR);
    }
  } else {
    // update 
    copy(img, p_pyr[0]);
    for(size_t i=1; i<p_pyr.size(); i++) {
      cv::Mat tmp = p_pyr[i-1].clone();
      cv::GaussianBlur(tmp, tmp, cv::Size(0, 0), sigma*sigma0, sigma*sigma0);
      cv::resize(tmp, p_pyr[i], p_pyr[i].size(), 0, 0, cv::INTER_LINEAR);
    }
  }
}

void LinearPyramid::build( cv::Mat const & img, int l)
{
  if ( p_pyr.size() == 0 or p_pyr.size() != (size_t)l ) {
    LS_ASSERT( l!=0, "Pyramid::build: pyramid of size 0. you must provide a number of levels" );
    p_pyr.resize(l);
  }
  p_pyr[0] = img.clone();
  for(size_t i=1; i<p_pyr.size(); i++) {
    p_pyr[i].create(p_pyr[i-1].rows / 2, p_pyr[i-1].cols / 2, p_pyr[i-1].type());
    cv::resize(p_pyr[i-1], p_pyr[i], p_pyr[i].size(), 0, 0, cv::INTER_LINEAR);
  }
  
}
