#include "IdFactory.hpp"

using namespace libsegments::utils;

IdFactory::IdFactory() : m_lastId(1)
{
}

IdFactory::~IdFactory()
{
}

unsigned int IdFactory::getId()
{
  return ++m_lastId;
}

void IdFactory::releaseId(unsigned int id)
{
  if(id == m_lastId )
  {
    --m_lastId;
  }
}
