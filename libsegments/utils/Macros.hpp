/** \file Macros.hpp Defines usefull macros, that can be used in public
 * headers.
 *
 * \ingroup utils
 */

#ifndef LIBSEGMENTS_MACROS_HPP_
#define LIBSEGMENTS_MACROS_HPP_

/**
 * This macro defines the member used to store the value of a parameter.
 * 
 * There is no reason to use that macro directly, you will want to use
 * LS_DEFINE_PARAM_RW, LS_DEFINE_PARAM_RO or LS_DEFINE_PARAM_WO
 * 
 * @param type the type of the parameter
 * @param name the name of the parameter (the name of the variable is
 *             prefixed with m_)
 */
#define LS_DEFINE_PARAM_VAR(type, name) \
  private: \
      type m_##name;

/**
 * This macro defines the function used to return the value of a parameter.
 * There is no reason to use that macro directly, you will want to use
 * LS_DEFINE_PARAM_RW, LS_DEFINE_PARAM_RO or LS_DEFINE_PARAM_WO
 * @param type the type of the parameter
 * @param name the name of the parameter
 */
#define LS_DEFINE_PARAM_GETTER(type, name) \
  public: \
     inline type name() const { return m_##name; }

/**
 * This macro defines the function used to set the value of a parameter.
 * There is no reason to use that macro directly, you will want to use
 * LS_DEFINE_PARAM_RW, LS_DEFINE_PARAM_RO or LS_DEFINE_PARAM_WO
 * @param type the type of the parameter
 * @param name the name of the parameter
 * @param settername the name of the setter (if the parameter is called
 * myParameter, the setter is ususally called setMyParamter)
 */
#define LS_DEFINE_PARAM_SETTER(type, name, settername ) \
  public: \
     inline void settername(type v) { m_##name = v; }

/**
 * This macro defines a parameter that can be read and write.
 * 
 * Exemple of use:
 * @code
 *  class MyClass {
 *    LS_DEFINE_PARAM_RW( double, myParam, setMyParam);
 *  };
 *  MyClass instance;
 *  instance.setMyParam(10.0);
 *  LS_DEBUG(instance.myParam())
 * @endcode
 * 
 * @param type the type of the parameter
 * @param name the name of the parameter
 * @param settername the name of the setter (if the parameter is called
 * myParameter, the setter is ususally called setMyParamter)
 */
#define LS_DEFINE_PARAM_RW( type, name, settername ) \
  LS_DEFINE_PARAM_GETTER(type, name) \
  LS_DEFINE_PARAM_SETTER(type, name, settername ) \
  LS_DEFINE_PARAM_VAR(type,name)

/**
 * This macro defines a parameter that can be read, but not set.
 * 
 * Exemple of use:
 * @code
 *  class MyClass {
 *    public:
 *      MyClass( ) : m_myParam(10.0) { }
 *    LS_DEFINE_PARAM_RO( double, myParam)
 *  };
 *  MyClass instance;
 *  LS_DEBUG(instance.myParam())
 * @endcode
 * 
 * Or if you want a personal setter
 * @code
 *  class MyClass {
 *    LS_DEFINE_PARAM_RO( double, myParam)
 *    public:
 *      MyClass( ) : m_myParam(0.0) { }
 *    public:
 *      void setMyParam(double p)
 *      {
 *        if(p < 5.0) m_myParam = p;
 *      }
 *  };
 *  MyClass instance;
 *  instance.setMyParam(10.0);
 *  LS_DEBUG(instance.myParam()); // will return 0.0
 * @endcode
 * 
 * @param type the type of the parameter
 * @param name the name of the parameter
 */
#define LS_DEFINE_PARAM_RO( type, name) \
  LS_DEFINE_PARAM_GETTER(type, name) \
  LS_DEFINE_PARAM_VAR(type,name)

/**
 * This macro defines a parameter that can only be written.
 * 
 * Exemple of use:
 * @code
 *  class MyClass {
 *    LS_DEFINE_PARAM_WO( double, myParam, setMyParam)
 *  };
 *  MyClass instance;
 *  instance.setMyParam(10.0);
 * @endcode
 * 
 * @param type the type of the parameter
 * @param name the name of the parameter
 * @param settername the name of the setter (if the parameter is called
 * myParameter, the setter is ususally called setMyParamter)
 */
#define LS_PARAM_WO( type, name, settername ) \
  LS_DEFINE_PARAM_SETTER(type, name, settername ) \
  LS_DEFINE_PARAM_VAR(type,name)

/**
 * The LS_DEPRECATED macro can be used to trigger compile-time warnings when a deprecated
 * functions are used.
 * 
 * For non-inline functions, the macro has to be inserted at the end of the declaration like in :
 * @code
 * DeprecatedConstructor() LS_DEPRECATED;
 * void deprecatedFunction() const LS_DEPRECATED;
 * @endcode
 * 
 * For inline functions, the macro has to be inserted before the declartion but after virtual or
 * static keywoard, like in :
 * @code
 * LS_DEPRECATED void deprecatedInline() { ... }
 * virtual LS_DEPRECATED int depreactedInline() { ... }
 * static LS_DEPRECATED char* deprecatedInline() { ... }
 * @endcode
 * 
 * You can declare a class or struct to be deprecated :
 * @code
 * class LS_DEPRECATED deprecatedClass { };
 * struct LS_DEPRECATED deprecatedStruct { };
 * @endcode
 */
#ifndef LS_DEPRECATED
#if __GNUC__ - 0 > 3 || (__GNUC__ - 0 == 3 && __GNUC_MINOR__ - 0 >= 2)
  /* gcc >= 3.2 */
# define LS_DEPRECATED __attribute__ ((deprecated))
#elif defined(_MSC_VER) && (_MSC_VER >= 1300)
  /* msvc >= 7 */
# define LS_DEPRECATED __declspec(deprecated)
#else
# define LS_DEPRECATED
#endif
#endif

#endif // LIBSEGMENTS_MACROS_HPP_
