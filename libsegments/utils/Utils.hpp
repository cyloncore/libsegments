#ifndef _UTILS_H_
#define _UTILS_H_

#include <Eigen/Core>

inline double corrAngle( double a1, double a2)
{
  return cos(a1 - a2);
}

template< typename _T1_, typename _T2_>
inline bool check(_T1_ v, _T2_ max)
{
  return v >= 0 and v <= max;
}

inline bool check2( const Eigen::Vector2d& v, const Eigen::Vector2d& max, double offset )
{
  return check(v(0) - offset, max(0)) and check( v(1) - offset, max(1));
}

inline double computeDirection( double angle )
{
  if( angle > 0.5 * M_PI ) angle -= M_PI;
  else if( angle < -0.5 * M_PI ) angle += M_PI;
  return angle;
}

inline double sign( double a )
{
  if( a < 0.0 ) return -1.0;
  return 1.0;
}

inline double pow2( double a )
{
  return a * a;
}

inline void projection( double &x_r, double &y_r, double x_A, double y_A, double a, double b )
{
  double aa = a * a;
  double bb = b * b;
  double ab = a * b;
  double num = 1.0 / (aa + bb);
  x_r = (x_A * bb - y_A * ab - a) * num;
  y_r = -(x_A * ab - y_A * aa + b) * num;
}

inline double max(double v1, double v2)
{
  return v1 > v2 ? v1 : v2;
}

inline double ratio(double v1, double v2)
{
  if( v1 > v2 )
  {
    return v2 / v1;
  }
  return v1 / v2;
}

#endif
