#ifndef LS_EXTRACTOR
#define LS_EXTRACTOR

#include "../SegmentsDetector.hpp"

namespace libsegments {
  class Segment;
  namespace chaining {
    
    //! Class with functions to extract line segments of an image
    /**
    This class provides a set of functions to extract line segments of an image. The idea is to extract all line segments contained in an image (global extraction). To re-extract a line from a prediction of that line (local extraction) have a look at LineSegment::fitLineOrientation() function and its Usage in the LsTracker class.
    
    
    To simply extract the set of lines of an image, the extractLineSegments() function can be used. It combines the extraction steps with the currently best performance.
    
    
    The functions of LsExtractor class works all with the same scheme: calculate gradient image or Canny image and find contour points by maxima suppression with hysteresis and find lines either with polygonalisation  There is a combination of the two OpenCV function for that task, but the contour representation that result by OpenCv cvFindContours runs a lot of contour points for multiple times if there are branched contours, thus the contours are very long. Therefore the well tested Calife functions for that task can be used alternatively. Some documentation of the Calife functions is contained in ctdef.hpp file. 
    The OpenCv polygonalization algorithm seems to be a little bit buggy (CVS version avaiable at January 2008), therefore a simple implementation of Douglas-Peucker Algorithm is provided.
    
    Further there is a function to apply a Hough-transformation to find lines.
    */
    class ChainingSegmentsDetector : public SegmentsDetector{
      public:
        ChainingSegmentsDetector();
        
        //! Extraction of line segments with memeber variables as parameters
        /**
         * Extracts lines from image and appends result to _segmentStorage. 
         * If only the resulting lines are desired to be contained in _segmentStorage, then _segmentStorage should be cleared before calling this function (LineSegmentSet::clear()). The _image is used as container for the Canny image, therefore it is changed within this function. As parameters for Canny the member variables are taken. 
         *
         * @param image Greyvalue image whose lines are extracted, image content is changed
         * @param cannyIm Storage for the resulting image of Canny edge detection
         * @param segmentStorage Storage for resulting line segments
         *
         */                
        std::vector< Segment > detectSegments( const cv::Mat& _image );
    
        void setCannyLowerThresh(int lowerThresh){ cannyLowerThresh = lowerThresh;}
        void setCannyHigherThresh(int higherThresh){ cannyHigherThresh = higherThresh;}
        void setCannyAp(int ap){ if(ap>0) cannyAp = ap; }
        /**
         * @param thresh Minimum length of accepted lines.
         */
        void setThreshold(double thres) { threshold = thres; }
      private:
        /** 
         * Input image should either be canny or gradient image, attention: an image with height
         * or width which is not a multiple of 4 slows down the function (because the whole image data must be copied)
         * The content of the image is changed. All pixels, that are contained in a taken contour are set to 127. 
         */
        static void findLinesCalife(const cv::Mat* image, std::vector< Segment >& newSegments, double thresh = 15 );
      protected:
        // parameters for Canny
        int cannyLowerThresh;   //!< Parameter for cvCanny
        int cannyHigherThresh;  //!< Parameter for cvCanny
        int cannyAp;            //!< Parameter for cvCanny
        double threshold;
    };
    
  } // namespace lines
} // namespace jafar

#endif
