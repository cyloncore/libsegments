#include "ChainingSegmentsDetector.hpp"

#include <opencv2/opencv.hpp>

#include "ctdef.hpp"
#include <Segment.hpp>

using namespace libsegments;
using namespace libsegments::chaining;

ChainingSegmentsDetector::ChainingSegmentsDetector(){
  // initialise parameters for Canny
  cannyLowerThresh = 130;
  cannyHigherThresh = 220;
  cannyAp = 3;
  threshold = 0.0;
}


std::vector< Segment > ChainingSegmentsDetector::detectSegments( const cv::Mat& _image )
{
  std::vector< Segment > segmentStorage;
  cv::Mat cannyIm(_image.size(), CV_MAKETYPE(CV_8U, 1));

  cv::Canny( _image, cannyIm, cannyLowerThresh, cannyHigherThresh, cannyAp);

  findLinesCalife(&cannyIm,segmentStorage, threshold);
  return segmentStorage;
}

void ChainingSegmentsDetector::findLinesCalife(const cv::Mat* image, std::vector<Segment>& newSegments, double thresh ){
  
  double threshSq=thresh*thresh;
  IMCT *output = new IMCT;
  
  ///////////// extract contours //////////////////
  
  // if image height or length are not normalized (a multiple of 4) than the data refered by IplImage->imageDataOrigin is not ordered proper for use of ExtractContours1 function, therefore all the data has to be copied in a char array
  if( image->cols%4!=0 || image->rows%4!=0 ){   
    // change image format to char vector 
    char cImage[image->cols*image->rows];
    for(int i=0; i<image->cols; i++){
      for(int j=0; j<image->rows; j++){
        cImage[i*image->cols + j] = image->at<char>(i, j);
      }
    }
    ExtractContours1(cImage, output, image->rows , image->cols, 0, 0, image->rows, image->cols, 80, 40, (int) thresh);
  }
  else{
    ExtractContours1(reinterpret_cast<char*>(image->data), output, image->rows , image->cols, 0, 0, image->rows, image->cols, 80, 40, (int) thresh);
  }
 
  //// put contours into openCV sequenz, calculate lines and store in newSegments ///////////
  for(CHAINE* chain : output->liste)
  {
    std::vector<cv::Point> chain_cv;
    for( int j=0; j< chain->npoints; j++)
    {
      cv::Point tmp;
      tmp.x=chain->y[j];
      tmp.y=chain->x[j];
      chain_cv.push_back(tmp);
    }
    std::vector<cv::Point> lines;
    lines.resize(chain_cv.size());
    cv::approxPolyDP(chain_cv, lines, 1.0, false );
    for(int k=0; k < lines.size()-1; ++k)
    {
      cv::Point p1 = lines[k];
      cv::Point p2 = lines[k+1];
      
      double dx = p1.x - p2.x;
      double dy = p1.y - p2.y;
      // if long enough 
      if(dx*dx + dy*dy > threshSq )
      {
        Segment newLine;
        newLine.x1 = p1.x;
        newLine.y1 = p1.y;
        newLine.x2 = p2.x;
        newLine.y2 = p2.y;
        // assign contour points to line points and use line only if there are only few points on line with no neighboured contour pixel (neccessary because of strange connecting lines of cvApproxPoly function) 
        newSegments.push_back( newLine );
      }
    }
  }
}
