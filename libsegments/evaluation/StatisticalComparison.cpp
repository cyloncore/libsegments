#include "StatisticalComparison.hpp"

#include <cfloat>
#include <map>

#include <Segment.hpp>
#include <utils/Utils.hpp>
#include <utils/Macros_p.hpp>

#include "hungarian.h"

#define ppVar(x) LS_PP_VAR(x)

using namespace libsegments::evaluation;

namespace
{

  std::ostream& operator<<(std::ostream& _stream, const libsegments::Segment& _segment)
  {
    _stream << "[" << _segment.x1 << ", " << _segment.y1 << " " << _segment.x2 << ", " << _segment.y2 << "]";
    return _stream;
  }

  inline bool intersection( Eigen::Vector2d v1, Eigen::Vector2d v2, Eigen::Vector2d v3, Eigen::Vector2d v4, Eigen::Vector2d& _vec )
  {
    double x1 = v1(0); double y1 = v1(1);
    double x2 = v2(0); double y2 = v2(1);
    double x3 = v3(0); double y3 = v3(1);
    double x4 = v4(0); double y4 = v4(1);
    double d = (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4);
    if(fabs(d) < 1e-5) return false;
    double x1y2my1x2 = ( x1 * y2 - y1 * x2 );
    double x3y4my3x4 = ( x3 * y4 - y3 * x4 );
    _vec(0) = ( x1y2my1x2 * ( x3 - x4 ) - ( x1 - x2 ) * x3y4my3x4 ) / d;
    _vec(1) = ( x1y2my1x2 * ( y3 - y4 ) - ( y1 - y2 ) * x3y4my3x4 ) / d;
    return true;
  } 

  double areaSimilitude1( const libsegments::Segment& seg1, const libsegments::Segment& seg2 )
  {
    Eigen::Vector2d seg1_fp; seg1_fp << seg1.x1, seg1.y1;
    Eigen::Vector2d seg1_lp; seg1_lp << seg1.x2, seg1.y2;
    Eigen::Vector2d seg2_fp; seg2_fp << seg2.x1, seg2.y1;
    Eigen::Vector2d seg2_lp; seg2_lp << seg2.x2, seg2.y2;
    
    Eigen::Vector2d seg1_dir = seg1_lp - seg1_fp;
    double seg1_dir_norm     = seg1_dir.norm();
    seg1_dir                /= seg1_dir_norm;
    Eigen::Vector2d seg2_dir = seg2_lp - seg2_fp; seg2_dir.normalize();
    
  //   std::cout << "========== areaSimilitude ==========" << std::endl;
    Eigen::Vector2d pt1 = seg1_dir.dot(seg2_fp - seg1_fp) * seg1_dir + seg1_fp;
    Eigen::Vector2d pt2 = seg1_dir.dot(seg2_lp - seg1_fp) * seg1_dir + seg1_fp;
    
    // Compute the "recouvrement"
    double pt1Pos = (pt1 - seg1_fp ).dot(seg1_dir) / seg1_dir_norm;
    double pt2Pos = (pt2 - seg1_fp ).dot(seg1_dir) / seg1_dir_norm;
  //   std::cout << ppVar(pt1) << " " << ppVar(pt2) << std::endl;
  //   std::cout << ppVar(seg1.firstPoint()) << " " << ppVar(seg1.lastPoint()) << " " << ppVar(seg1.direction()) << std::endl;
  //   std::cout << ppVar(seg2.firstPoint()) << " " << ppVar(seg2.lastPoint()) << " " << ppVar(seg2.direction()) << std::endl;
  //   std::cout << ppVar(pt1Pos) << " " << ppVar(pt2Pos) << std::endl;
    if( (pt1Pos < 0.0 and pt2Pos < 0.0) or (pt1Pos > 1.0 and pt2Pos > 1.0 ) )
    {
      return 0.5 * std::numeric_limits<double>::max();
    }
    if( pt1Pos > pt2Pos ) {
      double tmp = pt2Pos;
      pt2Pos = pt1Pos;
      pt1Pos = tmp;
    }
    double top, bottom;
    if( pt1Pos < 0.0 and pt2Pos < 1.0 )
    {
      top = 1.0 - pt1Pos;
      bottom = pt2Pos;
    } else if( pt1Pos > 0.0 and pt2Pos > 1.0 )
    {
      top = pt2Pos;
      bottom = 1.0 - pt1Pos;
    } else {
      top = pt2Pos -pt1Pos;
      bottom = 1.0;
    }
    if( top > bottom ) {
      double tmp = bottom;
      bottom = top;
      top = tmp;
    }
    double recc = top / bottom;
    // Compute the surface below seg1
    double dist;
    double pt1pt2 = ( pt2 - pt1 ).norm();
    double a      = (pt1 - seg2_fp).norm();
    Eigen::Vector2d inter;
    if( intersection( seg1_fp, seg1_lp, seg2_fp, seg2_lp, inter ) )
    {
      // Interesection is needed for surface
  //     std::cout << ppVar(pt1) << " " << ppVar(pt2) << " " << inter << std::endl;
      // Compute area
      double b = (pt2 - seg2_lp).norm();
      double c = (pt2 - inter ).norm();
      double d = (pt1 - inter ).norm();
      dist = 0.5 * ( b * c + a * d );
  //     std::cout << ppVar(a) << " " << ppVar(b) << " " << ppVar(c) << " " << ppVar(d) << std::endl; 
    } else {
      dist = a * pt1pt2;
    }
    // Compute coefficient of colinerarity
    double coll = fabs( seg1_dir.dot(seg2_dir));
    // std::cout << ppVar(dist) << " " << ppVar(coll) << " " << ppVar(recc) << " " << ppVar(pt1pt2) << " " << ppVar(dist / (coll * recc * pt1pt2)) << std::endl;
    return dist / (coll * recc * pt1pt2);
  }

  double areaSimilitude(const libsegments::Segment& seg1, const libsegments::Segment& seg2)
  {
    return areaSimilitude1(seg1, seg2) + areaSimilitude1(seg2, seg1);
  }

  double distance_point(double x1, double y1, double x2, double y2)
  {
    return std::sqrt(pow2(x1-x2) + pow2(y1-y2));
  }

  double distance_seg_end_point(double x1, double y1, double x2, double y2, const libsegments::Segment& seg2)
  {
    return distance_point(x1, y1, seg2.x1, seg2.y1) + distance_point(x2, y2, seg2.x2, seg2.y2);
  }

  struct projection
  {
    projection(const libsegments::Segment& _seg, const libsegments::Segment& _dest)
    {
      x0 = _dest.x1;
      y0 = _dest.y1;
      p3 = distance_point(_dest.x1, _dest.y1, _dest.x2, _dest.y2);
      u = (_dest.x2 - _dest.x1) / p3;
      v = (_dest.y2 - _dest.y1) / p3;
      p0 = proj(_seg.x1, _seg.y1);
      p1 = proj(_seg.x2, _seg.y2);
      if(p0 > p1)
      {
        std::swap(p0, p1);
      }
    }
    double proj(double _x, double _y)
    {
      return (_x - x0) * u + (_y - y0) * v;
    }
    double distance_point_proj(double _x, double _y)
    {
      double p = proj(_x, _y);
      return std::sqrt(pow2(_x - (x0 + p * u)) + pow2(_y - (y0 + p * v)));
    }
    double u, v;
    double x0, y0;
    double p0, p1, p3; // coordinate on _dest of first point of _seg, second point of _seg, and second point of _dest (first point of _dest always has zero)
  };

  /**
   * Compute the length of intersection of seg, as projected on dest.
   */
  double intersectionn_length(const libsegments::Segment& _seg, const libsegments::Segment& _dest)
  {
    projection p{_seg, _dest};
    if(p.p1 < 0 or p.p0 > p.p3)
    {
      return 0.0;
    }
    double m0 = std::max(p.p0, 0.0);
    double m1 = std::min(p.p1, p.p3);
    return m1 - m0;
  }

  /**
   * Compute the length of uniion of seg, as projected on dest.
   */
  double union_length(const libsegments::Segment& _seg, const libsegments::Segment& _dest)
  {
    projection p{_seg, _dest};
    double m0 = std::min(p.p0, 0.0);
    double m1 = std::max(p.p1, p.p3);
    return m1 - m0;
  }

  double distance_a(const libsegments::Segment& _seg, const libsegments::Segment& _dest)
  {
    projection p(_seg, _dest);
    return p.distance_point_proj(_seg.x1, _seg.y1) + p.distance_point_proj(_dest.x1, _dest.y1);
    
  }

  double distance_ortho(const libsegments::Segment& _seg, const libsegments::Segment& _dest)
  {
    return 0.5 * (distance_a(_seg, _dest) + distance_a(_dest, _seg));
  }

  /**
   * structuralDistance as defined in ELSED paper
   */
  double structuralDistance(const libsegments::Segment& _seg, const libsegments::Segment& _dest)
  {
    double iou = intersectionn_length(_seg, _dest) / union_length(_seg, _dest);
    if(iou < 0.1)
    {
      return std::numeric_limits<double>::max();
    }
    double dest_len = distance_point(_dest.x1, _dest.y1, _dest.x2, _dest.y2);
    double dest_u = (_dest.x2 - _dest.x1) / dest_len;
    double dest_v = (_dest.y2 - _dest.y1) / dest_len;
    double seg_len = distance_point(_seg.x1, _seg.y1, _seg.x2, _seg.y2);
    double seg_u = (_seg.x2 - _seg.x1) / seg_len;
    double seg_v = (_seg.y2 - _seg.y1) / seg_len;
    double dot = dest_u * seg_u + dest_v * seg_v;
    // std::cout << dot << std::endl;
    if(std::abs(dot) < 0.96592582628906828675) // cost(15deg) = 0.96592582628906828675
    {
      return std::numeric_limits<double>::max();
    }
    // std::cout << distance_ortho(_seg, _dest) << std::endl;
    if(distance_ortho(_seg, _dest) > 2.0 * sqrt(2.0))
    {
      return std::numeric_limits<double>::max();
    }
    // std::cout << "!!" << std::endl;
    return std::min(distance_seg_end_point(_seg.x1, _seg.y1, _seg.x2, _seg.y2, _dest), distance_seg_end_point(_seg.x2, _seg.y2, _seg.x1, _seg.y1, _dest));
  }
}

StatisticalComparison::StatisticalComparison( const std::vector<libsegments::Segment>& ref, const std::vector<libsegments::Segment>& comp, MatchMode _match_mode, double _sap_threshold)
{
  m_totalRefSegments = ref.size();
  m_totalCompSegments = comp.size();
  m_repeatedSegmentsCount = 0;

  std::vector<std::pair<libsegments::Segment, libsegments::Segment>> ref_2_comp;

  switch(_match_mode)
  {
    case MatchMode::DoubleMatch:
    {
      for( std::size_t i = 0; i < comp.size(); ++i )
      {
        LS_DEBUG("==========================" << i);
    //     ++pb;
        const libsegments::Segment* seg = &comp[i];
        LS_DEBUG(*seg);
        const libsegments::Segment* bestMatch = 0;
        double bestDistance = 50.0;
        bool splited = false;
        for( std::size_t j = 0; j < ref.size(); ++j )
        {
          LS_DEBUG("=========== i, j = " << i << ", " << j);
          const libsegments::Segment* segref = &ref[j];
          double distance = areaSimilitude(*seg, *segref);
          LS_DEBUG(*segref << "distance(" << j << ", " <<  i << ") = " << distance);
          if(distance < bestDistance )
          {
            bool hasCloser = false;
            const libsegments::Segment* bestCloser;
            for( std::size_t k = 0; k < comp.size(); ++k )
            {
              LS_DEBUG("===== k = " << k);
              if( k != i )
              {
                const libsegments::Segment* seg2 = &comp[k];
                double distance2 = areaSimilitude(*segref, *seg2);
                LS_DEBUG(*seg2 << "distance2(" << k << ", " <<  j << ") = " << distance2);
                if( distance2 < distance )
                {
                  hasCloser = true;
                  bestCloser = seg2;
                  break;
                }
              }
            }
            if( hasCloser ) {
              
              Eigen::Vector2d seg1_fp; seg1_fp << seg->x1, seg->y1;
              Eigen::Vector2d seg1_lp; seg1_lp << seg->x2, seg->y2;
              Eigen::Vector2d seg2_fp; seg2_fp << bestCloser->x1, bestCloser->y1;
              Eigen::Vector2d seg2_lp; seg2_lp << bestCloser->x2, bestCloser->y2;
              
              Eigen::Vector2d seg1_dir = seg1_lp - seg1_fp; seg1_dir.normalize();
              Eigen::Vector2d seg2_dir = seg2_lp - seg2_fp; seg2_dir.normalize();
              
              if( fabs(seg1_dir.dot(seg2_dir) ) > 0.95 )
              {
                splited = true;
              }
            } else {
              bestDistance = distance;
              bestMatch = segref;
            }
          }
        }
        if( bestMatch )
        {
          ref_2_comp.push_back({*bestMatch, *seg});
          ++m_repeatedSegmentsCount;
          m_distances.push_back(bestDistance);
        } else {
          if( splited )
          {
            m_splited.push_back(*seg);
          } else {
            m_unmachted.push_back(*seg);
          }
        }
      }
      break;
    }
    case MatchMode::StructuralHungarian:
    case MatchMode::AreaDistanceHungarian:
    {
      std::function<double(const Segment&, const Segment&)> cost_function;
      switch(_match_mode)
      {
      case MatchMode::StructuralHungarian:
        cost_function = [](const Segment& _a, const Segment& _b) { 
          return structuralDistance(_a, _b); };
        break;
      case MatchMode::AreaDistanceHungarian:
        cost_function = [](const Segment& _a, const Segment& _b) {
          if(distance_ortho(_a, _b) > 2.0 * sqrt(2.0))
          {
            return std::numeric_limits<double>::max();
          }
          return areaSimilitude(_a, _b); };
        break;
      default:
        LS_ERROR("Not an hungarian method.");
      }
      const double inf_cost = 1000.0; // std::numeric_limits<double>::max();
      std::vector<WeightedBipartiteEdge> allEdges;
      std::size_t count_cost = std::max(ref.size(), comp.size());
      for( std::size_t i = 0; i < comp.size(); ++i )
      {
        for( std::size_t j = 0; j < ref.size(); ++j )
        {
          double c = std::min(cost_function(comp[i], ref[j]), inf_cost);
          if(not std::isfinite(c))
          {
            c = inf_cost;
          }
          allEdges.push_back({i, j, c});
        }
        for( std::size_t j =  ref.size(); j < count_cost; ++j)
        {
          allEdges.push_back({i, j, inf_cost});
        }
      }
      for( std::size_t i = comp.size(); i < count_cost; ++i )
      {
        for( std::size_t j = 0; j < count_cost; ++j)
        {
          allEdges.push_back({i, j, inf_cost});
        }
      }
      std::vector<int> assignment = hungarianMinimumWeightPerfectMatching(count_cost, allEdges);
      if(assignment.size() > 0)
      {
        for(std::size_t i = 0; i < comp.size(); ++i)
        {
          int a = assignment[i];
          if(a < ref.size())
          {
            double s = cost_function(comp[i], ref[a]);
            if(s < inf_cost)
            {
              m_distances.push_back(s);
              ref_2_comp.push_back({ref[a], comp[i]});
            } else {
              m_unmachted.push_back(comp[i]);
            }
          } else {
            m_unmachted.push_back(comp[i]);
          }
        }
        m_repeatedSegmentsCount = m_distances.size();
      } else {
        LS_ERROR("Assignment failed");
      }
      break;
    }
    case MatchMode::sap:
    {
      std::map<std::size_t, std::pair<double, std::size_t>> match;
      for(std::size_t ls_idx = 0; ls_idx < comp.size(); ++ls_idx)
      {
        double best_score = _sap_threshold;
        std::size_t best_index = std::numeric_limits<std::size_t>::max();
        const libsegments::Segment& ls_seg = comp[ls_idx];
        for(std::size_t gt_idx = 0; gt_idx < ref.size(); ++gt_idx)
        {
          const libsegments::Segment& ref_seg = ref[gt_idx];
          double score = std::min(distance_seg_end_point(ls_seg.x1, ls_seg.y1, ls_seg.x2, ls_seg.y2, ref_seg), distance_seg_end_point(ls_seg.x2, ls_seg.y2, ls_seg.x1, ls_seg.y1, ref_seg));
          if(score < best_score)
          {
            best_score = score;
            best_index = gt_idx;
          }
        }
        if(best_index != std::numeric_limits<std::size_t>::max())
        {
          if(match.find(best_index) == match.end() or best_score < match[best_index].first)
          {
            match[best_index] = {best_score, ls_idx};
          }
        }
      }
      m_repeatedSegmentsCount = match.size();
      for(std::size_t ls_idx = 0; ls_idx < comp.size(); ++ls_idx)
      {
        bool found = false;
        for(auto it = match.begin(); it != match.end(); ++it)
        {
          if(it->second.second == ls_idx)
          {
            found = true;
            break;
          }
        }
        if(not found)
        {
          m_unmachted.push_back(comp[ls_idx]);
        }
      }
      for(auto it = match.begin(); it != match.end(); ++it)
      {
        ref_2_comp.push_back({ref[it->first], comp[it->second.second]});
        m_distances.push_back(it->second.first);
      }

    }
  }

  LS_ASSERT( m_repeatedSegmentsCount <= m_totalRefSegments, m_repeatedSegmentsCount << " > " << m_totalRefSegments );
  LS_ASSERT( (m_repeatedSegmentsCount + unmatched() + splited()) == m_totalCompSegments, "bug" );
  LS_ASSERT( std::size_t(m_repeatedSegmentsCount) == m_distances.size(), "bug");

  // Compute ELSED scores

  m_elsedPrecision = 0.0;
  m_elsedRecall = 0.0;
  m_elsedIntersectionOverUnion = 0.0;
  double elsedUnion = 0.0;
  double sum_ref_length = 0;
  double sum_comp_length = 0;

  for(const std::pair<libsegments::Segment, libsegments::Segment>& r2c : ref_2_comp)
  {
    m_elsedPrecision += intersectionn_length(r2c.first, r2c.second);
    m_elsedRecall += intersectionn_length(r2c.second, r2c.first);
    elsedUnion += union_length(r2c.second, r2c.first);
  }
  for(const libsegments::Segment& c : comp)
  {
    sum_comp_length += distance_point(c.x1, c.y1, c.x2, c.y2);
  }
  for(const libsegments::Segment& r : ref)
  {
    sum_ref_length += distance_point(r.x1, r.y1, r.x2, r.y2);
  }
  m_elsedIntersectionOverUnion = m_elsedRecall / elsedUnion;
  m_elsedPrecision = m_elsedPrecision / sum_comp_length;
  m_elsedRecall = m_elsedRecall / sum_ref_length;
}

StatisticalComparison::~StatisticalComparison()
{
}

double StatisticalComparison::repetability() const
{
  return m_repeatedSegmentsCount / (double)m_totalRefSegments;
}

int StatisticalComparison::unmatched() const
{
  return m_unmachted.size();
}

int StatisticalComparison::splited() const
{
  return m_splited.size();
}

double StatisticalComparison::distanceMean() const
{
  double m = 0.0;
  LS_FOREACH( double v, m_distances)
  {
    m += v;
  }
  return m / m_distances.size();
}

double StatisticalComparison::distanceStdDev() const
{
  double m = distanceMean();
  double s = 0.0;
  LS_FOREACH( double v, m_distances)
  {
    double a = v - m;
    s += a * a;
  }
  return sqrt( s / m_distances.size());
}

std::vector< libsegments::Segment > StatisticalComparison::umatchedSegments() const
{
  return m_unmachted;
}

std::vector< libsegments::Segment > StatisticalComparison::splitedSegments() const
{
  return m_splited;
}

int StatisticalComparison::totalRefSegments() const
{
  return m_totalRefSegments;
}

int StatisticalComparison::totalCompSegments() const
{
  return m_totalCompSegments;
}

double StatisticalComparison::sapScore() const
{
  return m_distances.size() / double(m_totalRefSegments);
}

double StatisticalComparison::elsedPrecision() const
{
  return m_elsedPrecision;
}

double StatisticalComparison::elsedRecall() const
{
  return m_elsedRecall;
}

double StatisticalComparison::elsedIntersectionOverUnion() const
{
  return m_elsedIntersectionOverUnion;
}
