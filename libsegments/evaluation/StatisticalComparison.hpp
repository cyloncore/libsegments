
#ifndef _STATISTICAL_COMPARISON_H_
#define _STATISTICAL_COMPARISON_H_

#include <vector>

#include "../Segment.hpp"

namespace libsegments::evaluation
{

  /**
    * @ingroup dseg
    * This class should be used to compute repeatibility of segments detection
    * on a "still" image (aka image taken with a camera that doesn't move,
    * or an image which have been filtered). Or for comparison with a ground truth.
    */
  class StatisticalComparison {
    public:
      enum class MatchMode
      {
        DoubleMatch,
        StructuralHungarian, //< Use structural distance as in ELSED paper
        AreaDistanceHungarian, //< Use aera distance
        sap
      };
    public:
      StatisticalComparison( const std::vector<libsegments::Segment>& ref,
                              const std::vector<libsegments::Segment>& comp,
                              MatchMode _match_mode, double _sap_threshold = 5.0 );
      ~StatisticalComparison();
      double repetability() const;
      /**
        * @return the number of segment in comp for which no segment was found in ref.
        */
      int unmatched() const;
      /**
        * @return the number of segments that were splited during redetection. Only available for DoubleMatch.
        */
      int splited() const;
      /**
        * @return the mean on the distance between matched segments and the original.
        */
      double distanceMean() const;
      /**
        * @return the standard deviation on the distance between matched segments and the original.
        */
      double distanceStdDev() const;
      /**
        * @return the list of segment that weren't matched
        */
      std::vector< libsegments::Segment > umatchedSegments() const;
      /**
        * @return the list of segment that were splitted. Only available for DoubleMatch.
        */
      std::vector< libsegments::Segment > splitedSegments() const;
      /**
        * @return thth enumber of segment in the reference image.
        */
      int totalRefSegments() const;
      /**
        * @return the number of segment in the comparison image
        */
      int totalCompSegments() const;

      /**
       * @return number of segment matched / number of reference segments
       */
      double sapScore() const;

      /**
       * From "ELSED: Enhanced Line SEgment Drawin"
       * 
       * @return  Length of the matched intersection measured over the detected segment xi, divided by the length of the detected segments.
       */
      double elsedPrecision() const;
      /**
       * From "ELSED: Enhanced Line SEgment Drawin"
       * 
       * @return Length of the matched intersection measured over the ground truth segment y j , divided by the length of the ground truth segments.
       */
      double elsedRecall() const;
      /**
       * From "ELSED: Enhanced Line SEgment Drawin"
       * 
       * @return Length of the matched intersection measured over the ground truth segments, divided by length of the matched union measured over the ground truth segments
       */
      double elsedIntersectionOverUnion() const;
      double elseRepeatibility() const;
    private:
      int m_totalRefSegments;
      int m_totalCompSegments;
      int m_repeatedSegmentsCount;
      double m_elsedPrecision, m_elsedRecall, m_elsedIntersectionOverUnion;
      std::vector< double > m_distances;
      std::vector< libsegments::Segment > m_unmachted;
      std::vector< libsegments::Segment > m_splited;
  };
  
} // namespace libsegments::evaluation

#endif
