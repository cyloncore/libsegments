/* $Id: HierarchicalDirectSegmentsDetector.hpp 4054 2009-08-24 19:02:41Z cberger $ */

#ifndef _DSEG_HIERARCHICAL_DIRECT_SEGMENTS_DETECTOR_HPP_
#define _DSEG_HIERARCHICAL_DIRECT_SEGMENTS_DETECTOR_HPP_

#include <list>
#include <dseg/DirectSegmentsDetectorBase.hpp>

namespace libsegments {
  namespace hdseg {
    class PyramidSP;
    /**
     * @ingroup dseg
     * This is the main class for detection segments in an image.
     * It uses a Hierarchical approach.
     */
    class HierarchicalDirectSegmentsDetector : public dseg::DirectSegmentsDetectorBase {
      public:
        HierarchicalDirectSegmentsDetector(utils::IdFactory* _idMaker = 0);
      public:
        /**
         * Start detection of segments in an image.
         * \param _image image on which to makes the detection
         * \param segmentStorage storage that will be filled with newly extracted segment,
         *                       if there are segments in the storage before calling this
         *                       function, then the dector will avoid segment detection
         *                       on the already existing segments (the detection can be
         *                       called after tracking, and tracked segments won't be
         *                       detected again).
         * \param SegmentDebug collect some debug information
         */
        void detectSegments( const cv::Mat& _image, libsegments::dseg::SegmentsSet& _segmentStorage, libsegments::dseg::SegmentDebug* _sdebug = 0);
      public:
        /**
         * Define the number of levels of the pyramid (minimum is 1, default is 3).
         */
        LS_DEFINE_PARAM_RW( int, levels, setLevels );
      protected:
        virtual void detectedSegment( dseg::SegmentHypothesis* , dseg::DirectSegmentsDetectorBase::LoopDetectionContext* _context, dseg::GrowSegmentContext&);
        virtual bool isInASegment( dseg::DirectSegmentsDetectorBase::LoopDetectionContext* _context, int _x, int _y );
      private:
        /**
         * Detect segment at a given scale using an hypothesis coming from a different level.
         * @param segHyp is the hypothesis at level @p level - 1
         * @param level is the pyramid level
         * 
         * This function will repeatively called itself until @p level == 0
         */
        void detectSegmentAtScale( dseg::SegmentHypothesis* segHyp, dseg::DirectSegmentsDetectorBase::LoopDetectionContext* _context, int level);
        /**
         * Find a segment hypothesis around @p _center .
         */
        dseg::SegmentHypothesis* findSegHyp( const Eigen::Vector2d& _center, double angle, std::vector< dseg::GrowSegmentContext >& hyps, int _level, dseg::DirectSegmentsDetectorBase::LoopDetectionContext* _context, double* grads, double scaledMinLength2  );
        struct Interval;
        void updateIntervals( dseg::SegmentHypothesis* _segHyp, const Interval& _currentInterval, std::list<Interval>& _intervals, int _level, const Eigen::Vector2d& _center, const Eigen::Vector2d& _dir, double _scaledMinLength );
      private:
        PyramidSP* m_pyramidSP; // Should be in the LoopDetectionContext
        int m_topToBottomScale;
    };
  }
}

#endif
