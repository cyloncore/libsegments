#ifndef _DESG_PYRAMIDSP_HPP_
#define _DESG_PYRAMIDSP_HPP_

namespace cv {
  class Mat;
}

namespace libsegments {
  namespace dseg {
    class SegmentProbabilityEstimator;
  }
  namespace hdseg {
    /**
     * @internal
     * Pyramid of segment probability, used by \ref HierarchicalDirectSegmentsDetector .
     */
    class PyramidSP {
      public:
        /**
         * @param _image image on which to compute the pyramid
         * @param _estimator probability estimator used to create this pyramid
         */
        PyramidSP( const cv::Mat& _image, int _levels, libsegments::dseg::SegmentProbabilityEstimator* _estimator );
        ~PyramidSP();
        const cv::Mat& imageAt( int _level ) const;
        const cv::Mat& dxAt( int _level ) const;
        const cv::Mat& dyAt( int _level ) const;
        const cv::Mat& probaAt( int _level ) const;
      private:
        struct Private;
        Private* const d;
    };
  }
}

#endif
