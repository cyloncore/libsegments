/* $Id: HierarchicalDirectSegmentsDetector.cpp 4054 2009-08-24 19:02:41Z cberger $ */

#include "HierarchicalDirectSegmentsDetector.hpp"

#include "PyramidSP.hpp"
#include "dseg/SegmentHypothesis.hpp"
#include "dseg/GradientsDescriptor.hpp"
#include "dseg/SegmentsSet.hpp"
#include "dseg/LineFitterKalman2.hpp"
#include "dseg/SegmentHypothesisDebug.hpp"
#include "dseg/DetectorGrowStopCondition.hpp"
#include "dseg/LoopDetectionContext.hpp"

#include "utils/Macros_p.hpp"
#include "utils/Image.hpp"

using namespace libsegments::dseg;
using namespace libsegments::hdseg;

HierarchicalDirectSegmentsDetector::HierarchicalDirectSegmentsDetector(libsegments::utils::IdFactory* _idMaker) : DirectSegmentsDetectorBase(_idMaker), m_levels(3)
{
}

void HierarchicalDirectSegmentsDetector::detectSegments( const cv::Mat& _image, libsegments::dseg::SegmentsSet& _segmentStorage, libsegments::dseg::SegmentDebug* _sdebug)
{
  m_topToBottomScale = pow(2, levels() - 1);
  segCount = 1;
  int levels = 3;
  PyramidSP pyramid(_image, levels, segmentProbabilityEstimator() );
  m_pyramidSP = &pyramid;
  
  LS_DEBUG( "Pyramid with " << levels << " levels." );
  for(int i = 0; i < levels; ++i)
  {
    LS_DEBUG( i << ": " << pyramid.imageAt(i).cols << " x " << pyramid.imageAt(i).rows );
  }
  
  cv::Mat topLevelProba = pyramid.probaAt( levels - 1 );
  cv::Mat topLevelDx = pyramid.dxAt( levels - 1 );
  cv::Mat topLevelDy = pyramid.dyAt( levels - 1 );
  
  // Init the inassegment image corresponding to the bottom
  cv::Mat inasegment( _image.rows, _image.cols, CV_MAKETYPE(CV_32S, 1) );

  if( _sdebug )
  {
    _sdebug->inasegment = inasegment;
    _sdebug->proba = topLevelProba;
    _sdebug->dx = topLevelDx;
    _sdebug->dy = topLevelDy;
  }
  
  memset( inasegment.data, 0, inasegment.rows * inasegment.step);
  
  for(std::size_t i = 0; i < _segmentStorage.count(); ++i )
  {
    const SegmentHypothesis* sh = _segmentStorage.segmentAt( i );
    
    coloriage( sh->x1(), sh->y1(), sh->x2(), sh->y2(), inasegment, sh->id() );
  }

  DirectSegmentsDetectorBase::LoopDetectionContext context(&_segmentStorage, topLevelProba, topLevelDx, topLevelDy, inasegment
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
      , _image
#endif
      );
  loopDetect( &context, 1.0 / m_topToBottomScale );

  if( computeDescriptor() )
  {
    for( unsigned int i = 0; i < _segmentStorage.count(); ++i )
    {
      SegmentHypothesis* segHyp = _segmentStorage.segmentAt( i );
      segHyp->setDescriptor( new GradientsDescriptor( *segHyp, pyramid.dxAt(0), pyramid.dyAt(0) ) );
    }
  }
  m_pyramidSP = 0;
}

void HierarchicalDirectSegmentsDetector::detectedSegment( libsegments::dseg::SegmentHypothesis* segHyp, libsegments::dseg::DirectSegmentsDetectorBase::LoopDetectionContext* _context, libsegments::dseg::GrowSegmentContext&)
{
  detectSegmentAtScale( segHyp, _context, levels() - 2 );
}

SegmentHypothesis* HierarchicalDirectSegmentsDetector::findSegHyp( const Eigen::Vector2d& _center, double angle, std::vector< GrowSegmentContext >& hyps, int _level, DirectSegmentsDetectorBase::LoopDetectionContext* _context, double* grads, double scaledMinLength2 ) {
  // Check that the center isn't allread usd in an other segment
  int scale = pow(2, _level);
  if(utils::image::getPixelValue<int>(_context->inasegment(), _center[0] / scale, _center[1] / scale ) != 0) return  0;
  
  LS_DEBUG_COND( DEBUG_COND, "Init search around: " << _center << " at level " << _level);
  // Create an hypothesis
  SegmentHypothesis* segHyp = new SegmentHypothesis(_center[0], _center[1], angle);
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
  SearchPointLineDebug searchPointLine;
  searchPointLine.addPoint( _center[0], _center[1], angle, grads[2], m_pyramidSP->imageAt(_level).getPixelValue<uint8_t>(_center[0], _center[1])  );
  segHyp->segmentHypothesisDebug()->addSearchPointLine( searchPointLine );
#endif

  GrowSegmentContext gsc;
#if ENABLE_SEGMENT_HYPOTHESIS_DEBUG
  gsc.originalImage = m_pyramidSP->imageAt(_level);
#endif
  gsc.segHyp = segHyp;
  gsc.proba = m_pyramidSP->probaAt(_level);
  gsc.dx = m_pyramidSP->dxAt(_level);
  gsc.dy = m_pyramidSP->dyAt(_level);
  gsc.inasegment = _level == 0 ? &_context->inasegment() : 0;
  gsc.x_1 = _center[0];
  gsc.x_2 = _center[0];
  gsc.y_1 = _center[1];
  gsc.y_2 = _center[1];
  gsc.k_p = 1;
  gsc.k_n = -1;
  DetectorGrowStopCondition dgsc;
  gsc.growStopCondition = &dgsc;
  
  bool success = growSegment( gsc, 3 );
  if(success)
  {
    segHyp->setExtremity1( gsc.x_1, gsc.y_1 );
    segHyp->setExtremity2( gsc.x_2, gsc.y_2 );
    if(gsc.segHyp->squareLength() > scaledMinLength2) {
      hyps.push_back(gsc);
      return segHyp;
    }
  }
  delete segHyp;
  return 0;
}

struct HierarchicalDirectSegmentsDetector::Interval {
    Interval(int _downBoundary, int _upBoundary) :
        m_downBoundary(_downBoundary),m_upBoundary(_upBoundary) {
      LS_ASSERT(m_downBoundary < m_upBoundary, "Unordered interval");
    }
    inline double middle() { return 0.5 * ( m_downBoundary + m_upBoundary ); }
    int m_downBoundary, m_upBoundary;
};

int makeIndex( double x, double y, const Eigen::Vector2d& center, const Eigen::Vector2d& dir )
{
  Eigen::Vector2d a;
  a(0) = x; a(1) = y;
  a -= center;
  return (a.adjoint() * dir)(0);
}

void HierarchicalDirectSegmentsDetector::updateIntervals( SegmentHypothesis* _segHyp, const Interval& _currentInterval, std::list<Interval>& _intervals, int _level, const Eigen::Vector2d& _center, const Eigen::Vector2d& _dir, double _scaledMinLength )
{
  if(_segHyp ) {
    std::list<Interval> new_intervals;
    int idx1 = makeIndex( _segHyp->x1(), _segHyp->y1(), _center, _dir);
    int idx2 = makeIndex( _segHyp->x2(), _segHyp->y2(), _center, _dir);
    if(idx2 < idx1 ) {
      int tmp = idx1; idx1 = idx2; idx2 = tmp;
    }
    LS_ASSERT(idx1 <= idx2, "bug");
    idx1 -= 1;
    idx2 += 1;
    LS_FOREACH(Interval iv, _intervals) {
      /*
       * Remove from the interval the part that was used to create the segment
       * we know that iv.m_downBoundary < iv.m_upBoundary and idx1 < idx2
       */
      LS_ASSERT(iv.m_downBoundary < iv.m_upBoundary, "Unordered interval");
      if( idx2 < iv.m_downBoundary or iv.m_upBoundary < idx1 ) {
        new_intervals.push_back(iv);
      } else if( idx1 < iv.m_downBoundary and iv.m_upBoundary < idx2 ) {
        // Bah on fait rien l'interval est bouffer par le segment
      } else {
        if( (idx1 - iv.m_downBoundary) > _scaledMinLength ) {
          new_intervals.push_back(Interval(iv.m_downBoundary, idx1));
        }
        if( (iv.m_upBoundary - idx2) > _scaledMinLength ) {
          new_intervals.push_back(Interval(idx2, iv.m_upBoundary));
        }
      }
    }
    _intervals = new_intervals;
  }
}

void HierarchicalDirectSegmentsDetector::detectSegmentAtScale( SegmentHypothesis* _segHyp, DirectSegmentsDetectorBase::LoopDetectionContext* _context, int _level )
{
  double scaledMinLength = minimumSegmentLength() / pow(2, _level );
  double scaledMinLength2 = scaledMinLength * scaledMinLength;
  // Compute the center of the segment at next point
//   double x_0 = _segHyp->x1() + _segHyp->x2(); // / 2.0 * 2.0 // Because of the change of scale
//   double y_0 = _segHyp->y1() + _segHyp->y2(); // / 2.0 * 2.0
  LS_DEBUG_COND( DEBUG_COND, _level << " " << _segHyp->x1() << " " << _segHyp->x2() << " " << _segHyp->y1() << " " << _segHyp->y2() );
  
#if 1
  
  double x_1 = _segHyp->x1() * 2.0;
  double y_1 = _segHyp->y1() * 2.0;
  Eigen::Vector2d origin; origin(0) = x_1; origin(1) = y_1;
  Eigen::Vector2d end; end(0) = 2 * _segHyp->x2(); end(1) = 2 * _segHyp->y2();
  
  // Compute the line parameters
  const Eigen::Vector2d& _normal = _segHyp->lineFitter().vecNormal();
  Eigen::Vector2d direction = end - origin;
  direction /= direction.norm();

  double angle = _segHyp->lineFitter().angle();

  // Initialise intervals
  int index1 = makeIndex( end(0), end(1), origin, direction);
  std::list<Interval> intervals;
  intervals.push_back(Interval(0, index1));
  
  // List of newly detected segment hypothesis
  std::vector< GrowSegmentContext > hyps;
  
  // We iterate on intervals, and try to find a segment starting from the middle of an interval
  while( not intervals.empty() ) {
    Interval interval = intervals.front();
    intervals.pop_front();
    Eigen::Vector2d center = direction * interval.middle() + origin;

    // We need to find the maximum(s) of gradients
    double grads[5];
    bool found = false; // Indicates if at least one segment has been found
    
    // Compute gradients
    // TODO a possible optimization could be to first compute from -1 to +1, to only compute
    // center, and then compute 0 and 4
    for( int i = -2; i <= 2; ++i )
    {
      Eigen::Vector2d vi = center + i * _normal;
      LS_DEBUG_COND( DEBUG_COND, vi );
      grads[i+2] = utils::image::getSubPixelValue<float>( m_pyramidSP->probaAt(_level), vi(0) - 0.5, vi(1) - 0.5, 0, utils::image::INTERP_CUBIC );
    }
    LS_DEBUG_COND( DEBUG_COND, "grads[] = " << grads[0] << " " << grads[1] << " " << grads[2] << " " << grads[3] << " " << grads[4] );
    LS_DEBUG_COND( DEBUG_COND, LS_PP_VAR(center) << LS_PP_VAR(center + _normal) << LS_PP_VAR(center - _normal) << LS_PP_VAR(_normal));
    
    // We have three possible maximum of gradient, on the left(1), center(2), right(3).
    // if the center gradient is a maximum, then left and right can't be one
    if( grads[2] > grads[1] and grads[2] > grads[3] ) 
    {
      SegmentHypothesis* segHyp = findSegHyp(center, angle, hyps, _level, _context, grads, scaledMinLength2);
      updateIntervals(segHyp, interval, intervals, _level, origin, direction, scaledMinLength);
      found |= bool(segHyp);
    } else {
      // Test for right
      if( grads[3] > grads[2] and grads[3] > grads[4] ) 
      {
        SegmentHypothesis* segHyp = findSegHyp(center + _normal, angle, hyps, _level, _context, grads, scaledMinLength2);
        updateIntervals(segHyp, interval, intervals, _level, origin, direction, scaledMinLength);
        found |= bool(segHyp);
      }
      // Test for left
      if( grads[1] > grads[0] and grads[1] > grads[2] ) 
      {
        SegmentHypothesis* segHyp = findSegHyp(center - _normal, angle, hyps, _level, _context, grads, scaledMinLength2);
        updateIntervals(segHyp, interval, intervals, _level, origin, direction, scaledMinLength);
        found |= bool(segHyp);
      }
    }
    // If we didn't found anything, split the interval in two, unless they become too small
    if(not found) {
      int middle = (interval.m_downBoundary + interval.m_upBoundary) / 2;
      if( (middle - interval.m_downBoundary) > scaledMinLength ) {
        intervals.push_back(Interval(interval.m_downBoundary, middle));
      }
      if( (interval.m_upBoundary - middle) > scaledMinLength ) {
        intervals.push_back(Interval(middle, interval.m_upBoundary));
      }
    }
  }
#else
  std::vector< SegmentHypothesis* > hyps;
  _segHyp->applyScale(2.0);
  hyps.push_back(_segHyp);
#endif
  LS_DEBUG_COND( DEBUG_COND, "Found " << hyps.size() << " at level " << _level);
  for( std::vector<GrowSegmentContext >::iterator it = hyps.begin();
       it != hyps.end(); ++it)
  {
    SegmentHypothesis* segHyp = it->segHyp;
    // If we are at level 0, we have reached the bottom of the pyramid
    if( _level == 0 )
    {
      double scale = 1.0; // / pow(2, levels() - 1);
      double x_1 = segHyp->x1() * scale; double y_1 = segHyp->y1() * scale;
      double x_2 = segHyp->x2() * scale; double y_2 = segHyp->y2() * scale;
      
      // First, try to merge the segment
      SegmentHypothesis* mergedWith = 0;
      if( mergeSegment() )
      {
        mergedWith = attemptToMerge( segHyp, _context->inasegment(), *_context->segmentStorage(), it->segmentToCount );
      }
      if( mergedWith )
      {
        coloriage( x_1, y_1, x_2, y_2, _context->inasegment(), mergedWith->id() );
        delete segHyp;
      } else {
        LS_DEBUG_COND( DEBUG_COND, x_1 << " " << x_2 << " " << y_1 << y_2 );
        LS_DEBUG_COND( DEBUG_COND, _segHyp->x1() << " " << _segHyp->x2() << " " << _segHyp->y1() << " " << _segHyp->y2() );
        segHyp->setId( nextId() );
        coloriage( x_1, y_1, x_2, y_2, _context->inasegment(), segHyp->id() );
        _context->segmentStorage()->addSegment( segHyp );
        ++segCount;
      }
    } else {
        detectSegmentAtScale( segHyp, _context, _level - 1 );
        // delete segHyp; // TODO no ?
    }
  }
}

bool HierarchicalDirectSegmentsDetector::isInASegment( DirectSegmentsDetectorBase::LoopDetectionContext* _context, int _x, int _y ) {
  return utils::image::getPixelValue<int>(_context->inasegment(),_x * m_topToBottomScale, _y * m_topToBottomScale) != 0;
}
