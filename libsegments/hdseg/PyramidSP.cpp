/* $Id: PyramidSP.cpp 3672 2009-03-04 15:53:07Z cberger $ */

#include "PyramidSP.hpp"

#include <opencv2/opencv.hpp>

#include "utils/Pyramid.hpp"
#include "dseg/SegmentProbabilityEstimator.hpp"

using namespace libsegments::dseg;
using namespace libsegments::hdseg;

struct PyramidSP::Private {
  Private( int _levels ) : imagePyramid(_levels), probaPyramid(_levels), dxPyramid(_levels), dyPyramid(_levels) { }
  utils::image::LinearPyramid imagePyramid;
  std::vector<cv::Mat> probaPyramid;
  std::vector<cv::Mat> dxPyramid;
  std::vector<cv::Mat> dyPyramid;
};

PyramidSP::PyramidSP( const cv::Mat& _image, int _levels, SegmentProbabilityEstimator* _estimator ) : d(new Private(_levels))
{
  d->imagePyramid.build( _image, _levels );
  for(int i = 0; i < _levels; ++i)
  {
    cv::Mat source = d->imagePyramid[i];
    cv::Mat dx( source.rows, source.cols, CV_MAKETYPE(CV_16S, 1) );
    cv::Mat dy( source.rows, source.cols, CV_MAKETYPE(CV_16S, 1) );
    cv::Mat proba( source.rows, source.cols, CV_MAKETYPE(CV_32F, 1) );
    cv::Sobel( source, dx, CV_16S, 1, 0, 3 );
    cv::Sobel( source, dy, CV_16S, 0, 1, 3 );
    _estimator->compute( dx, dy, proba  );
    d->dxPyramid[ i ] = dx;
    d->dyPyramid[ i ] = dy;
    d->probaPyramid[ i ] = proba;
  }
}

PyramidSP::~PyramidSP()
{
  delete d;
}

const cv::Mat& PyramidSP::imageAt( int _level ) const
{
  return d->imagePyramid[ _level ];
}

const cv::Mat& PyramidSP::dxAt( int _level ) const
{
  return d->dxPyramid[ _level ];
}

const cv::Mat& PyramidSP::dyAt( int _level ) const
{
  return d->dyPyramid[ _level ];
}

const cv::Mat& PyramidSP::probaAt( int _level ) const
{
  return d->probaPyramid[ _level ];
}

